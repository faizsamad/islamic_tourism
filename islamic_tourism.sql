-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2018 at 01:40 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2basic`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1493684515),
('support', '2', 1535283000);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1526015448, 1526015448),
('/admin/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/assign', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/revoke', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/default/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/default/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/delete', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/update', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/assign', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/delete', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/remove', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/update', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/assign', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/delete', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/remove', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/update', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/assign', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/refresh', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/remove', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/delete', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/update', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/user/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/activate', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/change-password', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/delete', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/index', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/login', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/logout', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/request-password-reset', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/reset-password', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/signup', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/view', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/dashboard/*', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/dashboard/index', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/debug/*', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/*', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/db-explain', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/download-mail', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/index', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/toolbar', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/view', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/user/*', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/user/reset-identity', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/user/set-identity', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/departments/*', 2, NULL, NULL, NULL, 1535259949, 1535259949),
('/gii/*', 2, NULL, NULL, NULL, 1526015448, 1526015448),
('/gii/default/*', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/action', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/diff', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/index', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/preview', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/view', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/profil/*', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/create', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/delete', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/index', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/update', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/view', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/route/*', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/ajax-stop-list', 2, NULL, NULL, NULL, 1505969462, 1505969462),
('/route/create', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/delete', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/get-location', 2, NULL, NULL, NULL, 1505969462, 1505969462),
('/route/index', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/list', 2, NULL, NULL, NULL, 1505969462, 1505969462),
('/route/map', 2, NULL, NULL, NULL, 1505969462, 1505969462),
('/route/update', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/view', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/site/*', 2, NULL, NULL, NULL, 1505969455, 1505969455),
('/site/captcha', 2, NULL, NULL, NULL, 1505969455, 1505969455),
('/site/error', 2, NULL, NULL, NULL, 1505969455, 1505969455),
('/site/index', 2, NULL, NULL, NULL, 1505969455, 1505969455),
('/user/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/assignments', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/block', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/confirm', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/create', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/delete', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/index', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/info', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/resend-password', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/switch', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/update', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/update-profile', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/profile/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/profile/index', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/profile/show', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/recovery/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/recovery/request', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/recovery/reset', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/confirm', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/connect', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/organizer', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/register', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/resend', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/security/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/security/auth', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/security/login', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/security/logout', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/account', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/confirm', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/delete', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/disconnect', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/networks', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/profile', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('access-control', 2, NULL, NULL, NULL, 1506327725, 1506327725),
('admin', 1, 'Administrator', NULL, NULL, 1493684477, 1493684477),
('homepage', 2, 'Homepage', NULL, NULL, 1522486941, 1522486941),
('support', 1, 'Support', NULL, NULL, 1535282960, 1535282960),
('user-profile', 2, NULL, NULL, NULL, 1506327698, 1506327698);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('access-control', '/admin/*'),
('admin', '/departments/*'),
('admin', 'access-control'),
('admin', 'homepage'),
('admin', 'manage-announcement'),
('admin', 'manage-department'),
('admin', 'manage-document'),
('admin', 'manage-hod'),
('admin', 'manage-meeting'),
('admin', 'manage-meeting-category'),
('admin', 'manage-pekeliling'),
('admin', 'manage-profil'),
('admin', 'manage-region'),
('admin', 'manage-user'),
('admin', 'user-profile'),
('homepage', '/site/*'),
('support', 'homepage'),
('user-profile', '/user/profile/*'),
('user-profile', '/user/profile/index'),
('user-profile', '/user/profile/show'),
('user-profile', '/user/registration/*'),
('user-profile', '/user/registration/confirm'),
('user-profile', '/user/registration/connect'),
('user-profile', '/user/registration/organizer'),
('user-profile', '/user/registration/register'),
('user-profile', '/user/registration/resend'),
('user-profile', '/user/security/*'),
('user-profile', '/user/security/auth'),
('user-profile', '/user/settings/*'),
('user-profile', '/user/settings/account'),
('user-profile', '/user/settings/confirm'),
('user-profile', '/user/settings/delete'),
('user-profile', '/user/settings/disconnect'),
('user-profile', '/user/settings/networks'),
('user-profile', '/user/settings/profile');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `desc` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `desc`, `status`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'Management', 'Company management', 1, 1, '2018-05-17 22:18:57', 1, '2018-08-26 10:18:25'),
(2, 'Support', 'Company Support', 1, 1, '2018-08-26 18:55:44', 1, '2018-08-26 10:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `office_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`, `image_id`, `department_id`, `office_no`, `work_no`, `company`) VALUES
(1, 'Faiz Samad', 'faiz@hpcs.my', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', 'Asia/Kuala_Lumpur', NULL, NULL, NULL, NULL, NULL),
(2, 'HPCS Support', 'support@hpcs.my', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', 'Asia/Kuala_Lumpur', NULL, NULL, '0358873341 ', '0103314117', 'HPCS Sdn Bhd');

-- --------------------------------------------------------

--
-- Table structure for table `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`, `status`, `password_reset_token`) VALUES
(1, 'faiz', 'faiz@hpcs.my', '$2y$10$2mJEVXVMfP7nr17kY6Ko2OLZcilPFE0OgWFU1TG4OMAZT3zE1mRlW', 'afrAn7_2CIX2Uuk5GHWxAMvyepfz-Srg', 1505969126, NULL, NULL, NULL, 1505969114, 1535211919, 0, 1535259279, 10, NULL),
(2, 'support', 'support@hpcs.my', '$2y$10$wMFbJ1dqUfOKpDVgD5RqFeCh.pfGFCQ.4YZR298KUDX65YcH9k7ti', 'Nenf8C7LQsruH7ChIu-fb6XLdKVgfiXq', 1505969126, NULL, NULL, NULL, 1505969114, 1535282818, 0, 1535283041, 10, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
