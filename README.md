<p align="center">
    <h1 align="center">Islamic Tourism System</h1>
    <br>
</p>

CONFIGURATION
-------------

### Database

Copy config/db.php.bak to config/db.php and edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=islamic_tourism',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```

### System Installation

To start the installation:  

1. clone from repository

    ```
    git clone git@bitbucket.org:faizsamad/islamic_tourism.git
    ```

2. Go to system folder (example : cd /var/www/islamic_tourism)

3. Update dependencies with Composer 

    ```
    composer update  
    ```

4. Run the Yii migrate command

    ```
    yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations
    yii migrate --migrationPath=@yii/rbac/migrations
    yii migrate
    ```