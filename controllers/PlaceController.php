<?php

namespace app\controllers;

use Yii;
use app\models\Place;
use app\models\PlaceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use app\models\PlaceCategory;
use app\models\PlaceIslamicElement;
use app\models\PlaceImage;
use app\models\PlaceOperationHour;
use app\models\Model;
use app\models\Category;
use app\models\IslamicElements;

/**
 * PlaceController implements the CRUD actions for Place model.
 */
class PlaceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Place models.
     * @return mixed
     */
    public function actionIndex()
    {   
        $status = Yii::$app->request->get('status');

        $searchModel = new PlaceSearch();
        $searchModel->status = $status;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Place model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        $model = $this->findModel($id);

        $placeCategories = PlaceCategory::find()->where(['place_id' => $id])->one();

        $placeElements = PlaceIslamicElement::find()->where(['place_id' => $id])->one();

        $operationHours = $model->placeOperationHours;

        $images = $model->placeImages;

        return $this->render('view', [
            'model' => $model,
            'placeCategories' => $placeCategories,
            'placeElements' => $placeElements,
            'operationHours' => $operationHours,
            'images' => $images
        ]);
    }

    /**
     * Creates a new Place model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Place();

        $placeCategories = new PlaceCategory();

        $placeElements = new PlaceIslamicElement();

        $placeImages = [new PlaceImage]; // for inserting multiple image

        $placeOperationHours[0] = new PlaceOperationHour;
        $placeOperationHours[1] = new PlaceOperationHour;
        $placeOperationHours[2] = new PlaceOperationHour;
        $placeOperationHours[3] = new PlaceOperationHour;
        $placeOperationHours[4] = new PlaceOperationHour;
        $placeOperationHours[5] = new PlaceOperationHour;
        $placeOperationHours[6] = new PlaceOperationHour;

        if ($model->load(Yii::$app->request->post())) { 

            $model->spending_type_id = 1;
            $model->status = 3;

            // place image
            $placeImages = Model::createMultiple(PlaceImage::className());
            Model::loadMultiple($placeImages, Yii::$app->request->post());
            foreach ($placeImages as $index => $image) {
                $image->detail = ($image->detail=='') ? '-':$image->detail;
                $imageFiles = \yii\web\UploadedFile::getInstance($image, "[{$index}]img");
                $image->img = $imageFiles;

            }

            // place operation hour
            $placeOperationHours = Model::createMultiple(PlaceOperationHour::className());
            Model::loadMultiple($placeOperationHours, Yii::$app->request->post());
            foreach ($placeOperationHours as $index => $hour) {
                $hour->day_id = ($hour->day_id=='') ? '-':$hour->day_id;
                $hour->start_time = ($hour->start_time=='') ? '-':$hour->start_time;
                $hour->end_time = ($hour->end_time=='') ? '-':$hour->end_time;

            }

            $valid = $model->validate();
            $valid = Model::validateMultiple($placeImages) && $valid;

            if ($valid) {
                $transaction = Yii::$app->db->beginTransaction();
                try {

                    if ($flag = $model->save(false)) {
                        foreach ($placeImages as $image) {

                            $upload_path = \Yii::getAlias('@app/web/upload');
                            if (!is_dir($upload_path)) {
                                mkdir($upload_path, 0777, true);
                            }

                            $place_image_path = \Yii::getAlias('@app/web/upload/place_image');
                            if (!is_dir($place_image_path)) {
                                mkdir($place_image_path, 0777, true);
                            }

                            $directory = $place_image_path . DIRECTORY_SEPARATOR . $model->id . DIRECTORY_SEPARATOR;

                            if (!is_dir($directory)) {
                                mkdir($directory,0777,true);
                            }

                            // if ada file
                            if($image->img) {

                                $uid = uniqid(time(), true);
                                $fileName = $uid . '.' . $image->img->extension;
                                $filePath = $directory . $fileName;
                                $fileSize = (string) $image->img->size;
                                $fileType = $image->img->type;
                                $placeId = $model->id;
                                $placeDetail = $image->detail;

                                if ($image->img->saveAs($filePath)) {
                                    $path = '/upload/place_image/' . $model->id . DIRECTORY_SEPARATOR . $fileName;

                                    // insert into table place image
                                    $image = new PlaceImage;
                                    $image->file_path = $path;
                                    $image->file_size = $fileSize;
                                    $image->file_type = $fileType;
                                    $image->file_name = $fileName;
                                    $image->place_id = $placeId; // set place id
                                    $image->detail = $placeDetail;

                                    if (!$image->save()) {
                                        print_r($image->errors);exit;
                                    }
                                }

                                if ($image->detail) {
                                    if (($flag = $image->save(false)) === false) {
                                        $transaction->rollBack();
                                        break;
                                    }
                                }
                            }

                        } // end foreach $placeImages

                        // insert into table place operation hour
                        foreach ($placeOperationHours as $hour) {

                            $hour->place_id = $model->id;
                            $hour->day_id = $hour->day_id;
                            $hour->start_time = date("H:i", strtotime($hour->start_time));
                            $hour->end_time = date("H:i", strtotime($hour->end_time));

                            if (!$hour->save()) {
                                print_r($hour->errors);exit;
                            }

                            if (($flag = $hour->save(false)) === false) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                        // loop group category and simpan dalam string
                        $post_category = $_POST['PlaceCategory']['category_id'];
                        if(!empty($post_category)) {
                            $category_string = implode (",", $post_category);

                            $placeCategories->category_id = $category_string;
                            $placeCategories->place_id = $model->id;
                            $placeCategories->save();
                        }

                        // loop islamic element and simpan dalam string
                        $post_element = $_POST['PlaceIslamicElement']['islamic_element_id'];
                        if(!empty($post_element)) {
                            $element_string = implode (",", $post_element);

                            $placeElements->islamic_element_id = $element_string;
                            $placeElements->place_id = $model->id;
                            $placeElements->save();
                        }
                    }

                    if ($flag) {
                        $transaction->commit();

                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'New Place Added'));
                        return $this->redirect(['view', 'id' => $model->id]);

                    }

                } catch (Exception $e) {

                    $transaction->rollBack();

                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'placeCategories' => $placeCategories,
            'placeElements' => $placeElements,
            'placeImages' => (empty($placeImages)) ? [new PlaceImage] : $placeImages,
            'categories' => $this->getCategoryList(),
            'elements' => $this->getElementList(),
            'placeOperationHours' => (empty($placeOperationHours)) ? [new PlaceOperationHour] : $placeOperationHours
        ]);
    }

    /**
     * Updates an existing Place model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        // $placeCategories = $model->placeCategories;
        $placeCategories = PlaceCategory::find()->where(['place_id' => $id])->one();
        if (empty($placeCategories)) {
            $placeCategories = new PlaceCategory();

        } else {
            $placeCategories->category_id = explode(',',$placeCategories->category_id);

        }

        $placeElements = PlaceIslamicElement::find()->where(['place_id' => $id])->one();
        if (empty($placeElements)) {
            $placeElements = new PlaceIslamicElement();

        } else {
            $placeElements->islamic_element_id = explode(',',$placeElements->islamic_element_id);

        }

        $placeImages = $model->placeImages;
        if (empty($placeImages)) {
            $placeImages = [new PlaceImage];

        }

        $placeOperationHours = $model->placeOperationHours;
        if (empty($placeOperationHours)) {
            
            $placeOperationHours[0] = new PlaceOperationHour;
            $placeOperationHours[1] = new PlaceOperationHour;
            $placeOperationHours[2] = new PlaceOperationHour;
            $placeOperationHours[3] = new PlaceOperationHour;
            $placeOperationHours[4] = new PlaceOperationHour;
            $placeOperationHours[5] = new PlaceOperationHour;
            $placeOperationHours[6] = new PlaceOperationHour;

        }

        if ($model->load(Yii::$app->request->post())) {

            $model->updated_date = date('Y-m-d H:i:s');
            $model->updated_by = Yii::$app->user->id;

            // find old place image
            $oldIDs = ArrayHelper::map($placeImages, 'id', 'id');
            $placeImages = Model::createMultiple(PlaceImage::className(), $placeImages);
            Model::loadMultiple($placeImages, Yii::$app->request->post());

            $deleteIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($placeImages, 'id', 'id')));
            $filterEmptyArray = array_filter($deleteIDs);   // filter empty array

            foreach ($placeImages as $index => $image) {
                $image->detail = ($image->detail=='') ? '-':$image->detail;
                $imageFiles = \yii\web\UploadedFile::getInstance($image, "[{$index}]img");
                $image->img = $imageFiles;

            }

            // fing old place operation hour
            $oldHourIDs = ArrayHelper::map($placeOperationHours, 'id', 'id');
            $placeOperationHours = Model::createMultiple(PlaceOperationHour::className(), $placeOperationHours);
            Model::loadMultiple($placeOperationHours, Yii::$app->request->post());

            $deleteHourIDs = array_diff($oldHourIDs, array_filter(ArrayHelper::map($placeOperationHours, 'id', 'id')));
            $filterEmptyHourArray = array_filter($deleteHourIDs);   // filter empty array

            foreach ($placeOperationHours as $index => $hour) {
                $hour->day_id = ($hour->day_id=='') ? '-':$hour->day_id;
                $hour->start_time = ($hour->start_time=='') ? '00:00:00':$hour->start_time;
                $hour->end_time = ($hour->end_time=='') ? '00:00:00':$hour->end_time;

            }

            $valid = $model->validate();
            $valid = Model::validateMultiple($placeImages) && $valid;

            if ($valid) {
                $transaction = Yii::$app->db->beginTransaction();

                try {

                    if ($flag = $model->save(false)) {

                        // Delete photo
                        if (!empty($filterEmptyArray)) { 

                            foreach ($deleteIDs as $deleteId) {

                                $delete_model = PlaceImage::findOne($deleteId);
                                $delete_model->delete();

                            }
                        }

                        foreach ($placeImages as $image) {

                            $upload_path = \Yii::getAlias('@app/web/upload');
                            if (!is_dir($upload_path)) {
                                mkdir($upload_path, 0777, true);
                            }

                            $place_image_path = \Yii::getAlias('@app/web/upload/place_image');
                            if (!is_dir($place_image_path)) {
                                mkdir($place_image_path, 0777, true);
                            }

                            $directory = $place_image_path . DIRECTORY_SEPARATOR . $model->id . DIRECTORY_SEPARATOR;

                            if (!is_dir($directory)) {
                                mkdir($directory,0777,true);
                            }

                            // if ada file
                            if($image->img) {

                                $uid = uniqid(time(), true);
                                $fileName = $uid . '.' . $image->img->extension;
                                $filePath = $directory . $fileName;
                                $fileSize = (string) $image->img->size;
                                $fileType = $image->img->type;
                                $placeId = $model->id;
                                $placeDetail = $image->detail;

                                if ($image->img->saveAs($filePath)) {
                                    $path = '/upload/place_image/' . $model->id . DIRECTORY_SEPARATOR . $fileName;

                                    // insert into table place image
                                    $image = new PlaceImage;
                                    $image->file_path = $path;
                                    $image->file_size = $fileSize;
                                    $image->file_type = $fileType;
                                    $image->file_name = $fileName;
                                    $image->place_id = $placeId; // set place id
                                    $image->detail = $placeDetail;

                                    if (!$image->save()) {
                                        print_r($image->errors);exit;
                                    }
                                }

                            }
                            if ($image->detail) {
                                if (($flag = $image->save(false)) === false) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        } // end foreach $placeImages

                        // Delete operation hour
                        if (!empty($filterEmptyHourArray)) { 

                            foreach ($deleteHourIDs as $deleteHourId) {

                                $delete_model = PlaceOperationHour::findOne($deleteHourId);
                                $delete_model->delete();

                            }
                        }

                            // print('<pre>'.print_r($placeOperationHours,true).'</pre>'); die;
                        foreach ($placeOperationHours as $hours) {
                            $hour = new PlaceOperationHour;
                            $hour->place_id = $id;
                            $hour->day_id = $hours->day_id;
                            $hour->start_time = date("H:i", strtotime($hours->start_time));
                            $hour->end_time = date("H:i", strtotime($hours->end_time));
                            $hour->save();
                        }

                        // loop group category and simpan dalam string            
                        $post_category = $_POST['PlaceCategory']['category_id'];
                        if(!empty($post_category)) {
                            $category_string = implode (",", $post_category);

                            $placeCategories->place_id = $model->id;
                            $placeCategories->category_id = $category_string;
                            $placeCategories->save();
                        }

                        // loop islamic element and simpan dalam string
                        $post_element = $_POST['PlaceIslamicElement']['islamic_element_id'];
                        if(!empty($post_element)) {
                            $element_string = implode (",", $post_element);

                            $placeElements->place_id = $model->id;
                            $placeElements->islamic_element_id = $element_string;
                            $placeElements->save();
                        }
                    }

                    if ($flag) {
                        $transaction->commit();

                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Place Been Updated'));
                        return $this->redirect(['view', 'id' => $model->id]);

                    }

                } catch (Exception $e) {

                    $transaction->rollBack();

                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'placeCategories' => $placeCategories,
            'placeElements' => $placeElements,
            'placeImages' => $placeImages,
            'placeOperationHours' => $placeOperationHours,
            'categories' => $this->getCategoryList(),
            'elements' => $this->getElementList(),
        ]);
    }

    /**
     * get category list
     *
     */
    public function getCategoryList() {

        $model = Category::find()->where(['status'=>1])->all();

        return $model;
    }

    /**
     * get element list
     *
     */
    public function getElementList() {

        $model = IslamicElements::find()->where(['status'=>1])->all();

        return $model;
    }

    /**
     * Download
     *
     * @param $id
     */
    public function actionDownload($id) {

        $model = PlaceImage::findOne($id);

        $download_path = \Yii::getAlias('@app/web') . $model->file_path;

        if (file_exists($download_path)) {
            Yii::$app->response->sendFile($download_path);
        }
    } // end actionDownload

    /**
     * Deletes an existing Place model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Place model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Place the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Place::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

