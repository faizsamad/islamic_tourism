<?php

namespace app\controllers\api;

use Yii;
use app\models\Place;
use app\models\BaseFunctions;
use app\models\Category;

use app\models\Days;
use app\models\States;
use app\models\PlaceImage;
use app\models\PlaceOperationHour;
use app\models\PlaceIslamicElement;
use app\models\IslamicElements;
use app\models\RatePlace;


class PlaceController extends ApiBaseController
{

    public function actionState()
    {
        $data = States::find()->asArray()->all();

        return $this->response(200, $data, true, 'OK');
    }

    public function actionList()
    {
        $model = Place::find()->where(['status' => 3])->all(); // status open

        $data = [];
        foreach ($model as $key => $value)
        {
            $data[$key]['id'] = $value->id;
            $data[$key]['name'] = $value->name;
            $data[$key]['address1'] = $value->address1;
            $data[$key]['address2'] = (isset($value->address2)) ? $value->address2:'-';
            $data[$key]['address3'] = (isset($value->address3)) ? $value->address3:'-';
            $data[$key]['poscode'] = $value->poscode;
            $data[$key]['city'] = $value->city;
            $data[$key]['state_id'] = $value->state_id;
            $data[$key]['state'] = $value->state->name;
            $data[$key]['contact_no'] = (isset($value->contact_no)) ? $value->contact_no:'-';
            $data[$key]['duration'] = (isset($value->duration)) ? BaseFunctions::durations($value->duration):'-';
            $data[$key]['price_range'] = (isset($value->price_range)) ? nl2br($value->price_range):'-';
            $data[$key]['parking_type_id'] = $value->parkingType->name;
            $data[$key]['spending_type_id'] = (isset($value->spending_type_id)) ? $value->spendingType->name:'-';
            $data[$key]['detail'] = (isset($value->detail)) ? nl2br($value->detail):'-';
            $data[$key]['short_detail'] = (isset($value->detail)) ? substr(nl2br($value->detail),0, 200):'-';
            $data[$key]['latitude'] = $value->latitude;
            $data[$key]['longitude'] = $value->longitude;
            $data[$key]['url'] = (isset($value->url)) ? $value->url:'-';

            $images = PlaceImage::find()->where(['place_id' => $value->id])->all(); // status open
            foreach ($images as $k => $image) {
                $data[$key]['images'][$k]['image_url'] = \Yii::$app->urlManager->createAbsoluteUrl($image->file_path);
                $data[$key]['images'][$k]['detail'] = $image->detail;
            }
        }

        return $this->response(200, $data, true, 'OK');
    }

    public function actionTest() {

        $request = Yii::$app->request;
        $id = $request->getBodyParam('id');
        return $this->response(200, $id . "<- ID");
    }

    public function actionSearch() {

        $request = Yii::$app->request;
        $categories = $request->getBodyParam('categories');
        $categories = explode(",", $categories);
        $spending = $request->getBodyParam('spending');
        $from = $request->getBodyParam('from');
        $to = $request->getBodyParam('to');
        $lat = $request->getBodyParam('lat');
        $long = $request->getBodyParam('long');

        $date1 = new \DateTime($from, new \DateTimeZone('Asia/Kuala_Lumpur'));
        $date2 = new \DateTime($to, new \DateTimeZone('Asia/Kuala_Lumpur'));
        $diff_date = $date2->diff($date1)->format("%d");
        $radius = 0;

        switch ($diff_date) {
            case 0:
                $radius = 25;
                break;
            case 1:
                $radius = 50;
                break;
            case 2:
                $radius = 75;
                break;
            default:
                $radius = 150;
        }

        $places = Place::find()->with(['placeCategories', 'placeImages', 'placeOperationHours', 'placeIslamicElements'])->where(['spending_type_id' => $spending, 'status' => 3])->asArray()->all();

        $data = [];
        $i = 0; $sumRate = 0; $placeRate = 0;
        foreach ($places as $key =>  $place) {

            // get distance
            $distance = $this->distance($place['latitude'], $place['longitude'], $lat, $long, 'K');

            $placeCategories = $place['placeCategories']['category_id'];

            $placeIslamics = PlaceIslamicElement::find()->where(['place_id' => $place['id']])->one();
            if (!empty($placeIslamics)) {

                $placeIslamics = $placeIslamics->islamic_element_id;

            } else {
                $placeIslamics = '';
            }

            // calculate total rates
            $placeRates = RatePlace::find()->where(['place_id' => $place['id']])->all();
            $rateCount = count($placeRates);
            if (!empty($placeRates)) {

                foreach ($placeRates as $key => $value) {
                    $sumRate = $sumRate += $value->star;
                }
                $avgRate = $sumRate / $rateCount;
                $placeRate = $avgRate;

            } else {
                $placeRate = $sumRate;
            }

            $state = States::find()->where(['id' => $place['state_id']])->one();

            $placeOperationHours = $place['placeOperationHours'];

            $has_category = false;
            if (count($categories) > 0) {
                foreach ($categories as $category) {
                    if (strpos($placeCategories, $category) !== false) {
                        $has_category = true;
                    }
                }
            }

            if ($has_category == false) {
                continue;
            }

            // yang mana distance by hari
            if ($distance <= $radius) {

                $data[$i] = $place;

                $val_categories = explode(",", $placeCategories);

                $list_categories = [];
                foreach ($val_categories as $k_cat => $v_cat) {

                    $cat = Category::findOne($v_cat);
                    $list_categories[$k_cat]['id'] = $cat->id;
                    $list_categories[$k_cat]['name'] = $cat->name;
                }

                $list_elements = [];
                if ($placeIslamics != '') {
                    $val_elements = explode(",", $placeIslamics);
                    $count_element = count($val_elements);

                    foreach ($val_elements as $k_ele => $v_ele) {

                        $element = IslamicElements::findOne($v_ele);
                        $list_elements[$k_ele]['id'] = $element->id;
                        $list_elements[$k_ele]['name'] = $element->name;
                    }
                }

                $operation_hours = [];
                foreach ($placeOperationHours as $k_oh => $v_oh) {

                    $day = Days::findOne($v_oh['day_id']);
                    $operation_hours[$k_oh]['id'] = $day->id;
                    $operation_hours[$k_oh]['name'] = $day->name;
                    $operation_hours[$k_oh]['open'] = date("h:i A", strtotime($v_oh['start_time']));
                    $operation_hours[$k_oh]['close'] = date("h:i A", strtotime($v_oh['end_time']));
                }   

                $today = BaseFunctions::today(date('l'));
                $now = date("H:i:s");
                $openHour = PlaceOperationHour::find()->where(['place_id' => $place['id'], 'day_id' => $today])->one();
                $start = date("H:i:s", strtotime($openHour['start_time']));
                $end = date("H:i:s", strtotime($openHour['end_time']));

                if ($now >= $start && $now <= $end) {
                    $status_open = 'Open';

                } else {
                    $status_open = 'Closed';

                }
                $data[$i]['status_open'] = $status_open;

                $short = 'No description...';

                if  ($place['detail']) {
                    $short = substr(nl2br($place['detail']),0, 150) . '...';
                }

                $data[$i]['short_detail'] = $short;
                $data[$i]['list_categories'] = $list_categories;
                $data[$i]['list_elements'] = $list_elements;
                $data[$i]['operation_hours'] = $operation_hours;
                $data[$i]['distance'] = number_format((float)round( $distance ,2, PHP_ROUND_HALF_DOWN),2,'.',',') . 'KM';
                $data[$i]['sort_distance'] = $distance;
                $data[$i]['rating'] = round($placeRate);
                $data[$i]['rating_count'] = $rateCount;
                $data[$i]['state'] = $state->name;

                $images = $place['placeImages'];
                $data[$i]['images'] = [];
                foreach ($images as $index => $image) {
                    $data[$i]['images'][$index]['image_url'] = \Yii::$app->urlManager->createAbsoluteUrl($image['file_path']);
                }
                $i++;
            }
        }

        $keys = array_column($data, 'sort_distance');
        $result = array_multisort($keys, SORT_ASC, $data);
        $response['lists'] = $data;
        $response['total'] = count($data);
        $response['spending'] = $spending;
        $response['day'] = $diff_date;
        $response['radius'] = $radius;

        return $this->response(200, $response, true, 'OK');
    }

    /**
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @param $unit
     * @return float|int
     *
     * calculate distance
     */
    protected function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /**
     * @param $lat1
     * @param $lat2
     * @param $long1
     * @param $long2
     * @return array
     *
     * Get driving distance and time using google API
     */
    function getDrivingDistance($lat1, $lat2, $long1, $long2)
    {
        $key = 'AIzaSyBS3mPquixj80LrbIPmhLjwWvl_MrTvQ7g';
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&key=".$key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $dist = (isset($response_a['rows'][0]['elements'][0]['distance'])) ? $response_a['rows'][0]['elements'][0]['distance']['text']: '';
        $time = (isset($response_a['rows'][0]['elements'][0]['duration'])) ? $response_a['rows'][0]['elements'][0]['duration']['text']:'';

        return array('distance' => $dist, 'time' => $time);
    }
}