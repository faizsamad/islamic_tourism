<?php

namespace app\controllers\api;

use Yii;
use app\models\BaseFunctions;
use app\models\Category;
use app\models\SpendingType;
use app\models\Place;
use app\models\PlaceCategory;
use app\models\PlaceImage;
use app\models\PlaceOperationHour;
use app\models\PlaceIslamicElement;
use app\models\IslamicElements;
use app\models\Days;
use app\models\RatePlace;
use app\models\States;

class DataController extends ApiBaseController
{
    /**
     * Return Category list
     */
    public function actionCategory()
    {
        $data = Category::find()->asArray()->all();

        return $this->response(200, $data);
    }

    public function actionSpending() {
        $data = SpendingType::find()->asArray()->all();

        return $this->response(200, $data);
    }

    public function actionPlace() {

        $places = Place::find()->with(['placeCategories', 'placeImages', 'placeOperationHours', 'placeIslamicElements'])->where(['status' => 3])->asArray()->all();

        $data = [];
        $i = 0;
        foreach ($places as $key =>  $place) {

            $lower = $place['city'] = strtolower($place['city']);
            $place['city'] = ucwords($lower);

            $placeCategories = $place['placeCategories']['category_id'];

            $placeIslamics = PlaceIslamicElement::find()->where(['place_id' => $place['id']])->one();
            if (!empty($placeIslamics)) {

                $placeIslamics = $placeIslamics->islamic_element_id;

            } else {
                $placeIslamics = '';
            }

            $placeOperationHours = $place['placeOperationHours'];

            $data[$i] = $place;

            $val_categories = explode(",", $placeCategories);

            $list_categories = [];
            foreach ($val_categories as $k_cat => $v_cat) {

                $cat = Category::findOne($v_cat);
                $list_categories[$k_cat]['id'] = $cat->id;
                $list_categories[$k_cat]['name'] = $cat->name;
            }

            $list_elements = [];
            if ($placeIslamics != '') {
                $val_elements = explode(",", $placeIslamics);
                $count_element = count($val_elements);

                foreach ($val_elements as $k_ele => $v_ele) {

                    $element = IslamicElements::findOne($v_ele);
                    $list_elements[$k_ele]['id'] = $element->id;
                    $list_elements[$k_ele]['name'] = $element->name;
                }
            }

            $operation_hours = [];
            foreach ($placeOperationHours as $k_oh => $v_oh) {

                $day = Days::findOne($v_oh['day_id']);
                $operation_hours[$k_oh]['id'] = $day->id;
                $operation_hours[$k_oh]['name'] = $day->name;
                $operation_hours[$k_oh]['open'] = date("h:i A", strtotime($v_oh['start_time']));
                $operation_hours[$k_oh]['close'] = date("h:i A", strtotime($v_oh['end_time']));
            }   

            $today = BaseFunctions::today(date('l'));
            $now = date("H:i:s");
            $openHour = PlaceOperationHour::find()->where(['place_id' => $place['id'], 'day_id' => $today])->one();
            $start = date("H:i:s", strtotime($openHour['start_time']));
            $end = date("H:i:s", strtotime($openHour['end_time']));

            if ($now >= $start && $now <= $end) {
                $status_open = 'Open';
            } else {
                $status_open = 'Closed';
            }

            $data[$i]['status_open'] = $status_open;
            $data[$i]['list_categories'] = $list_categories;
            $data[$i]['list_elements'] = $list_elements;
            $data[$i]['operation_hours'] = $operation_hours;

            $images = $place['placeImages'];
            $data[$i]['images'] = [];
            foreach ($images as $index => $image) {
                $data[$i]['images'][$index]['image_url'] = \Yii::$app->urlManager->createAbsoluteUrl($image['file_path']);
            }
            $i++;
        }

        $response['lists'] = $data;
        $response['total'] = count($data);

        return $this->response(200, $response, true, 'OK');
    }

    public function actionHotel() {

        $hotels = PlaceCategory::find()->where(['LIKE', 'category_id', '%16%', false])->all();

        $data = [];
        $i = 0; $sumRate = 0; $placeRate = 0;
        if (!empty($hotels)) {

            foreach ($hotels as $hotel) {
                $hotel_id[] = $hotel->place_id;
            }

            $places = Place::find()->with(['placeCategories', 'placeImages', 'placeOperationHours', 'placeIslamicElements'])->where(['id' =>$hotel_id, 'status' => 3])->asArray()->all();

            foreach ($places as $key =>  $place) {

                $placeIslamics = PlaceIslamicElement::find()->where(['place_id' => $place['id']])->one();
                if (!empty($placeIslamics)) {

                    $placeIslamics = $placeIslamics->islamic_element_id;

                } else {
                    $placeIslamics = '';
                }

                // calculate total rates
                $placeRates = RatePlace::find()->where(['place_id' => $place['id']])->all();
                $rateCount = count($placeRates);
                if (!empty($placeRates)) {

                    foreach ($placeRates as $key => $value) {
                        $sumRate = $sumRate += $value->star;
                    }
                    $avgRate = $sumRate / $rateCount;
                    $placeRate = $avgRate;

                } else {
                    $placeRate = $sumRate;
                }
                
                $state = States::find()->where(['id' => $place['state_id']])->one();

                $placeOperationHours = $place['placeOperationHours'];

                $data[$i] = $place;

                $placeCategories = $place['placeCategories']['category_id'];
                $val_categories = explode(",", $placeCategories);

                $list_categories = [];
                foreach ($val_categories as $k_cat => $v_cat) {

                    $cat = Category::findOne($v_cat);
                    $list_categories[$k_cat]['id'] = $cat->id;
                    $list_categories[$k_cat]['name'] = $cat->name;
                }

                $list_elements = [];
                if ($placeIslamics != '') {
                    $val_elements = explode(",", $placeIslamics);
                    $count_element = count($val_elements);

                    foreach ($val_elements as $k_ele => $v_ele) {

                        $element = IslamicElements::findOne($v_ele);
                        $list_elements[$k_ele]['id'] = $element->id;
                        $list_elements[$k_ele]['name'] = $element->name;
                    }
                }

                $operation_hours = [];
                foreach ($placeOperationHours as $k_oh => $v_oh) {

                    $day = Days::findOne($v_oh['day_id']);
                    $operation_hours[$k_oh]['id'] = $day->id;
                    $operation_hours[$k_oh]['name'] = $day->name;
                    $operation_hours[$k_oh]['open'] = date("h:i A", strtotime($v_oh['start_time']));
                    $operation_hours[$k_oh]['close'] = date("h:i A", strtotime($v_oh['end_time']));
                }   

                $today = BaseFunctions::today(date('l'));
                $now = date("H:i:s");
                $openHour = PlaceOperationHour::find()->where(['place_id' => $place['id'], 'day_id' => $today])->one();
                $start = date("H:i:s", strtotime($openHour['start_time']));
                $end = date("H:i:s", strtotime($openHour['end_time']));

                if ($now >= $start && $now <= $end) {
                    $status_open = 'Open';

                } else {
                    $status_open = 'Closed';

                }
                $data[$i]['status_open'] = $status_open;

                $short = 'No description...';

                if  ($place['detail']) {
                    $short = substr(nl2br($place['detail']),0, 150) . '...';
                }

                $data[$i]['short_detail'] = $short;
                $data[$i]['list_categories'] = $list_categories;
                $data[$i]['list_elements'] = $list_elements;
                $data[$i]['operation_hours'] = $operation_hours;
                $data[$i]['rating'] = round($placeRate);
                $data[$i]['rating_count'] = $rateCount;
                $data[$i]['state'] = $state->name;

                $images = $place['placeImages'];
                $data[$i]['images'] = [];
                foreach ($images as $index => $image) {
                    $data[$i]['images'][$index]['image_url'] = \Yii::$app->urlManager->createAbsoluteUrl($image['file_path']);
                }
                $i++;
            }
        }

        $response['lists'] = $data;
        $response['total'] = count($data);

        return $this->response(200, $response, true, 'OK');
    }

    public function actionRestaurant() {

        $restaurants = PlaceCategory::find()->where(['LIKE', 'category_id', '%4%', false])->all();

        $data = [];
        $i = 0; $sumRate = 0; $placeRate = 0;
        if (!empty($restaurants)) {

            foreach ($restaurants as $restaurant) {
                $restaurant_id[] = $restaurant->place_id;
            }

            $places = Place::find()->with(['placeCategories', 'placeImages', 'placeOperationHours', 'placeIslamicElements'])->where(['id' =>$restaurant_id, 'status' => 3])->asArray()->all();

            foreach ($places as $key =>  $place) {

                $placeIslamics = PlaceIslamicElement::find()->where(['place_id' => $place['id']])->one();
                if (!empty($placeIslamics)) {

                    $placeIslamics = $placeIslamics->islamic_element_id;

                } else {
                    $placeIslamics = '';
                }

                // calculate total rates
                $placeRates = RatePlace::find()->where(['place_id' => $place['id']])->all();
                $rateCount = count($placeRates);
                if (!empty($placeRates)) {

                    foreach ($placeRates as $key => $value) {
                        $sumRate = $sumRate += $value->star;
                    }
                    $avgRate = $sumRate / $rateCount;
                    $placeRate = $avgRate;

                } else {
                    $placeRate = $sumRate;
                }
                
                $state = States::find()->where(['id' => $place['state_id']])->one();

                $placeOperationHours = $place['placeOperationHours'];

                $data[$i] = $place;

                $placeCategories = $place['placeCategories']['category_id'];
                $val_categories = explode(",", $placeCategories);

                $list_categories = [];
                foreach ($val_categories as $k_cat => $v_cat) {

                    $cat = Category::findOne($v_cat);
                    $list_categories[$k_cat]['id'] = $cat->id;
                    $list_categories[$k_cat]['name'] = $cat->name;
                }

                $list_elements = [];
                if ($placeIslamics != '') {
                    $val_elements = explode(",", $placeIslamics);
                    $count_element = count($val_elements);

                    foreach ($val_elements as $k_ele => $v_ele) {

                        $element = IslamicElements::findOne($v_ele);
                        $list_elements[$k_ele]['id'] = $element->id;
                        $list_elements[$k_ele]['name'] = $element->name;
                    }
                }

                $operation_hours = [];
                foreach ($placeOperationHours as $k_oh => $v_oh) {

                    $day = Days::findOne($v_oh['day_id']);
                    $operation_hours[$k_oh]['id'] = $day->id;
                    $operation_hours[$k_oh]['name'] = $day->name;
                    $operation_hours[$k_oh]['open'] = date("h:i A", strtotime($v_oh['start_time']));
                    $operation_hours[$k_oh]['close'] = date("h:i A", strtotime($v_oh['end_time']));
                }   

                $today = BaseFunctions::today(date('l'));
                $now = date("H:i:s");
                $openHour = PlaceOperationHour::find()->where(['place_id' => $place['id'], 'day_id' => $today])->one();
                $start = date("H:i:s", strtotime($openHour['start_time']));
                $end = date("H:i:s", strtotime($openHour['end_time']));

                if ($now >= $start && $now <= $end) {
                    $status_open = 'Open';

                } else {
                    $status_open = 'Closed';

                }
                $data[$i]['status_open'] = $status_open;

                $short = 'No description...';

                if  ($place['detail']) {
                    $short = substr(nl2br($place['detail']),0, 150) . '...';
                }

                $data[$i]['short_detail'] = $short;
                $data[$i]['list_categories'] = $list_categories;
                $data[$i]['list_elements'] = $list_elements;
                $data[$i]['operation_hours'] = $operation_hours;
                $data[$i]['rating'] = round($placeRate);
                $data[$i]['rating_count'] = $rateCount;
                $data[$i]['state'] = $state->name;

                $images = $place['placeImages'];
                $data[$i]['images'] = [];
                foreach ($images as $index => $image) {
                    $data[$i]['images'][$index]['image_url'] = \Yii::$app->urlManager->createAbsoluteUrl($image['file_path']);
                }
                $i++;
            }
        }

        $response['lists'] = $data;
        $response['total'] = count($data);

        return $this->response(200, $response, true, 'OK');
    }
}