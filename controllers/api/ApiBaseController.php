<?php
/**
 * Created by PhpStorm.
 * User: haezalmusa
 * Date: 28/07/2018
 * Time: 8:34 AM
 */
namespace app\controllers\api;

use Yii;
use yii\rest\Controller;

class ApiBaseController extends Controller
{
    public function response($code, $data = '')
    {
        $response = array();
        $message = $this->getStatusCodeMessage($code);
        if (!empty($message)) {
            $response = $data;
        }
        $this->setHeader($code);

        echo json_encode($response);die;
    }

    private function getStatusCodeMessage($status)
    {
        $codes = Array(
            // Success 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            509 => 'Bandwidth Limit Exceeded',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }


    private function setHeader($status)
    {
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->getStatusCodeMessage($status);
        $content_type = "application/json; charset=utf-8";
        header($status_header);
        header('Content-type: ' . $content_type);
        header('X-Powered-By: ' . "Haezal Musa <ezalepy@gmail.com>");
    }

    public function authenticate()
    {
//        $encode = 'Basic ' . base64_encode('fmms:janganbencicintaku#teamHusna');
//
//        $headers = Yii::$app->request->headers;
//        $auth = $headers->get('Authorization');
//
//        return $_SERVER; exit;
//        if ($auth != $encode) {
//            $msg = array('response' => false, "message" => "Invalid token credentials");
//            $this->setHeader(401);
//            $this->response(401, $msg);
//        }
    }

}