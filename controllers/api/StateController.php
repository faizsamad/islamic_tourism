<?php

namespace app\controllers\api;

use app\models\States;

class StateController extends ApiBaseController
{

    /**
     * Return State list
     */
    public function actionIndex()
    {
        $data = States::find()->asArray()->all();

        return $this->response(200, $data, true, 'OK');
    }
}