<?php

namespace app\controllers\api;

use Yii;
use app\models\BaseFunctions;
// use dektrium\user\helpers\Password;

use app\models\User;
use app\models\Profile;
use dektrium\user\models\User as BaseUser;


class UserController extends ApiBaseController
{
    /**
     * Get user profile
     *
     * @return array|void
     */
    public function actionProfile() {
        
        $request = Yii::$app->request;

        $id = $request->get('id');

        if (!$id)
            $id = $request->getBodyParam('id');

        // get user
        $user = User::find()->select(['id', 'username', 'email'])->where(['id' => $id])->asArray()->one();

        $response = [];
        if ($user) {

            $profile = Profile::find()->where(['user_id' => $user['id']])->asArray()->one();

            $response = [
                'status' => true,
                'data' => [
                    'user' => $user,
                    'profile' => $profile,
                ]
            ];
        }
        else {
            $response = [
                'status' => false,
                'message' => 'User not found',
            ];
        }
        return $this->response(200, $response);
    }

    /**
     * Update user password
     *
     * @return array|void
     */
    public function actionUpdatePassword() {

        $request = Yii::$app->request;
        $id = $request->getBodyParam('id');
        $curr_password = $request->getBodyParam('curr_password');
        $password = $request->getBodyParam('password');
        $repeat_password = $request->getBodyParam('repeat_password');

        $model = User::findOne($id);

        if (!$model) {
            return [
                'status' => false,
                'message' => 'User not found',
            ];
        }

        if (Yii::$app->getSecurity()->validatePassword($curr_password, $model->password_hash)) {

            if ($password == $repeat_password) {
                $new_password = Yii::$app->getSecurity()->generatePasswordHash($password);

                // update password
                $model->password_hash = $new_password;
                if ($model->save()) {
                    $response = [
                        'status' => true,
                        'message' => 'Password have successfully updated',
                    ];
                }
                else {
                    $response = [
                        'status' => false,
                        'message' => 'Error while saving data',
                    ];
                }
            }
            else {
                $response = [
                    'status' => false,
                    'message' => 'New password not match',
                ];
            }

        } else {

            $response = [
                'status' => false,
                'message' => 'Current password not match'
            ];
        }
        $this->response(200, $response);
    }


    /**
     * Update user profile
     *
     * @return array|void
     */
    public function actionUpdateProfile() {

        $request = Yii::$app->request;
        $id = $request->getBodyParam('id');
        $name = $request->getBodyParam('name');
        $work_no = $request->getBodyParam('work_no');

        $model = User::findOne($id);
        $profile = Profile::find()->where(['user_id' => $id])->one();

        if (!$model) {
            return [
                'status' => false,
                'message' => 'User not found',
            ];
        }

        $profile->name = $name;
        $profile->work_no = $work_no;

        if ($profile->save()) {
            return $this->response(200, [
                'status' => true,
                'message' => 'Profile Updated',
            ]);
        }
        else {
            return $this->response(200, [
                'status' => false,
                'message' => 'Error while saving data',
            ]);
        }
    }
    
}