<?php

namespace app\controllers\api;

use Yii;

use app\models\User;
use app\models\Profile;
use dektrium\user\models\User as BaseUser;

class AccountController extends ApiBaseController
{
    // protected function verbs()
    // {
        /**
         * GET, POST, PUT, PATCH, OPTIONS, HEAD, DELETE
         */
    //     return [
    //         'index' => ['GET'],
    //     ];
    // }
    
    /**
     * Sign up new user
     *
     * @return array|void
     */
    public function actionSignup()
    {
        $request = Yii::$app->request;
        $name = $request->getBodyParam('name');
        $email = $request->getBodyParam('email');
        $password = $request->getBodyParam('password');
        $work_no = $request->getBodyParam('work_no');

        $model = User::find()->where(['email' => $email])->one();

        if ($model) {
            return [
                'status' => false,
                'message' => 'An account has been existed using this email address. Please log in to your account',
            ];
        }

        if (!empty($name) || !empty($email) || !empty($password)) {
            $model = new User();
            $model->username = $email; 
            $model->email = $email;
            $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
            $model->confirmed_at = time();
            $model->status = 10;                

            if ($model->save()) {
                
                $profile = Profile::find()->where(['user_id' => $model->id])->one();
                $profile->name = $name;
                $profile->work_no = $work_no;
                $profile->public_email = $model->email;
                $profile->timezone = 'Asia/Kuala_Lumpur';
                $profile->gravatar_id = 'd41d8cd98f00b204e9800998ecf8427e';
                $profile->save();

                $auth = \Yii::$app->authManager;
                $speakerRole = $auth->getRole('mobile');
                $auth->assign($speakerRole, $model->id);

                $response = [
                    'status' => true,
                    'message' => 'Success, account created successfully!',
                    'data' => [
                        'id' => $model->id,
                        'username' => $model->username,
                        'email' => $model->email,
                        'name' => $profile->name,
                        'work_no' => $profile->work_no,
                    ],
                ];
            }
            
        } else {
            return [
                'status' => false,
                'message' => 'Something went wrong and your account has not been created.'
            ];
        }
        $this->response(200, $response);
    }

    public function actionLogin()
    {
        $request = Yii::$app->request;
        $email = $request->getBodyParam('email');
        $password = $request->getBodyParam('password');

        $model = User::find()->where(['email' => $email])->one();

        if (!$model) {
            return [
                'status' => false,
                'message' => 'User not found',
            ];
        }

        if (Yii::$app->getSecurity()->validatePassword($password, $model->password_hash)) {

            // get user profile
            $profile = Profile::find()->where(['user_id' => $model->id])->one();

            $response = [
                'status' => true,
                'message' => 'You have successfully login',
                'data' => [
                    'id' => $model->id,
                    'username' => $model->username,
                    'email' => $model->email,
                    'name' => $profile->name,
                    'work_no' => $profile->work_no,
                ],
            ];

        } else {
            $response = [
                'status' => false,
                'message' => 'Invalid username and password'
            ];
        }

        $this->response(200, $response);
    }
    
    /**
     * Sign up new user
     *
     * @return array|void
     */
    public function actionReset()
    {
        $request = Yii::$app->request;
        $email = $request->getBodyParam('email');
        $password = Yii::$app->security->generateRandomString(6);

        $model = User::find()->where(['email' => $email])->one();

        if (empty($model)) {
            return [
                'status' => false,
                'message' => 'Cant find account register with this email. Please register new account.',
            ];
        }

        if (!empty($model)) {
            $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);          

            if ($model->save()) {

                // Sent notification email ***********************************************
                $mail = 'muslimfriendlytrip@gmail.com'; //hpcs@123
                $body = 'Temporary password for the user '.$model->username.' is :- '.$password;

                Yii::$app->mailer->compose()
                    ->setFrom([$mail => 'muslimfriendlytrip@gmail.com'])
                    ->setTo($model->email)
                    ->setSubject('MF Trip - Reset Password')
                    ->setHtmlBody($body)
                    ->send();
                // End Sent notification email ********************************************

                $response = [
                    'status' => true,
                    'message' => 'Success, account reset successfully and new password sent to your email!',
                ];
            }
            
        } else {
            return [
                'status' => false,
                'message' => 'Something went wrong and your account password has not been reset.'
            ];
        }
        $this->response(200, $response);
    }
}