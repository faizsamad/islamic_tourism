<?php

namespace app\controllers\api;

use Yii;

use app\models\User;
use app\models\Profile;
use app\models\RateUs;
use app\models\RatePlace;

class RateController extends ApiBaseController
{
    /**
     * add app rating
     *
     * @return array|void
     */
    public function actionUs()
    {
        $request = Yii::$app->request;
        $user_id = $request->getBodyParam('user_id');
        $rate = $request->getBodyParam('user_rate');
        $com = $request->getBodyParam('user_comment');

        $model = new RateUs();
        $user = User::findOne($user_id);
        $profile = Profile::find()->where(['user_id' => $user_id])->one();

        if (empty($rate)) {
            return [
                'status' => false,
                'message' => 'Rating field empty',
            ];
        }

        if (empty($user)) {
        	$model->user_id = null;
        	$model->email = null;
        	$model->name = 'Anonymous';

        } else {
        	$model->user_id = $user_id;
        	$model->email = $user->email;
        	$model->name = $profile->name;
        	$model->created_by = $user_id;
        }

        $model->star = $rate;
        $model->comment = $com;

        if ($model->save(false)) {
            return $this->response(200, [
                'status' => true,
                'message' => 'Rating submitted',
            ]);
        
        } else {
            return $this->response(200, [
                'status' => false,
                'message' => 'Error while saving data',
            ]);
        }
    }

    /**
     * add place rating
     *
     * @return array|void
     */
    public function actionPlace() 
    {
        $request = Yii::$app->request;
        $user_id = $request->getBodyParam('user_id');
        $place = $request->getBodyParam('place_id');
        $rate = $request->getBodyParam('user_rate');
        $com = $request->getBodyParam('user_comment');

        $model = new RatePlace();
        $user = User::findOne($user_id);
        $profile = Profile::find()->where(['user_id' => $user_id])->one();

        if (empty($rate)) {
            return [
                'status' => false,
                'message' => 'Rating field empty',
            ];
        }

        if (empty($user)) {
        	$model->user_id = null;
        	$model->email = null;
        	$model->name = 'Anonymous';

        } else {
        	$model->user_id = $user_id;
        	$model->email = $user->email;
        	$model->name = $profile->name;
        	$model->created_by = $user_id;
        }

        $model->place_id = $place;
        $model->star = $rate;
        $model->comment = $com;

        if ($model->save(false)) {
            return $this->response(200, [
                'status' => true,
                'message' => 'Rating submitted',
            ]);
        
        } else {
            return $this->response(200, [
                'status' => false,
                'message' => 'Error while saving data',
            ]);
        }
    }
}
