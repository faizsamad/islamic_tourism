<?php
/**
 * Created by PhpStorm.
 * User: haezalmusa
 * Date: 10/02/2018
 * Time: 11:09 AM
 */

namespace app\controllers\user;

use app\models\AuthAssignment;
use Yii;

use dektrium\user\controllers\AdminController as BaseAdminController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\rbac\DbManager;

class AdminController extends BaseAdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'          => ['post'],
                    'confirm'         => ['post'],
                    'resend-password' => ['post'],
                    'block'           => ['post'],
                    'switch'          => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
//                'ruleConfig' => [
//                    'class' => AccessRule::className(),
//                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['switch'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Switches to the given user for the rest of the Session.
     * When no id is given, we switch back to the original admin user
     * that started the impersonation.
     *
     * @param int $id
     *
     * @return string
     */
    public function actionSwitch($id = null)
    {
        if (!$this->module->enableImpersonateUser) {
            throw new ForbiddenHttpException(Yii::t('user', 'Impersonate user is disabled in the application configuration'));
        }

        if(!$id && Yii::$app->session->has(self::ORIGINAL_USER_SESSION_KEY)) {
            $user = $this->findModel(Yii::$app->session->get(self::ORIGINAL_USER_SESSION_KEY));

            Yii::$app->session->remove(self::ORIGINAL_USER_SESSION_KEY);
        } else {
//            if (!Yii::$app->user->identity->isAdmin) {
//                throw new ForbiddenHttpException;
//            }

            $user = $this->findModel($id);
            Yii::$app->session->set(self::ORIGINAL_USER_SESSION_KEY, Yii::$app->user->id);
        }

        $event = $this->getUserEvent($user);

        $this->trigger(self::EVENT_BEFORE_IMPERSONATE, $event);

        Yii::$app->user->switchIdentity($user, 3600);

        $this->trigger(self::EVENT_AFTER_IMPERSONATE, $event);

        return $this->goHome();
    }

    /**
     * Updates an existing profile.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdateProfile($id)
    {
        Url::remember('', 'actions-redirect');
        $user    = $this->findModel($id);
        $profile = $user->profile;

        $assignment = AuthAssignment::find()->where(['user_id' => $id])->one();
        $current_assignment = '';
        if (!$assignment)
        {
            $assignment = new AuthAssignment();
        }
        else
        {
            $current_assignment = $assignment->item_name;
        }


        if ($profile == null) {
            $profile = \Yii::createObject(Profile::className());
            $profile->link('user', $user);
        }
        $event = $this->getProfileEvent($profile);

        $this->performAjaxValidation($profile);

        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event);

        if ($profile->load(\Yii::$app->request->post()) && $assignment->load(\Yii::$app->request->post())) {

            if ($profile->save())
            {
                if ($assignment->item_name != $current_assignment) {
                    $role = Yii::$app->authManager->getRole($assignment->item_name);
                    $r = new DbManager();
                    $r->assign($role, $id);

                    if ($current_assignment != '') {
                        $curr_role = Yii::$app->authManager->getRole($current_assignment);
                        $r->revoke($curr_role, $id);
                    }
                }

                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Profile details have been updated'));
                $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
                return $this->refresh();
            }

        }

        $roles = Yii::$app->authManager->getRoles();

        return $this->render('_profile', [
            'user'    => $user,
            'profile' => $profile,
            'assignment' => $assignment,
            'roles' => $roles,
        ]);
    }
}