<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class BaseFunctions
{
	/**
	 * Get states lists
	 *
	 **/

	public static function dropDownStates()
	{
		return ['' => '- Choose One - '] + ArrayHelper::map(States::find()->where(['status' => 1])->all(), 'id', 'name');
	}

	/**
	 * Get parking type lists
	 *
	 **/

	public static function dropDownParkingTypes()
	{
		return ['' => '- Choose One - '] + ArrayHelper::map(ParkingType::find()->where(['status' => 1])->all(), 'id', 'name');
	}

	/**
	 * Get spending type lists
	 *
	 **/

	public static function dropDownSpendingTypes()
	{
		return ['' => '- Choose One - '] + ArrayHelper::map(SpendingType::find()->where(['status' => 1])->all(), 'id', 'name');
	}

	/**
	 * Get category lists
	 *
	 **/

	public static function checkboxCategories()
	{
		return ArrayHelper::map(Category::find()->where(['status' => 1])->all(), 'id', 'name');
	}

	/**
	 * Get day lists
	 *
	 **/

	public static function dropDownDays($id)
	{	
		return ArrayHelper::map(Days::find()->where(['id' => $id ,'status' => 1])->all(), 'id', 'name');
	}

	/**
	 * Get place address
	 *
	 **/
	public static function address($id)
	{
		$place = Place::find()->where(['id' => $id])->one();

		if (empty($place->address2)) {
			$full_address = $place->address1.'<br>'.$place->poscode.', '.$place->city.', '.$place->state->name;

		} else if (empty($place->address3)) {
			$full_address = $place->address1.'<br>'.$place->address2.'<br>'.$place->poscode.', '.$place->city.', '.$place->state->name;

		} else {
			$full_address = $place->address1.'<br>'.$place->address2.'<br>'.$place->address3.'<br>'.$place->poscode.', '.$place->city.', '.$place->state->name;

		}
		return $full_address;
	}

	/**
	 * Get place duration
	 *
	 **/
	public static function durations($id)
	{
		
		if (date('H', strtotime($id)) == 00) {
			$duration = '-';

		} else if (date('H', strtotime($id)) == 01 && date('i', strtotime($id)) == 00) {
			$duration = date('G', strtotime($id)).' hour';

		} else if (date('H', strtotime($id)) > 00 && date('i', strtotime($id)) == 00) {
			$duration = date('G', strtotime($id)).' hours';

		} else {
			$duration = date('G', strtotime($id)).' hours '. date('i', strtotime($id)).' minutes';

		}
		return $duration;
	}

	/**
	 * Get place latlong
	 *
	 **/
	public static function latLong($id)
	{
		$lat = $id->latitude;
		$long = $id->longitude;

		if (empty($lat) && empty($long)) {
			$latlong = '-';

		} else {
			$latlong = $lat.' °N, '.$long.' °E';

		}
		return $latlong;
	}
	
	/**
	 * Get status lists
	 *
	 **/

	public static function dropDownStatus()
	{
		return ['' => '- Choose One - '] + ['1' => 'Active', '2' => 'Deactive'];
	}
	
	/**
	 * Get status place lists
	 *
	 **/

	public static function dropDownStatusPlace()
	{
		return ['' => '- Choose One - '] + ['3' => 'Open', '4' => 'Temporarily Closed', '5' => 'Closed'];
	}

	/**
	 * Get status
	 * 
	 **/
	public static function status($id)
	{
		switch ($id) {
			case '1':
				$status = '<span class="badge bg-green" title="Active"> Active </span>';
				break;

			case '3':
				$status = '<span class="badge bg-blue" title="Open"> Open </span>';
				break;

			case '4':
				$status = '<span class="badge bg-yellow" title="Temporarily Closed"> Temporarily Closed </span>';
				break;

			case '5':
				$status = '<span class="badge bg-red" title="Close"> Closed </span>';
				break;
			
			default:
				$status = '<span class="badge bg-red" title="Deactive"> Deactive </span>';
				break;
		}

		return $status;
	}

	public static function today($id)
	{
		switch ($id) {
            case 'Monday':
                $today_id = 1;
                break;

            case 'Tuesday':
                $today_id = 2;
                break;

            case 'Wednesday':
                $today_id = 3;
                break;

            case 'Thurdays':
                $today_id = 4;
                break;

            case 'Friday':
                $today_id = 5;
                break;

            case 'Saturday':
                $today_id = 6;
                break;  
            
            default:
                $today_id = 7;
                break;
        }

        return $today_id;
	}
	/**
	 * Get rating lists
	 *
	 **/

	public static function dropDownRating()
	{
		return ['' => '- Choose One - ', '1' => '1 Star', '2' => '2 Star', '3' => '3 Star', '4' => '4 Star', '5' => '5 Star'];
	}

}
?>