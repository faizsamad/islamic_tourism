<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * This is the model class for table "rate_us".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $email
 * @property string $star
 * @property string $comment
 * @property int $created_by
 * @property string $created_at
 */
class RateUs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rate_us';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'email', 'comment'], 'string', 'max' => 255],
            [['star'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'star' => Yii::t('app', 'Rating'),
            'comment' => Yii::t('app', 'Comment'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
    
    /**
     * before save changed data to desire format.
     */
    public function beforeSave($insert)
    {   
        // new record
        $this->created_at = new Expression('NOW()');

        return parent::beforeSave($insert);
    }
    
    /**
     * before show changed data to desire format.
     */
    public function afterFind()
    {
        parent::afterFind();

        // convert to display format
        $this->created_at = strtotime ($this->created_at);
        $this->created_at = date ('d-m-Y h:i:s A', $this->created_at);

    }

    public function getImageurl()
    {   
        $stars = $this->star; $count = 1; $result = "";

        for($i = 1; $i <= 5; $i++){
            if($stars >= $count){
                $result .= Html::img(Yii::$app->request->BaseUrl.'/image/star-fill.png', ['width'=>18,'height'=>18]);
            } else {
                $result .= Html::img(Yii::$app->request->BaseUrl.'/image/star.png', ['width'=>18,'height'=>18]);
            }
            $count++;
        }

        return $result;
    }
}
