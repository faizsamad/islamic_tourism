<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "departments".
 *
 * @property int $id
 * @property string $name
 * @property string $desc
 * @property int $status
 * @property int $created_by
 * @property string $created_date
 * @property int $updated_by
 * @property string $updated_date
 */
class Departments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'departments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['desc'], 'string'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'desc' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_date' => Yii::t('app', 'Created Date'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_date' => Yii::t('app', 'Updated Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            if ($this->isNewRecord) {
                // new record
                $this->created_by = (Yii::$app->user->id) ? Yii::$app->user->id: '';
                $this->updated_by = (Yii::$app->user->id) ? Yii::$app->user->id: '';
                $this->created_date = new \yii\db\Expression('NOW()');
            }
            else {
                // update record
                $this->updated_by = (Yii::$app->user->id) ? Yii::$app->user->id: '';
                $this->updated_date = new \yii\db\Expression('NOW()');
            }

            return true;
        } else {
            return false;
        }
    }

    public static function dropDown()
    {
        $model = Departments::find()->where(['status' => 1])->all();

        $list = ArrayHelper::map($model, 'id', 'name');
        return $list;
    }
}
