<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "place_islamic_element".
 *
 * @property int $id
 * @property int $place_id
 * @property string $islamic_element_id
 *
 * @property Place $place
 */
class PlaceIslamicElement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place_islamic_element';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['place_id'], 'integer'],
            [['islamic_element_id'], 'string', 'max' => 100],
            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => Place::className(), 'targetAttribute' => ['place_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'place_id' => Yii::t('app', 'Place ID'),
            'islamic_element_id' => Yii::t('app', 'Islamic Element ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }
}
