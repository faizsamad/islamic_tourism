<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "place_category".
 *
 * @property int $id
 * @property int $place_id
 * @property string $category_id
 *
 * @property Place $place
 */
class PlaceCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['place_id'], 'integer'],
            [['category_id'], 'string', 'max' => 100],
            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => Place::className(), 'targetAttribute' => ['place_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'place_id' => Yii::t('app', 'Place ID'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }
}
