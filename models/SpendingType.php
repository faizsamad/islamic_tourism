<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spending_type".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 *
 * @property Place[] $places
 */
class SpendingType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spending_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_date' => Yii::t('app', 'Created Date'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_date' => Yii::t('app', 'Updated Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            if ($this->isNewRecord) {
                // new record
                $this->created_by = (Yii::$app->user->id) ? Yii::$app->user->id: '';
                $this->updated_by = (Yii::$app->user->id) ? Yii::$app->user->id: '';
                $this->created_date = new \yii\db\Expression('NOW()');
            }
            else {
                // update record
                $this->updated_by = (Yii::$app->user->id) ? Yii::$app->user->id: '';
                $this->updated_date = new \yii\db\Expression('NOW()');
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaces()
    {
        return $this->hasMany(Place::className(), ['spending_type_id' => 'id']);
    }
}
