<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "place_image".
 *
 * @property int $id
 * @property int $place_id
 * @property string $file_name
 * @property string $file_size
 * @property string $file_type
 * @property string $file_path
 * @property string $detail
 *
 * @property Place $place
 */
class PlaceImage extends \yii\db\ActiveRecord
{
    public $img;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, pdf, jpeg'], // uploaded file validation
            [['place_id'], 'integer'],
            [['detail'], 'string'],
            [['file_name', 'file_size', 'file_type'], 'string', 'max' => 100],
            [['file_path'], 'string', 'max' => 255],
            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => Place::className(), 'targetAttribute' => ['place_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'place_id' => Yii::t('app', 'Place ID'),
            'file_name' => Yii::t('app', 'File Name'),
            'file_size' => Yii::t('app', 'File Size'),
            'file_type' => Yii::t('app', 'File Type'),
            'file_path' => Yii::t('app', 'File Path'),
            'detail' => Yii::t('app', 'Detail'),
            'img' => Yii::t('app', 'Select Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }
}
