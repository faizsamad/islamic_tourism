<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "place".
 *
 * @property int $id
 * @property string $name
 * @property string $contact_no
 * @property string $duration
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $poscode
 * @property string $city
 * @property int $state_id
 * @property string $price_range
 * @property int $parking_type_id
 * @property string $detail
 * @property int $spending_type_id
 * @property double $latitude
 * @property double $longitude
 * @property string $url
 * @property int $status
 * @property int $created_by
 * @property string $created_date
 * @property int $updated_by
 * @property string $updated_date
 *
 * @property ParkingType $parkingType
 * @property States $state
 * @property PlaceCategory[] $placeCategories
 * @property PlaceImage[] $placeImages
 * @property PlaceIslamicElement[] $placeIslamicElements
 * @property PlaceOperationHour[] $placeOperationHours
 */
class Place extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address1', 'poscode', 'city', 'state_id', 'parking_type_id', 'spending_type_id', 'latitude', 'longitude'], 'required'],
            [['duration', 'created_date', 'updated_date'], 'safe'],
            [['state_id', 'parking_type_id', 'spending_type_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['price_range', 'detail', 'operating_remark'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['name', 'address1', 'address2', 'address3', 'url'], 'string', 'max' => 255],
            [['contact_no', 'poscode'], 'string', 'max' => 15],
            [['city'], 'string', 'max' => 100],
            [['parking_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParkingType::className(), 'targetAttribute' => ['parking_type_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Place Name'),
            'contact_no' => Yii::t('app', 'Contact No'),
            'duration' => Yii::t('app', 'Estimate Duration'),
            'address1' => Yii::t('app', 'Address1'),
            'address2' => Yii::t('app', 'Address2'),
            'address3' => Yii::t('app', 'Address3'),
            'poscode' => Yii::t('app', 'Poscode'),
            'city' => Yii::t('app', 'City'),
            'state_id' => Yii::t('app', 'State'),
            'price_range' => Yii::t('app', 'Price Range'),
            'parking_type_id' => Yii::t('app', 'Parking Type'),
            'detail' => Yii::t('app', 'Detail'),
            'spending_type_id' => Yii::t('app', 'Visit Duration'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'url' => Yii::t('app', 'Url'),
            'operating_remark' => Yii::t('app', 'Operation Hour Remark'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_date' => Yii::t('app', 'Created Date'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_date' => Yii::t('app', 'Updated Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParkingType()
    {
        return $this->hasOne(ParkingType::className(), ['id' => 'parking_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpendingType()
    {
        return $this->hasOne(SpendingType::className(), ['id' => 'spending_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceCategories()
    {
        return $this->hasOne(PlaceCategory::className(), ['place_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceImages()
    {
        return $this->hasMany(PlaceImage::className(), ['place_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceIslamicElements()
    {
        return $this->hasMany(PlaceIslamicElement::className(), ['place_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceOperationHours()
    {
        return $this->hasMany(PlaceOperationHour::className(), ['place_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            if ($this->isNewRecord) {
                // new record
                $this->created_by = (Yii::$app->user->id) ? Yii::$app->user->id: '';
                $this->updated_by = (Yii::$app->user->id) ? Yii::$app->user->id: '';
                $this->created_date = new \yii\db\Expression('NOW()');
            }
            else {
                // update record
                $this->updated_by = (Yii::$app->user->id) ? Yii::$app->user->id: '';
                $this->updated_date = new \yii\db\Expression('NOW()');
            }

            return true;
        } else {
            return false;
        }
    }
}
