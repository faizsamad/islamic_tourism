<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "place_operation_hour".
 *
 * @property int $id
 * @property int $place_id
 * @property int $day_id
 * @property string $start_time
 * @property string $end_time
 *
 * @property Days $day
 * @property Place $place
 */
class PlaceOperationHour extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place_operation_hour';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['place_id', 'day_id'], 'integer'],
            [['start_time', 'end_time'], 'safe'],
            [['day_id'], 'exist', 'skipOnError' => true, 'targetClass' => Days::className(), 'targetAttribute' => ['day_id' => 'id']],
            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => Place::className(), 'targetAttribute' => ['place_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'place_id' => Yii::t('app', 'Place ID'),
            'day_id' => Yii::t('app', 'Day ID'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
        ];
    }

    public function afterFind ()
    {
        // convert to display format
        $this->start_time = date("h:i A", strtotime($this->start_time));
        $this->end_time = date("h:i A", strtotime($this->end_time));
        
        if($this->start_time=="") {
            $this->start_time="Closed";
        }
        
        if($this->end_time=="") {
            $this->end_time="Closed";
        }

        parent::afterFind ();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDay()
    {
        return $this->hasOne(Days::className(), ['id' => 'day_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }
}
