<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\BaseFunctions;

/* @var $this yii\web\View */
/* @var $model app\models\SpendingType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="spending-type-form">
    <?php $form = ActiveForm::begin(); ?>
    
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo ($model->isNewRecord) ? Yii::t('app', 'Create New Visit Duration'):Yii::t('app', 'Visit Duration Update: '. $model->name); ?></h3>
        </div><!-- box-header -->

        <div class="box-body">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?php if(!$model->isNewRecord) { ?>
                <?= $form->field($model, 'status')->dropDownList(BaseFunctions::dropDownStatus()); ?>
            <?php } ?>            

        </div><!-- box-body -->

        <div class="box-footer">
            <div class="form-group">
                <a href="<?php echo Yii::$app->request->referrer; ?>" class="btn btn-warning"><i class="fa fa-arrow-left"></i> <?php echo Yii::t('app', 'Back'); ?></a>

                <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-plus"></i> '.Yii::t('app', 'Create') : '<i class="fa fa-save"></i> '.Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div><!-- box-footer -->

    </div><!-- box -->
    
    <?php ActiveForm::end(); ?>
</div>
