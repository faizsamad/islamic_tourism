<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\BaseFunctions;

/* @var $this yii\web\View */
/* @var $model app\models\SpendingType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Visit Duration'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">
    table.detail-view th {
        width: 25%;
    }
    table.detail-view td {
        width: 75%;
    }
</style>

<div class="spending-type-view">

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Spending Type Detail: '. $model->name); ?></h3>
        </div><!-- box-header -->
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    [   
                        'attribute' => 'status',
                        'value' => function($model) { 
                            return BaseFunctions::status($model->status);
                        },
                        'format'=>'html',
                    ],
                ],
            ]) ?>

            <div><i><small><br>
                <?= Yii::t('app', 'Updated by '); echo (isset($model->updatedBy->profile)) ? $model->updatedBy->profile->name:'-'; echo Yii::t('app', ' on '); echo date('d-M-Y h:i A', strtotime($model->updated_date)) ?>
            </small></i></div>
        </div><!-- box-body -->

        <div class="box-footer">

            <p>
                <?= Html::a('<i class="fa fa-arrow-left"></i> Back', ['index'], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('<i class="fa fa-cog"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure to delete this record?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div><!-- box-footer -->
    </div><!-- box -->    

</div>
