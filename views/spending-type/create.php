<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SpendingType */

$this->title = Yii::t('app', 'Visit Duration');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Visit Durations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>
<div class="spending-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
