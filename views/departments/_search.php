<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\BaseFunctions;

/* @var $this yii\web\View */
/* @var $model app\models\DepartmentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="departments-search">
    <div class="row">
        <div class="col-md-12">

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>

            <div class="col-md-7">
                <?= $form->field($model, 'name') ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'status')->dropDownList(BaseFunctions::dropDownStatus()); ?>
            </div>

            <div class="col-md-2">
                <label>&nbsp;</label><br>
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
