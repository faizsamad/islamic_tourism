<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;


/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */

$this->title = Yii::t('user', 'Users Setting');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/admin/_menu') ?>

<?php Pjax::begin() ?>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('user', 'List of Users'); ?></h3>

        
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    
    <div class="box-body table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'layout'       => "{items}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width:5%'],
                    'contentOptions' => ['class' => 'text-center'],
                ],
                
                'username',
                'email:email',
//        [
//            'attribute' => 'registration_ip',
//            'value' => function ($model) {
//                return $model->registration_ip == null
//                    ? '<span class="not-set">' . Yii::t('user', '(not set)') . '</span>'
//                    : $model->registration_ip;
//            },
//            'format' => 'html',
//        ],
                [
                    'attribute' => 'created_at',
                    'value' => function ($model) {
                        if (extension_loaded('intl')) {
                            return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
                        } else {
                            return date('d-m-Y h:i:s A', $model->created_at);
                        }
                    },
                ],

                [
                    'attribute' => 'last_login_at',
                    'value' => function ($model) {
                        if (!$model->last_login_at || $model->last_login_at == 0) {
                            return Yii::t('user', 'Never');
                        } else if (extension_loaded('intl')) {
                            return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
                        } else {
                            return date('d-m-Y h:i:s A', $model->last_login_at);
                        }
                    },
                ],
                [
                    'header' => Yii::t('user', 'Confirmation'),
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['class' => 'text-center'],
                    'value' => function ($model) {
                        if ($model->isConfirmed) {
                            return '<div class="text-center">
                                <span class="text-success">' . Yii::t('user', 'Confirmed') . '</span>
                            </div>';
                        } else {
                            return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-success btn-block',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                            ]);
                        }
                    },
                    'format' => 'raw',
                    'visible' => Yii::$app->getModule('user')->enableConfirmation,
                ],
                [
                    'header' => Yii::t('user', 'Block status'),
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['class' => 'text-center'],
                    'value' => function ($model) {
                        if ($model->isBlocked) {
                            return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-success btn-block',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                            ]);
                        } else {
                            return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-danger btn-block',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                            ]);
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Action',
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width:10%'],
                    'contentOptions' => ['class' => 'text-center'],
                    'template' => '{switch} {resend_password} {update} {delete}',
                    'buttons' => [
                        'resend_password' => function ($url, $model, $key) {
                            if (!$model->isAdmin) {
                                return 
                                '<a data-method="POST" data-confirm="' . Yii::t('user', 'Are you sure you want to generate and send new password for this user?') . '" href="' . Url::to(['resend-password', 'id' => $model->id]) . '">
                                <span title="' . Yii::t('user', 'Generate and send new password to user') . '" class="glyphicon glyphicon-envelope">
                                </span> </a>';
                            }
                        },
                        'switch' => function ($url, $model) {
                            if($model->id != Yii::$app->user->id && Yii::$app->getModule('user')->enableImpersonateUser) {
                                return Html::a('<span class="glyphicon glyphicon-user"></span>', ['/user/admin/switch', 'id' => $model->id], [
                                    'title' => Yii::t('user', 'Become this user'),
                                    'data-confirm' => Yii::t('user', 'Are you sure you want to switch to this user for the rest of this session?'),
                                    'data-method' => 'POST',
                                ]);
                            }
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
    <!-- /.box-body -->
</div>

<?php Pjax::end() ?>
