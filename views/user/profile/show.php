<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-6 col-md-3">
        <?= $this->render('../settings/_menu') ?>
    </div>
    <div class="col-sm-6 col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td style="width: 5%" class="text-center"><i class="fa fa-user"></i></td>
                            <td style="width: 18%"><?= Yii::t('app', 'Name'); ?></td>
                            <td style="width: 2%">:</td>
                            <td style="width: 75%"><?= Html::encode($profile->name) ?></td>
                        </tr>
                        <tr>
                            <td style="width: 5%" class="text-center"><i class="fa fa-envelope"></i></td>
                            <td style="width: 18%"><?= Yii::t('app', 'Email'); ?></td>
                            <td style="width: 2%">:</td>
                            <td style="width: 75%"><?= Html::a(Html::encode($profile->user->email), 'mailto:' . Html::encode($profile->user->email)) ?></td>
                        </tr>
                        <tr>
                            <td style="width: 5%" class="text-center"><i class="fa fa-mobile"></i></td>
                            <td style="width: 18%"><?= Yii::t('app', 'Contact Number'); ?></td>
                            <td style="width: 2%">:</td>
                            <td style="width: 75%"><?= (!empty($profile->work_no)) ? Html::encode($profile->work_no) : '-' ?></td>
                        </tr>
                        <tr>
                            <td style="width: 5%" class="text-center"><i class="fa fa-building"></i></td>
                            <td style="width: 18%"><?= Yii::t('app', 'Company Name'); ?></td>
                            <td style="width: 2%">:</td>
                            <td style="width: 75%"><?= (!empty($profile->company)) ? Html::encode($profile->company) : '-' ?></td>
                        </tr>
                        <tr>
                            <td style="width: 5%" class="text-center"><i class="fa fa-phone"></i></td>
                            <td style="width: 18%"><?= Yii::t('app', 'Company Number'); ?></td>
                            <td style="width: 2%">:</td>
                            <td style="width: 75%"><?= (!empty($profile->office_no)) ? Html::encode($profile->office_no) : '-' ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
