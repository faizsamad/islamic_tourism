<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
	<div class="col-lg-3 col-xs-6">
	  	<!-- small box -->
	  	<div class="small-box bg-blue">
	    	<div class="inner"><h3><?= $open; ?></h3><p><?= Yii::t('app', 'Total Open Places'); ?></p></div>

	    	<div class="icon"><i class="fa fa-map-marker"></i></div>
        <?php echo Html::a(Yii::t('app', 'More info') . ' <i class="fa fa-arrow-circle-right"></i>', ['/place', 'status' => 3], ['class' => 'small-box-footer']) ?>
	  	</div>
	</div>
	<!-- ./col -->

	<div class="col-lg-3 col-xs-6">
	  	<!-- small box -->
	  	<div class="small-box bg-yellow">
	    	<div class="inner"><h3><?= $temporarily; ?></h3><p><?= Yii::t('app', 'Total Temporarily Closed Places'); ?></p></div>

	    	<div class="icon"><i class="fa fa-refresh"></i></div>
        <?php echo Html::a(Yii::t('app', 'More info') . ' <i class="fa fa-arrow-circle-right"></i>', ['/place', 'status' => 4], ['class' => 'small-box-footer']) ?>
	  	</div>
	</div>
	<!-- ./col -->

	<div class="col-lg-3 col-xs-6">
	  	<!-- small box -->
	  	<div class="small-box bg-red">
	    	<div class="inner"><h3><?= $closed; ?></h3><p><?= Yii::t('app', 'Total Closed Places'); ?></p></div>

	    	<div class="icon"><i class="fa fa-power-off"></i></div>
        <?php echo Html::a(Yii::t('app', 'More info') . ' <i class="fa fa-arrow-circle-right"></i>', ['/place', 'status' => 5], ['class' => 'small-box-footer']) ?>
	  	</div>
	</div>
	<!-- ./col -->
</div>
<!-- ./1st-row -->