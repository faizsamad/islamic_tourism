<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\BaseFunctions;

/* @var $this yii\web\View */
/* @var $model app\models\RateUsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rate-us-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-10">
        <?= $form->field($model, 'star')->dropDownList(BaseFunctions::dropDownRating()); ?>
    </div>

    <div class="col-md-2">
        <label>&nbsp;</label><br>
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
