<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RateUs */

$this->title = Yii::t('app', 'Create Rate Us');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rate uses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rate-us-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
