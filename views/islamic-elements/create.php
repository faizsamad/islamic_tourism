<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IslamicElements */

$this->title = Yii::t('app', 'Islamic Elements');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Islamic Elements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>
<div class="islamic-elements-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
