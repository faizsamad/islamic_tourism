<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IslamicElements */

$this->title = Yii::t('app', 'Islamic Elements');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Islamic Elements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="islamic-elements-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
