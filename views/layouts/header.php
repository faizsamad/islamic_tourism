<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$this->registerJs('
$(document).ready(function() {
    $("#refresh").on("click", function () {
        location.reload();
    });
});
', \yii\web\View::POS_END, 'refresh');
?>

<header class="main-header">

    <a href="../../index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>MFT</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>MFTrip</b></span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->

                <?php if (!Yii::$app->user->isGuest) { ?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                            <span class=""><i class="fa fa-user"></i> <?php echo (isset(Yii::$app->user->identity->profile->name))?Yii::$app->user->identity->profile->name:Yii::$app->user->identity->username; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">

                                <p>
                                    <?php echo (isset(Yii::$app->user->identity->profile->name))?Yii::$app->user->identity->profile->name:Yii::$app->user->identity->username; ?>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?= Html::a(
                                        Yii::t('user', 'Profil'),
                                        ['/user/profile'],
                                        ['class'=>'btn btn-default btn-flat']
                                    )?>
                                </div>
                                <div class="pull-right">
                                    <?= Html::a(
                                        Yii::t('app', 'Log Keluar'),
                                        ['/site/logout'],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    ) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php } ?>

                <!-- User Account: style can be found in dropdown.less -->
                <!--                <li>-->
                <!--                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
                <!--                </li>-->
            </ul>
        </div>
    </nav>
</header>
