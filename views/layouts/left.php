<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu', 'data-widget'=> 'tree'],
                'items' => [

                    // admin menu
                    [
                        'label' => 'Administrator', 
                        'options' => ['class' => 'header'], 
                        'visible' => Yii::$app->user->can('admin'),
                    ],

                    [
                        'label' => 'User Management',
                        'icon' => 'user',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('admin'),
                        'items' => [
                            [
                                'label' => Yii::t('app', 'Users Setting'),
                                'icon' => 'ellipsis-h', 
                                'url' => ['/user/admin'],
                            ],
                            [
                                'label' => Yii::t('app', 'Roles Setting'),
                                'icon' => 'ellipsis-h', 
                                'url' => ['/admin'],
                            ],
                        ]
                    ],

                    [
                        'label' => 'System Management',
                        'icon' => 'laptop',
                        'url' => ['/admin'],
                        'visible' => Yii::$app->user->can('admin'),
                        'items' => [
                            [
                                'label' => Yii::t('app', 'Categories Setting'),
                                'icon' => 'ellipsis-h', 
                                'url' => ['/category'],
                            ],
                            // [
                            //     'label' => Yii::t('app', 'Departments Setting'),
                            //     'icon' => 'ellipsis-h', 
                            //     'url' => ['/departments'],
                            // ],
                            [
                                'label' => Yii::t('app', 'Islamic Elements Setting'),
                                'icon' => 'ellipsis-h', 
                                'url' => ['/islamic-elements'],
                            ],
                            [
                                'label' => Yii::t('app', 'Parking Types Setting'),
                                'icon' => 'ellipsis-h', 
                                'url' => ['/parking-type'],
                            ],
                            [
                                'label' => Yii::t('app', 'Visit Durations Setting'),
                                'icon' => 'ellipsis-h', 
                                'url' => ['/spending-type'],
                            ],
                        ]
                    ],

                    // menu
                    ['label' => 'Menu', 'options' => ['class' => 'header']],

                    [
                        'label' => 'Dashboard',
                        'icon' => 'home',
                        'url' => ['/']
                    ],

                    [
                        'label' => 'Place',
                        'icon' => 'building',
                        'url' => ['/place']
                    ],

                    [
                        'label' => 'Rate Us',
                        'icon' => 'star',
                        'url' => ['/rate-us']
                    ],

                    [
                        'label' => 'Rate Place',
                        'icon' => 'star-o',
                        'url' => ['/rate-place']
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
