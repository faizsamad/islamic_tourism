<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


/**
 * 1. page search
 * 2. search result
 * 3. advanced search
 * 4. view event detais
 *
 * semua di atas, papar default template. Tak de sidebar
 */
if (in_array(Yii::$app->controller->action->id, ['login'])) {

    /**
     * This is for homepage
     */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );

}
elseif (in_array(Yii::$app->controller->id, ['pat'])) {

    /**
     * This is for homepage
     */
    echo $this->render(
        'main-top-navbar',
        ['content' => $content]
    );

}
else {


    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    app\assets\AppAsset::register($this);


    dmstr\web\AdminLteAsset::register($this);


    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?php echo Yii::$app->name; ?> | <?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>


    </head>
    <body class="hold-transition skin-green sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>

    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
