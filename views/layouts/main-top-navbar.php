<?php
use yii\helpers\Html;

app\assets\AppAsset::register($this);


dmstr\web\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>


    <title><?php echo Yii::$app->name; ?> | <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


    <style>
        .content-wrapper
        {
            background-color: #FFFFFF;
        }
    </style>
    <?php
    if (Yii::$app->controller->id == 'route' && Yii::$app->controller->action->id == 'map') {
        ?>
        <style>
            .content {
                padding: 0px;
            }


        </style>

        <?php
    }
    ?>
    <style>
            .fixed .content-wrapper, .fixed .right-side {
                padding-top: 50px;
            }
    </style>
</head>
<body class="hold-transition fixed skin-green-light layout-top-nav">
<?php $this->beginBody() ?>
<div class="wrapper">

    <?= $this->render(
        'header-top-navbar.php',
        ['directoryAsset' => $directoryAsset]
    ) ?>

    <?= $this->render(
        'content-top-navbar.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>

</div>

<?php $this->endBody() ?>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?v=3&callback=initMap&key=AIzaSyBS3mPquixj80LrbIPmhLjwWvl_MrTvQ7g">
</script>
</body>
</html>
<?php $this->endPage() ?>