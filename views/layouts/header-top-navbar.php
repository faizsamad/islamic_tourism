<?php
use yii\helpers\Html;
?>
<header class="main-header">


    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">


                <button type="button" class="navbar-toggle pull-left collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>

                <?= Html::a(\yii\helpers\Html::encode($this->title), Yii::$app->homeUrl, ['class' => 'navbar-brand']) ?>
            </div>

            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><?= Html::a('Student Details', ['/pat/index']) ?></li>
                </ul>

            </div>

            <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
                <ul class="nav navbar-nav">

                    <?php if (Yii::$app->session->get('parent_id') != '') { ?>
                        <li><?= Html::a('Sign Out', ['/pat/logout']) ?></li>
                    <?php } ?>
                </ul>

            </div>

        </div>
        <!-- /.container-fluid -->
    </nav>
</header>