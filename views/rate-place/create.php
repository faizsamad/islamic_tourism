<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RatePlace */

$this->title = Yii::t('app', 'Create Rate Place');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rate Places'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rate-place-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
