<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RatePlaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rate Place');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success box-solid collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('app', 'Search Criteria') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->

    <div class="box-body" style="display: none;">
        <?= $this->render('_search', [
            'model' => $searchModel
        ]) ?>
    </div>
    <!-- /.box-body -->
</div>

<div class="rate-place-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('app', '&nbsp;') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <div class="pull-right"></div>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn',
                                    'headerOptions' => ['class' => 'text-center', 'style' => 'width:5%'],
                                    'contentOptions' => ['class' => 'text-center'],
                                ],

                                [
                                    'header'=>'Name',
                                    'headerOptions' => ['style' => 'width:20%'],
                                    'attribute' => 'name',
                                ],

                                [
                                    'header'=>'Place Name',
                                    'headerOptions' => ['style' => 'width:20%'],
                                    'attribute' => 'duration',
                                    'value' => function($model) {   
                                        return $model->place->name;
                                    },
                                    'format'=>'html',
                                ],

                                'comment',

                                [   
                                    'header'=>'Rating',
                                    'headerOptions' => ['class' => 'text-center', 'style' => 'width:10%'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'attribute' => 'star',
                                    'value' => function ($model) {
                                        return $model->getImageUrl(); 
                                    },
                                    'format'=>'html',
                                ],

                                [
                                    'header'=>'Created At',
                                    'headerOptions' => ['style' => 'width:10%'],
                                    'attribute' => 'created_at',
                                ],

                            ],
                        ]); ?>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
