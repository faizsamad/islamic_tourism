<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\BaseFunctions;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ParkingTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Parking Types');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success box-solid collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('app', 'Search Criteria') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->

    <div class="box-body" style="display: none;">
        <?= $this->render('_search', [
            'model' => $searchModel
        ]) ?>
    </div>
    <!-- /.box-body -->
</div>

<div class="parking-type-index">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title pull-right">
                <?= Html::a('<span class="fa fa-plus"></span> Create', ['create'], ['class' => 'btn btn-success']) ?>
            </h3>
        </div><!-- box-header -->

        <div class="box-body table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'summary' => "Memaparkan {begin} - {end} daripada {totalCount} rekod",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn',
                        'headerOptions' => ['class' => 'text-center', 'style' => 'width:5%'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],

                    'name',

                    [
                        'header'=>'Status',
                        'headerOptions' => ['class' => 'text-center', 'style' => 'width:12%'],
                        'contentOptions' => ['class' => 'text-center'],
                        'attribute' => 'status',
                        'value' => function($model) {   
                            return BaseFunctions::status($model->status);
                        },
                        'format'=>'html',
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Action',
                        'headerOptions' => ['class' => 'text-center', 'style' => 'width:15%'],
                        'contentOptions' => ['class' => 'text-center'],
                        'template' => '{view} {update} {delete}', //
                        'buttons' => [
                            'update' => function ($url, $model) {     
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('yii', 'Update'),
                                    'class'=>'btn btn-warning btn btn-xs',
                                ]);   
                            },
                            'view' => function ($url, $model) {     
                                return Html::a('<span class="glyphicon glyphicon-search"></span>', $url, [
                                    'title' => Yii::t('yii', 'View'),
                                    'class'=>'btn btn-info btn-xs',
                                ]);   
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this data?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div><!-- box-body -->
    </div><!-- box -->

</div>
