<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ParkingType */

$this->title = Yii::t('app', 'Parking Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parking Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="parking-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
