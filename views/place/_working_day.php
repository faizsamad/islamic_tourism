<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\JuiAsset;
use yii\web\JsExpression;
use kartik\file\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\time\TimePicker;

use app\models\BaseFunctions;
use app\models\PlaceOperationHour;

?>

<!-- <div id="panel-option-values" class="panel panel-default panel-responsive"> -->

    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper',
        'widgetBody' => '.body-operation-hour',
        'widgetItem' => '.item-operation-hour',
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.delete-item',
        'model' => $placeOperationHours[0],
        'formId' => 'operation-form',
        'formFields' => [
            'day_id',
            'start_time',
        ],

    ]); ?>

    <table class="table table-bordered table-striped table-responsive">
        <thead>
            <tr>
                <th style="width: 30%; text-align: center"><?= Yii::t('app', 'Day'); ?></th>
                <th style="width: 20%; text-align: center"><?= Yii::t('app', 'Start Time'); ?></th>
                <th style="width: 20%; text-align: center"><?= Yii::t('app', 'Close Time'); ?></th>
            </tr>

        </thead>

        <tbody class="body-operation-hour">
            <?php $key = 1; foreach ($placeOperationHours as $index => $hour) { ?>
                <tr class="item-operation-hour">
                    <?= $form->field($hour, "[{$index}]day_id")->hiddenInput(['value'=> $key])->label(false); ?>
                    <td>                        
                        <?= $form->field($hour, "[{$index}]day_id")->dropDownList(BaseFunctions::dropDownDays($key), ['disabled' => 'disabled'])->label(false); ?>                            
                    </td>

                    <td>
                        <?= $form->field($hour, "[{$index}]start_time")->widget(TimePicker::classname(), [
                            'pluginOptions' => [
                                'defaultTime' => false
                            ]
                        ])->label(false); ?>
                    </td>

                    <td>
                        <?= $form->field($hour, "[{$index}]end_time")->widget(TimePicker::classname(), [
                            'pluginOptions' => [
                                'defaultTime' => false
                            ]
                        ])->label(false); ?>
                    </td>
                </tr>
            <?php $key++; } ?>
        </tbody>

    </table>

    <?= $form->field($model, 'operating_remark')->textarea(['rows' => 3]) ?>

    <?php DynamicFormWidget::end(); ?>
<!-- </div> -->