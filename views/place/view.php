<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

use app\models\BaseFunctions;
use app\models\PlaceImage;
use app\models\Category;
use app\models\IslamicElements;

/* @var $this yii\web\View */
/* @var $model app\models\Place */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Places'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $attribute_labels = $model->attributeLabels(); ?>

<div class="box box-success box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-street-view"></i>&nbsp;&nbsp;<?= Yii::t('app', 'Place Details') ?></h3>
        <div class="box-tools pull-right">
        </div>
    </div>
    <div class="table-responsive">

        <table class="table table-condensed table-bordered table-striped">
            <tr>
                <th width="15%"><?= $attribute_labels['name']; ?></th>
                <td width="35%"><?= (isset($model->name))?$model->name:'-'; ?></td>     
            
                <th width="15%"><?= $attribute_labels['duration'] ?></th>
                <td width="35%"><?= (isset($model->duration)) ? BaseFunctions::durations($model->duration):'-'; ?></td>
            </tr>
            <tr>
                <th width="15%"><?= Yii::t('app', 'Address') ?></th>
                <td width="35%"><?= BaseFunctions::address($model->id) ?></td>
            
                <th width="15%"><?= $attribute_labels['price_range'] ?></th>
                <td width="35%"><?= nl2br($model->price_range); ?></td>
            </tr>
            <tr>
                <th width="15%"><?= $attribute_labels['detail'] ?></th>
                <td width="85%" colspan="3"><?= nl2br($model->detail); ?></td>
            </tr>
            <tr>
                <th width="15%"><?= $attribute_labels['parking_type_id'] ?></th>
                <td width="35%"><?= (isset($model->parking_type_id)) ? $model->parkingType->name:'-'; ?></td>
            
                <!-- <th width="15%"><?= $attribute_labels['spending_type_id'] ?></th>
                <td width="35%"><?= (isset($model->spending_type_id)) ? $model->spendingType->name:'-'; ?></td> -->

                <th width="15%"><?= Yii::t('app', 'Latitude/Longitude') ?></th>
                <td width="35%"><?= BaseFunctions::latLong($model); ?></td>
            </tr>
            <tr><th width="15%"><?= $attribute_labels['url'] ?></th>
                <td width="35%"><?= (isset($model->url)) ? Html::a($model->url, Url::to($model->url, true), ['target'=>'_blank']) :'-'; ?></td>

                <th width="15%"><?= Yii::t('app', 'Status') ?></th>
                <td width="35%"><?= BaseFunctions::status($model->status); ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="box box-success box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-sitemap"></i>&nbsp;&nbsp;<?php echo Yii::t('app', 'Place Category') ?></h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered table-responsive">
                <?php if(!empty($placeCategories->category_id)) {

                    $categories = explode(",", $placeCategories->category_id);
                    foreach($categories as $key => $value) {
                        // get category name
                            $model_package = Category::findOne($value);
                            echo '<div class="col-xs-8 col-md-2">';
                            echo '<br><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;' . $model_package->name;
                            echo '</div>';
                    }; 
                } else {
                    echo 'No category been saved';
                } ?>
            </table>
        </div>
    </div>
</div>

<div class="box box-success box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo Yii::t('app', 'Place Islamic Element') ?></h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered table-responsive">
                <?php if(!empty($placeElements->islamic_element_id)) {

                    $elements = explode(",", $placeElements->islamic_element_id);
                    foreach($elements as $key => $value) {
                            // get category name
                            $model_element = IslamicElements::findOne($value);
                            echo '<div class="col-xs-8 col-md-2">';
                            echo '<br><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;' . $model_element->name;
                            echo '</div>';
                    }; 
                } else {
                    echo 'No islamic element been saved';
                } ?>
            </table>
        </div>
    </div>
</div>

<div class="box box-success box-solid collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-image"></i>&nbsp;&nbsp;<?php echo Yii::t('app', 'Place Photos') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div class="box-body" style="display: none;">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-responsive">
                <thead>
                    <tr>
                        <th width="5%" class="text-center"><?php echo Yii::t('app', 'No.'); ?></th>
                        <th width="35%"><?php echo Yii::t('app', 'File'); ?></th>
                        <th width="50%"><?php echo Yii::t('app', 'Detail'); ?></th>
                        <th width="10%" class="text-center"><?php echo Yii::t('app', 'Download'); ?></th>
                    </tr>
                </thead>

                <tbody>
                <?php if (count($images) == 0) { ?>
                    <tr>
                        <td colspan="4"><?php echo Yii::t('app', 'No Attachment') ?></td>
                    </tr>
                <?php } ?>
                <?php $bil=1; $path = null; foreach ($images as $image) { ?>
                    <tr>
                        <td class="text-center"><?php echo $bil; ?></td>
                        <td>
                            <?php if (isset($image)) {
                                $type = $image->file_type;
                                $path = Yii::$app->request->baseUrl . $image->file_path;

                                if (file_exists(\Yii::getAlias('@app/web') . DIRECTORY_SEPARATOR . $image->file_path )) {
                                    if ($type == 'application/pdf') {

                                        echo '<img src="'. Yii::$app->request->baseUrl .'/images/icon_pdf.png" width="250px;">';

                                    } else { ?>
                                        <button type="button"  data-toggle="modal" data-target="#modal-default<?= $bil; ?>">
                                            <img src=<?= $path; ?> width="280px;">
                                        </button>

                                        <div class="modal fade" id="modal-default<?= $bil; ?>" style="display: none;">
                                            <div class="modal-dialog">
                                                <br><br>
                                                <img src=<?= $path; ?> width="100%;">
                                            </div>
                                        </div>
                                    <?php }
                                } else {
                                    
                                    echo '<span class="label label-danger">'.Yii::t('app', 'Image not found') .'</span>';
                                }

                            } else {

                                echo Yii::t('app', 'No image attach');
                            } ?>
                        </td>
                        <td><?php echo $image->detail; ?></td>
                        <td class="text-center">
                            <?php echo Html::a('<i class="fa fa-download"></i>', ['download', 'id' => $image->id], [
                                'class' => 'btn btn-default', 'data-method' => 'post']) ?>
                        </td>
                    </tr>
                    <?php $bil++;} ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="box box-success box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-tachometer"></i>&nbsp;&nbsp;<?php echo Yii::t('app', 'Place Operation Hour') ?></h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <div class="col-md-6">
                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr>
                            <th style="width: 30%;"><?= Yii::t('app', 'Day'); ?></th>
                            <th style="width: 20%; text-align: center"><?= Yii::t('app', 'Start Time'); ?></th>
                            <th style="width: 20%; text-align: center"><?= Yii::t('app', 'Close Time'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($model->status == 4 || $model->status == 5) { ?>
                            <tr>
                                <td colspan="3"><?php echo Yii::t('app', 'Place temporarily closed/closed') ?></td>
                            </tr>
                        <?php } else { ?>

                        <?php if (count($operationHours) == 0) { ?>
                            <tr>
                                <td colspan="3"><?php echo Yii::t('app', 'No Data Found') ?></td>
                            </tr>
                        <?php } else { ?>
                            <?php foreach ($operationHours as $hour) { ?>
                                <tr>
                                    <td><?= $hour->day->name; ?></td>
                                    <td class="text-center"><?= $hour->start_time; ?></td>
                                    <td class="text-center"><?= $hour->end_time; ?></td>
                                </tr>
                        <?php }; }; } ?>
                    </tbody>
                </table>
            </div>

            <div class="col-md-6">
                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr>
                            <th style="width: 30%;"><?= Yii::t('app', 'Operation Hour Remark'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php if(!empty($model->operating_remark)) { ?>
                                <td><?= $model->operating_remark; ?></td>
                            <?php } else { ?>
                                <td><?= Yii::t('app', 'No data been saved'); ?></td>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php echo Html::a('<i class="fa fa-arrow-left"></i> ' . Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-warning']) ?>

<?php echo Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

<?php echo Html::a('<i class="fa fa-trash"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
    'class' => 'btn btn-danger', 
    'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this data?'),
        'method' => 'post',
    ]
]) ?>
