<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;

use app\models\BaseFunctions;
use app\models\Days;

/* @var $this yii\web\View */
/* @var $model app\models\Place */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-success">

    <?php $form = ActiveForm::begin(['id' => 'place-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="box-header with-border">
      <h3 class="box-title"><?= Yii::t('app', 'Place'); ?></h3>
    </div>
    <!-- /.box-header -->

    <!-- form start -->
    <div class="box-body">

        <!-- place -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-street-view"></i> <?= Yii::t('app', 'Place Details'); ?></h3>
            </div>

            <div class="box-body" style="">
                <div class="att-place-form">

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'address1')->textInput(['maxlength' => true]) ?>

                    <div class="row">
                        <div class="col-md-7">
                            <?= $form->field($model, 'address2')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-md-5">
                            <?= $form->field($model, 'address3')->textInput(['maxlength' => true]) ?>                    
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <?= $form->field($model, 'poscode')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-md-5">
                            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>                  
                        </div>

                        <div class="col-md-5">
                            <?= $form->field($model, 'state_id')->dropDownList(BaseFunctions::dropDownStates()); ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'price_range')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'parking_type_id')->dropDownList(BaseFunctions::dropDownParkingTypes()); ?>
                        </div>

                        <div class="col-md-4">
                            <?= $form->field($model, 'duration')->widget(TimePicker::classname(), [
                                'pluginOptions' => [
                                //     'showSeconds' => true
                                    'showMeridian' => false,
                                    'defaultTime' => false
                                ]
                            ]); ?>
                        </div>

                        <!-- <div class="col-md-4">
                            <?= $form->field($model, 'spending_type_id')->dropDownList(BaseFunctions::dropDownSpendingTypes()); ?>            
                        </div> -->
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <?= $form->field($model, 'latitude', ['template' => '{label}<div class= "input-group">{input}<span class="input-group-addon">°N <span></div>{hint}{error}'])->textInput() ?>
                        </div>

                        <div class="col-md-2">
                            <?= $form->field($model, 'longitude', ['template' => '{label}<div class= "input-group">{input}<span class="input-group-addon">°E <span></div>{hint}{error}'])->textInput() ?>                    
                        </div>

                        <div class="col-md-4">
                            <?= $form->field($model, 'contact_no')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-md-4">
                            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>                    
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <?php if(!$model->isNewRecord) { ?>
                                <?= $form->field($model, 'status')->dropDownList(BaseFunctions::dropDownStatusPlace()); ?>
                            <?php } ?>
                        </div>
                    </div>

                </div>
                <!-- /.place-form -->
            </div>
            
        </div>
        <!-- /place -->

        <!-- place category -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-sitemap"></i> <?= Yii::t('app', 'Place Category'); ?></h3>
            </div>

            <div class="box-body" style="">
                <div class="checkbox">

                    <?= $form->field($placeCategories, 'category_id')->checkboxList(ArrayHelper::map($categories,'id','name'))->label(false); ?>

                    <?php //$form->field($placeCategories, 'category_id')
                        // ->listBox(ArrayHelper::map($categories,'id','name'), ['multiple' => true])
                        /* or, you may use a checkbox list instead */
                        /* ->checkboxList($categories) */
                        // ->hint('Select the categories of the post.');
                    ?>
                </div>
            </div>
            
        </div>
        <!-- /.place category -->

        <!-- place islamic element -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bank"></i> <?= Yii::t('app', 'Place Islamic Element'); ?></h3>
            </div>

            <div class="box-body" style="">
                <div class="checkbox">

                    <?= $form->field($placeElements, 'islamic_element_id')->checkboxList(ArrayHelper::map($elements,'id','name'))->label(false); ?>

                    <?php //$form->field($placeCategories, 'category_id')
                        // ->listBox(ArrayHelper::map($categories,'id','name'), ['multiple' => true])
                        /* or, you may use a checkbox list instead */
                        /* ->checkboxList($categories) */
                        // ->hint('Select the categories of the post.');
                    ?>
                </div>
            </div>
            
        </div>
        <!-- /.place islamic element -->

        <!-- place photo -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-image"></i> <?= Yii::t('app', 'Place Photos'); ?></h3>
            </div>

            <div class="box-body" style="">
                <?= $this->render('_place_image', [
                    'form' => $form,
                    'model' => $model,
                    'placeImages' => $placeImages
                ]) ?>
            </div>
            
        </div>
        <!-- /.place photo -->

        <!-- place photo -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-tachometer"></i> <?= Yii::t('app', 'Place Operating Hour'); ?></h3>
            </div>

            <div class="box-body" style="">
                <?= $this->render('_working_day', [
                    'form' => $form,
                    'model' => $model,
                    'placeOperationHours' => $placeOperationHours
                ]) ?>
            </div>
            
        </div>
        <!-- /.place photo -->

       <!--  <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <td><?= Yii::t('app', 'Place Categories'); ?></td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table> -->
        <!-- /.table-category -->
        <!-- <br>

        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <td><?= Yii::t('app', 'Place Pictures'); ?></td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table> -->
        <!-- /.table-upload -->
        <!-- <br>

        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <td><?= Yii::t('app', 'Place Operation Hour'); ?></td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table> -->
        <!-- /.table-operation-hour -->
        
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <div class="form-group">
            <a href="<?php echo Yii::$app->request->referrer; ?>" class="btn btn-warning"><i class="fa fa-arrow-left"></i> <?php echo Yii::t('app', 'Back'); ?></a>

            <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-plus"></i> '.Yii::t('app', 'Create') : '<i class="fa fa-save"></i> '.Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
