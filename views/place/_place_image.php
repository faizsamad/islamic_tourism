<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\JuiAsset;
use yii\web\JsExpression;
use kartik\file\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;

use app\models\PlaceImage;

?>

<!-- <div id="panel-option-values" class="panel panel-default panel-responsive"> -->

    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper',
        'widgetBody' => '.body-image',
        'widgetItem' => '.item-image',
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.delete-item',
        'model' => $placeImages[0],
        'formId' => 'place-form',
        'formFields' => [
            'img',
            'detail',
        ],

    ]); ?>

    <table class="table table-bordered table-striped table-responsive">
        <thead>
            <tr>
                <th style="width: 20%;">Attachment</th>
                <th style="width: 70%">Detail</th>
                <th style="width: 10%; text-align: center">Actions</th>
            </tr>

        </thead>

        <tbody class="body-image">
            <?php foreach ($placeImages as $index => $image) { ?>
                <tr class="item-image">
                    <td>
                        <?php $modelImage = PlaceImage::findOne(['id' => $image->id]);

                        $initialPreview = [];

                        if ($modelImage) {
                            $pathImg = Yii::$app->request->baseUrl . $modelImage->file_path;
                            $initialPreview[] = Html::img($pathImg, ['class' => 'file-preview-image', 'width' => '138px']);
                        }

                        ?>

                        <?= $form->field($image, "[{$index}]img")->label(false)->widget(FileInput::classname(), [

                            'options' => [
                                'multiple' => false,
                                'accept' => 'image/*',
                                'class' => 'optionvalue-img'
                            ],
                            'pluginOptions' => [
                                'previewFileType' => 'image',
                                'showCaption' => false,
                                'showUpload' => false,
                                'dropZoneEnabled' =>  false,
                                'browseClass' => 'btn btn-primary btn-sm',
                                'browseLabel' => ' Select Image',
                               'browseIcon' => '<i class="glyphicon glyphicon-picture"></i>',
                                'removeClass' => 'btn btn-danger btn-sm',
                                'removeLabel' => ' Delete',
                                'removeIcon' => '<i class="fa fa-trash"></i>',
                                // 'previewSettings' => [
                                //     'image' => ['width' => '138px', 'height' => 'auto']
                                // ],
                                'initialPreview' => $initialPreview,
                                'layoutTemplates' => ['footer' => '']
                            ]

                        ]) ?>

                        <?php if (!$image->isNewRecord) { ?>

                            <?= Html::activeHiddenInput($image, "[{$index}]id"); ?>

                        <?php } ?>
                    </td>

                    <td>
                        <?= $form->field($image, "[{$index}]detail")->label(false)->textarea(['row' => 3]); ?>
                    </td>

                    <td class="text-center vcenter">
                        <button type="button" class="delete-item btn btn-danger btn-xs"><i class="fa fa-minus"></i>
                        </button>
                    </td>
                </tr>
            <?php } ?>
        </tbody>

        <tfoot>
            <tr>
                <td colspan="2"></td>
                <td class="text-center vcenter">
                    <button type="button" class="add-item btn btn-success btn-sm"><span class="fa fa-plus"></span> New
                    </button>
                </td>
            </tr>
        </tfoot>

    </table>

    <?php DynamicFormWidget::end(); ?>
<!-- </div> -->