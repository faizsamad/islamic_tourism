<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\BaseFunctions;

/* @var $this yii\web\View */
/* @var $model app\models\PlaceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="place-search">
    <div class="row">
        <div class="col-md-12">

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>

            <div class="col-md-4">
                <?= $form->field($model, 'name') ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'state_id')->dropDownList(BaseFunctions::dropDownStates()); ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'status')->dropDownList(BaseFunctions::dropDownStatusPlace()); ?>
            </div>

            <div class="col-md-2">
                <label>&nbsp;</label><br>
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
