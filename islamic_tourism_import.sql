-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2019 at 02:21 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `islamic_tourism_import`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1493684515),
('mobile', '10', 1545882528),
('mobile', '11', 1545910414),
('mobile', '12', 1546423364),
('mobile', '13', 1546480210),
('mobile', '14', 1546480959),
('mobile', '15', 1546481002),
('mobile', '16', 1547460511),
('mobile', '17', 1547520105),
('mobile', '18', 1547520159),
('mobile', '3', 1544758345),
('mobile', '4', 1545200013),
('mobile', '5', 1545627757),
('mobile', '6', 1545631167),
('mobile', '7', 1545644488),
('mobile', '8', 1545843640),
('mobile', '9', 1545871978),
('support', '2', 1535283000);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1526015448, 1526015448),
('/admin/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/assign', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/revoke', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/assignment/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/default/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/default/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/delete', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/update', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/menu/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/assign', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/delete', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/remove', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/update', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/permission/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/assign', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/delete', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/remove', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/update', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/role/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/assign', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/refresh', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/route/remove', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/*', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/create', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/delete', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/index', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/update', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/rule/view', 2, NULL, NULL, NULL, 1506327689, 1506327689),
('/admin/user/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/activate', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/change-password', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/delete', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/index', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/login', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/logout', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/request-password-reset', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/reset-password', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/signup', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/admin/user/view', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/category/*', 2, NULL, NULL, NULL, 1535945159, 1535945159),
('/dashboard/*', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/dashboard/index', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/debug/*', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/*', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/db-explain', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/download-mail', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/index', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/toolbar', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/default/view', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/user/*', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/user/reset-identity', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/debug/user/set-identity', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/departments/*', 2, NULL, NULL, NULL, 1535259949, 1535259949),
('/departments/create', 2, NULL, NULL, NULL, 1535424907, 1535424907),
('/departments/delete', 2, NULL, NULL, NULL, 1535424907, 1535424907),
('/departments/index', 2, NULL, NULL, NULL, 1535424907, 1535424907),
('/departments/update', 2, NULL, NULL, NULL, 1535424907, 1535424907),
('/departments/view', 2, NULL, NULL, NULL, 1535424907, 1535424907),
('/gii/*', 2, NULL, NULL, NULL, 1526015448, 1526015448),
('/gii/default/*', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/action', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/diff', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/index', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/preview', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/gii/default/view', 2, NULL, NULL, NULL, 1526015447, 1526015447),
('/islamic-elements/*', 2, NULL, NULL, NULL, 1535945162, 1535945162),
('/parking-type/*', 2, NULL, NULL, NULL, 1535945166, 1535945166),
('/place/*', 2, NULL, NULL, NULL, 1535424889, 1535424889),
('/place/create', 2, NULL, NULL, NULL, 1535424896, 1535424896),
('/place/delete', 2, NULL, NULL, NULL, 1535424896, 1535424896),
('/place/download', 2, NULL, NULL, NULL, 1535424896, 1535424896),
('/place/index', 2, NULL, NULL, NULL, 1535424896, 1535424896),
('/place/update', 2, NULL, NULL, NULL, 1535424896, 1535424896),
('/place/view', 2, NULL, NULL, NULL, 1535424896, 1535424896),
('/profil/*', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/create', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/delete', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/index', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/update', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/profil/view', 2, NULL, NULL, NULL, 1526805949, 1526805949),
('/route/*', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/ajax-stop-list', 2, NULL, NULL, NULL, 1505969462, 1505969462),
('/route/create', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/delete', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/get-location', 2, NULL, NULL, NULL, 1505969462, 1505969462),
('/route/index', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/list', 2, NULL, NULL, NULL, 1505969462, 1505969462),
('/route/map', 2, NULL, NULL, NULL, 1505969462, 1505969462),
('/route/update', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/route/view', 2, NULL, NULL, NULL, 1493684422, 1493684422),
('/site/*', 2, NULL, NULL, NULL, 1505969455, 1505969455),
('/site/captcha', 2, NULL, NULL, NULL, 1505969455, 1505969455),
('/site/error', 2, NULL, NULL, NULL, 1505969455, 1505969455),
('/site/index', 2, NULL, NULL, NULL, 1505969455, 1505969455),
('/spending-type/*', 2, NULL, NULL, NULL, 1535945170, 1535945170),
('/user/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/assignments', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/block', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/confirm', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/create', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/delete', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/index', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/info', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/resend-password', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/switch', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/update', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/admin/update-profile', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/profile/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/profile/index', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/profile/show', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/recovery/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/recovery/request', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/recovery/reset', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/confirm', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/connect', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/organizer', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/register', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/registration/resend', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/security/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/security/auth', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/security/login', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/security/logout', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/*', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/account', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/confirm', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/delete', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/disconnect', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/networks', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('/user/settings/profile', 2, NULL, NULL, NULL, 1506327686, 1506327686),
('access-control', 2, NULL, NULL, NULL, 1506327725, 1506327725),
('admin', 1, 'Administrator', NULL, NULL, 1493684477, 1493684477),
('homepage', 2, 'Homepage', NULL, NULL, 1522486941, 1522486941),
('mobile', 1, 'Mobile User', NULL, NULL, 1544756773, 1544756773),
('support', 1, 'Support', NULL, NULL, 1535282960, 1535282960),
('user-profile', 2, NULL, NULL, NULL, 1506327698, 1506327698);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('access-control', '/admin/*'),
('admin', '/category/*'),
('admin', '/departments/*'),
('admin', '/islamic-elements/*'),
('admin', '/parking-type/*'),
('admin', '/place/*'),
('admin', '/spending-type/*'),
('admin', 'access-control'),
('admin', 'homepage'),
('admin', 'manage-announcement'),
('admin', 'manage-department'),
('admin', 'manage-document'),
('admin', 'manage-hod'),
('admin', 'manage-meeting'),
('admin', 'manage-meeting-category'),
('admin', 'manage-pekeliling'),
('admin', 'manage-profil'),
('admin', 'manage-region'),
('admin', 'manage-user'),
('admin', 'user-profile'),
('homepage', '/site/*'),
('support', '/place/*'),
('support', 'homepage'),
('user-profile', '/user/profile/*'),
('user-profile', '/user/profile/index'),
('user-profile', '/user/profile/show'),
('user-profile', '/user/registration/*'),
('user-profile', '/user/registration/confirm'),
('user-profile', '/user/registration/connect'),
('user-profile', '/user/registration/organizer'),
('user-profile', '/user/registration/register'),
('user-profile', '/user/registration/resend'),
('user-profile', '/user/security/*'),
('user-profile', '/user/security/auth'),
('user-profile', '/user/settings/*'),
('user-profile', '/user/settings/account'),
('user-profile', '/user/settings/confirm'),
('user-profile', '/user/settings/delete'),
('user-profile', '/user/settings/disconnect'),
('user-profile', '/user/settings/networks'),
('user-profile', '/user/settings/profile');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'Culture', 1, NULL, NULL, NULL, '0000-00-00 00:00:00'),
(2, 'Outdoor', 1, NULL, NULL, 1, '2018-09-03 03:38:19'),
(3, 'Relaxing', 1, NULL, NULL, NULL, '0000-00-00 00:00:00'),
(4, 'Food Hunting', 1, NULL, NULL, 1, '2018-09-03 03:38:46'),
(5, 'Beaches', 1, NULL, NULL, NULL, '0000-00-00 00:00:00'),
(6, 'Historical', 1, NULL, NULL, 1, '2018-09-03 03:38:34'),
(7, 'Museums', 1, NULL, NULL, NULL, '0000-00-00 00:00:00'),
(8, 'Shopping', 1, NULL, NULL, NULL, '0000-00-00 00:00:00'),
(9, 'Wildlife', 1, NULL, NULL, NULL, '0000-00-00 00:00:00'),
(10, 'Mosque', 1, 1, '2018-09-03 11:38:52', 1, '2018-09-03 03:38:52'),
(11, 'Recreation', 1, NULL, NULL, NULL, '2019-02-18 04:41:18'),
(12, 'Theme Park', 1, NULL, NULL, NULL, '2019-02-18 04:41:18'),
(13, 'Eco-Tourism', 1, NULL, NULL, NULL, '2019-02-18 04:41:41'),
(14, 'Cultural-Tourism', 1, NULL, NULL, NULL, '2019-02-18 04:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `name`, `status`) VALUES
(1, 'Monday', 1),
(2, 'Tuesday', 1),
(3, 'Wednesday', 1),
(4, 'Thursday', 1),
(5, 'Friday', 1),
(6, 'Saturday', 1),
(7, 'Sunday', 1);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `desc` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `desc`, `status`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'Management', 'Company management', 1, 1, '2018-05-17 22:18:57', 1, '2018-08-26 10:18:25'),
(2, 'Support', 'Company Support', 1, 1, '2018-08-26 18:55:44', 1, '2018-08-26 10:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `islamic_elements`
--

CREATE TABLE `islamic_elements` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `islamic_elements`
--

INSERT INTO `islamic_elements` (`id`, `name`, `status`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'Surau/ Masjid', 1, NULL, NULL, 1, '2018-10-15 16:22:51'),
(2, 'Halal Food', 1, 1, '2018-10-16 00:23:01', 1, '2018-10-15 16:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m180818_052007_create_place_table', 1535424837),
('m180818_054759_create_place_image_table', 1535424837),
('m180818_054818_create_place_operation_hour_table', 1535424837),
('m180829_030149_alter_place_table', 1535513874),
('m180830_031004_alter_category_table', 1535945118),
('m180831_135950_alter_spending_type_table', 1535945118),
('m180831_140013_alter_parking_type_table', 1535945119),
('m180831_154605_create_islamic_elements_table', 1535945119),
('m180901_022144_create_place_islamic_element_table', 1535945119),
('m180903_060557_alter_place_table', 1535957864),
('m190116_084420_alter_place_table', 1547629971);

-- --------------------------------------------------------

--
-- Table structure for table `parking_type`
--

CREATE TABLE `parking_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parking_type`
--

INSERT INTO `parking_type` (`id`, `name`, `status`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'Lot parking available', 1, NULL, NULL, NULL, '0000-00-00 00:00:00'),
(2, 'Lot parking not available', 1, NULL, NULL, NULL, '2019-02-18 04:27:15');

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE `place` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `address3` varchar(255) DEFAULT NULL,
  `poscode` varchar(15) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state_id` int(11) NOT NULL,
  `price_range` text,
  `parking_type_id` int(11) NOT NULL,
  `detail` text,
  `spending_type_id` int(11) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `operating_remark` text,
  `status` int(11) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `place`
--

INSERT INTO `place` (`id`, `name`, `contact_no`, `duration`, `address1`, `address2`, `address3`, `poscode`, `city`, `state_id`, `price_range`, `parking_type_id`, `detail`, `spending_type_id`, `latitude`, `longitude`, `url`, `operating_remark`, `status`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(3, 'Malacca Jonker Walk', '', '00:00:00', 'Jalan Hang Jebat, ', '', '', '75200', 'Melaka', 4, '', 1, 'Jonker Street - the center street of Chinatown - was once renowned for its antique shops. However, over the years it has turned to clothing and crafts outlets as well as restaurants. The best part of Jonker Street is the night market on Fridays and Saturdays that sells everything from tasty treats to cheap keepsakes.', 1, 2.195, 102.248, 'http://www.malacca.ws/jonker-street/', NULL, 3, 2, '2018-08-29 21:41:44', 2, '2019-01-15 02:20:56'),
(4, 'The Shore Sky Tower', '+606-288 3833', '00:00:00', 'Level 42, Tower 1, The Shore Melaka, ', '93 Pinggiran @ Sungai Melaka,', 'Jalan Persisiran Bunga Raya,', '75100 ', 'Melaka', 4, 'Adult RM 25\r\n\r\nChildren (age 3 – 12) RM 18\r\n\r\nTicket price included a pack of souvenir postcards.', 1, 'The Shore Sky Tower is the tallest building in Melaka and has an observation deck on the 43rd floor, 163 metres above street level, providing a splendid panoramic 360 degree view of this historic city and the surrounding area.\r\n\r\nBy comparison, Melaka’s other tall structure, the revolving tower, Menara Taming Sari, is considerably shorter at just 110 metres high, so the views are superior at Sky Tower.', 1, 2.20231, 102.25, 'http://skytower.theshoremelaka.com/', NULL, 3, 2, '2018-08-29 21:57:01', 2, '2018-09-21 07:25:10'),
(5, 'Upside Down House Malacca', '+6011-1072 2260', '00:00:00', 'G12 & G14, Jalan PM 5, ', 'Plaza Mahkota,', '', ' 75000', 'Bandar Hilir', 4, 'RM20 for adults and RM16 for children and senior citizens.\r\nFor locals (with MyKad identifications), tickets are priced at RM15 and RM10 respectively.\r\n\r\n', 1, 'The Upside Down House Melaka is a unique exhibition space within Malacca City, where visitors can step into an inverted version of a modern Malaysian house. While Malacca City is well-kept for its historical landmarks, Jonker Street, as well as the unique Peranakan culture and delicacies, Upside Down House Melaka is a relatively new addition to its list of family-friendly attractions. Set within a two-storey shophouse, the gallery is divided into a living room, kitchen, playroom, master bedroom, and bathroom, all of which are fitted with air-conditioning, interior décor, and life-sized furniture which is firmly screwed to the ceiling.', 1, 2.1893, 102.246, 'http://www.malacca.ws/attractions/placestovisit.htm', NULL, 3, 2, '2018-08-29 22:09:08', 2, '2018-09-21 07:25:10'),
(6, 'Malacca Straits Mosque (Masjid Selat Melaka)', '', '00:00:00', 'Jalan Pulau Melaka 8, ', '', '', '75000', 'Melaka', 4, '', 1, 'Standing on a man-made island named Pulau Melaka, above the waters of the Straits of Malacca, Masjid Selat Melaka really looks like it’s floating when the tide is high. The mosque was built between 2003 and 2006 and officiated in November 2006 by Tuanku Syed Sirajuddin Ibni Al-Marhum Tuanku Syed Putra Jamalullail, the country’s Yang Di-Pertuan Agong at that time. Its design idea came from the state’s former Chief Minister, Datuk Seri Mohd. Ali Rustam, who was inspired by a floating mosque he had seen during his trip to Jeddah.\r\nThe mosque’s most prominent feature is its 30-metre-high minaret which also functions as a lighthouse, acting as a guide for boats, ships and aircrafts. A massive golden dome with blue trims which is visible from a distance sits above its main prayer hall. The building incorporates Middle Eastern architectural style peppered with Malay decorative elements such as the use of bamboo as part of its structure and a beautifully-carved pulpit made out of teak wood. The mosque is quite a spectacular sight at night when the whole building lights up.\r\nMasjid Selat Melaka is equipped with various facilities which include a multipurpose hall, a library and a learning centre.', 1, 0.179, 2.249, 'http://www.itc.gov.my/mosque/masjid-selat-melaka-malacca-straits-mosque/', NULL, 3, 2, '2018-08-29 22:14:56', 2, '2018-09-25 06:25:14'),
(7, 'Morten’s Village (Kampung Morten)', '06-282 2988', '00:00:00', 'Jalan Kampung Morten,  Malaysia', '', '', '75300', 'Melaka ', 4, '', 1, 'To reach the village, a walk along the walkway beside the Melaka River from Melaka Tourist Information Center (near Stadthuys) will take around 20 - 30 minutes.', 1, 2.2051, 102.25, 'http://bettyandlingshing.blogspot.com/2013/11/kampung-morten-melaka.html', NULL, 3, 2, '2018-08-29 22:24:35', 2, '2018-09-21 07:25:10'),
(8, ' Teanquerah Mosque (Masjid Tengkera)', '', '00:00:00', 'Masjid Tengkera, , ', ' Km. 2,  Tengkera', '', ' 75200 Melaka', 'Melaka', 4, '', 1, 'Built in 1728, Masjid Tengkera is one of the oldest mosques in the country. Located in Melaka Tengah, the mosque was the State Mosque before Masjid Al-Azim was built. The original mosque structure was made entirely of timber brought in from Kalimantan, Indonesia. It had since undergone several renovations and restorations.\r\nThe mosque’s Javanese architecture is a testament to the existence of Islam in this country for more than 600 years. It features triple-tiered pyramidal roof and a square base and is one of the few mosques in this country with a pagoda in place of a minaret. The interior is a mixture of Malay, Chinese and Indonesian elements, with intricate carvings and wooden doors, windows, ceiling, beams, pulpit and four tiang agung (grand pillars).\r\nLocated next to the mosque is the tomb of Sultan Hussain Shah, the ruler of Johor and Singapore in the early 19th century, who was also the person responsible for giving away Singapore to Sir Stamford Raffles in 1819.', 1, 2.2042, 102.232, 'http://www.itc.gov.my/mosque/masjid-tengkera-tranquerah-mosque/', NULL, 3, 2, '2018-08-29 22:26:55', 2, '2018-10-01 08:03:00'),
(9, 'Kampung Kling Mosque', '062826526', '00:00:00', 'Km. 17, Jalan Tukang Mas', '', '', ' 75400 ', 'Melaka', 4, '', 1, 'Masjid Kampung Kling is located within the vicinity of the infamous Harmony Street, where houses of worship from various religions harmoniously coexist. Built in 1748 – like many Malaccan mosques built around that period – it displays strong Javanese and Oriental influences in its architecture. The mosque was reconstructed and renovated in 1872 and 1908 to replace the original wooden structure with one made of concrete. In 1999, after a restoration exercise by the Department of Museum and Antiquity, it was declared a historical monument.', 1, 2.1968, 102.247, 'http://www.itc.gov.my/mosque/masjid-kampung-kling/', NULL, 3, 2, '2018-08-29 22:30:48', 2, '2018-10-01 08:02:48'),
(10, 'Nyonya Memoirs', '018-6639696', '00:00:00', 'level 3, Dataran Pahlawan  Melaka Megamall', '', '', '75300', 'Melaka', 4, 'Adult: RM 52.00 (MyKad Holder) & RM 66.00 (Non MyKad Holder)\r\n\r\nChild (7-15yo) & Senior Citizen (60 yo and above): RM 26.00 (MyKad Holder) & RM 33.00 (Non MyKad Holder)', 1, 'Walk down memory lane with Nyonya Memoirs, a gorgeous walk-through stage-set of spellbinding beauty, coulours and textures. Come away with an unforgettable museum experience that is uniquely Peranakan.', 1, 2.1907, 102.253, 'http://www.nyonyamemoirs.com/', NULL, 3, 2, '2018-08-29 22:33:08', 2, '2018-09-25 06:28:10'),
(11, 'Kampung Hulu Mosque', '', '00:00:00', ' KM.1, KG. HULU', '', '', '75200 ', 'MELAKA', 4, '', 1, 'One of the oldest mosques in Melaka and in the country, Masjid Kampung Hulu has been around for more than 280 years. Situated in Banda Hilir, the mosque was built in 1728 by a Chinese immigrant convert named Datuk Arom @ Harun. Its original wooden structure had been replaced with concrete in 1892, except for its four main pillars and roof structure which were retained in their original forms.The mosque has a unique architectural style which combines distinct Chinese and Javanese elements. Its sturdy minaret bears resemblance to a lighthouse or a fortress tower. Its four main pillars are made of hardwood and support the main roof. The walls are believed to have been made from a mixture of sand, egg white and granite. Its interior includes adornments in the form of ceramic tiles and floor tiles from the Ching Dynasty, as well as a wooden pulpit from the original structure which still stands strong over the centuries. Outside, in the front yard, two ancient cannons guard its entrance.Not far from the mosque is the ancient tomb of Habib Abdullah Abu Bakar Al-Hadar, a Yemeni missionary, believed to be a hundred years old.', 1, 2.1992, 102.248, '', NULL, 3, 2, '2018-08-29 22:34:17', 2, '2018-09-25 06:32:01'),
(12, 'Al Azim Mosque', '06-2841142', '00:00:00', 'BUKIT PALAH', '', '', '75400 ', 'MELAKA', 4, '', 1, 'Masjid Al-Azim is Melaka’s State Mosque which is located in Melaka Tengah, approximately 3 kilometres from Melaka City. Built between 1984 and 1990, the mosque was officiated by the Yang Di-Pertuan Agong at that time, Sultan Azlan Shah, in July 1990.The mosque’s architectural style is a combination of Javanese and Chinese, with green temple-style and pyramidal roof structures. It features large windows for optimum natural lighting and ventilation. The square base of its main building matches its square triple-tiered roof, inspired by the first mosque built by Prophet Muhammad (PBUH) which was based on the four Islamic elements – the foundation or base represents ‘Syariah’ (Islamic Law), the first level represents ‘Tariqat’ (Spiritual Path), the second level represents ‘Haqiqat’ (Truth) and the third level represents ‘Makrifat’ (Spiritual Knowledge). Its 188-feet-tall minaret is in the shape of a pentagon, symbolising the five tenets of Islam.', 1, 2.2152, 102.262, 'http://www.itc.gov.my/mosque/masjid-al-azim-state-mosque/', NULL, 3, 2, '2018-08-29 22:37:46', 2, '2018-09-25 06:32:50'),
(13, 'Dataran Keris', '06-3241191', '00:00:00', 'Dataran Keris  Alor Gajah', '', '', '78000', 'Melaka', 4, '', 1, 'For the Alor Gajah community, the keris is a symbol of the splendor of the Malays especially during the Naning War. Therefore, the Keris Replication is the main identity of the Alor Gajah District.\r\n\r\nThe replica of the giant Keris is located on Keris Square while smaller sizes of Keris replicas can be found at the main entrance to the Alor Gajah District, Simpang Empat, Simpang Ayer Keroh - Durian Tunggal and Simpang Lubuk Cina.', 1, 2.3829, 102.21, 'http://www.mpag.gov.my/ms/dataran-keris-alor-gajah', NULL, 3, 2, '2018-08-29 22:37:46', 2, '2018-09-25 06:33:59'),
(14, 'Mini Malaysia & ASEAN Cultural Park Malacca', '06-232 1331', '00:00:00', 'Leboh Ayer Keroh,', '', '', '75450', 'Melaka', 4, '', 1, 'Mini Malaysia & ASEAN Cultural Park is a cultural park where impressive replicas of traditional Malaysian and ASEAN homes can be seen. Similar to the Karyaneka Handicraft Center in KL, each house contains furnishings, fixtures and works of art depicting the culture of each state or country. Located in Ayer Keroh, the Mini Malaysia & ASEAN Cultural Park showcases the country''s 13 states represented with houses that actually look alike to the untrained eye (save for the Borneo house). However, inside each abode is where the differences can be seen with life-sized wax dummies dressed in respective traditional costumes as well as local handicrafts. At the Mini Malaysia Complex there is also a model of an Orang Asli village.', 1, 2.2825, 102.304, 'http://www.malacca.ws/attractions/taman-mini-asean.htm', NULL, 3, 2, '2018-08-29 22:40:07', 2, '2018-09-25 06:35:07'),
(15, 'Masjid Cina Melaka', '010-2207082', '00:00:00', 'Masjid Cina Melaka, Jalan Solok Hilir , Paya Rumput ', '', '', '75260 ', 'MELAKA', 4, '', 1, 'The mosque is estimated to cost about RM7.5 million and would cater to all Muslim near Krubong. The mosque is being built on a 2.4 ha land. It has traditional Chinese architecture and could accommodate over 2,000 people to carry out their prayers and obligations at one time.  It will definitely become a tourist attraction for its unique Chinese architecture. The mosque design is a combination of architectural designs of several mosques in Beijing, Shanghai and Xian. It would have a natural ventilation concept.\r\nThe construction of the mosques’s main building began earlier this year through an allocation of RM5.9 million from the Federal Government which was approved by Deputy Prime Minister, Tan Sri Muhyiddin Yassin in October 2011.  Currently the mosque is 62 percent completed. The balance will be funded by the Melaka Chinese Muslim Association and public.  The mosque not only be a place of worship but also a place where community members can share their rich culture in this case of the rich Chinese culture and practices.\r\nThe association has also applied a 20 acres land next to the mosque to be developed into a Chinese Muslim community where shops and stalls will be set up. It will become an ideal center to facilitate the association’s activities and programs for the community. ', 1, 2.2957, 102.23, '', NULL, 3, 2, '2018-08-29 22:42:02', 2, '2018-09-25 06:35:49'),
(16, 'Malacca Butterfly & Reptile Sanctuary', '06-232 0033', '00:00:00', 'Lebuhraya Ayer Keroh', '', '', ' 75450', 'Ayer Keroh', 4, '', 1, 'Butterfly & Reptile Sanctuary is a tourist attraction in Ayer Keroh, about 15km northeast of Malacca town. Also known as Taman Rama Rama, it is home to a collection of animals ranging from beautiful butterflies to snakes, lizards, crocodiles, koi fish and even a pair of gorgeous golden leopards. Spread across an 11ha jungle area, the complex is part secondary jungle and part landscaped park. The well-maintained sanctuary was opened in 1991 and is divided into five sections: the Koi River Valley, Butterfly Garden, Reptile Aviary, Wild Photo Lane and Nature’s Art Centre.', 1, 2.2997, 102.311, 'http://www.malacca.ws/attractions/butterfly-reptile.htm', NULL, 3, 2, '2018-08-29 22:44:18', 2, '2018-09-25 06:36:47'),
(17, 'Wildlife Theatre Malacca', '06-231 3333', '00:00:00', 'Lot 101, Pulau Melaka', '', '', '75300', 'Bandar Hilir', 4, 'TICKET PRICE\r\nAdult \r\nWEEKDAYS - RM 15.00\r\nWEEKENDS, SCHOOL HOLIDAYS & PUBLIC HOLIDAYS - RM 20.00\r\n\r\nChild\r\nWEEKDAYS - RM 10.00\r\nWEEKENDS, SCHOOL HOLIDAYS & PUBLIC HOLIDAYS - RM 15.00\r\n\r\nSenior Citizen\r\nWEEKDAYS - RM 10.00\r\nWEEKENDS, SCHOOL HOLIDAYS & PUBLIC HOLIDAYS - RM 15.00\r\n\r\nSenior Citizen\r\nWEEKDAYS - RM 10.00\r\nWEEKENDS, SCHOOL HOLIDAYS & PUBLIC HOLIDAYS - RM 15.00\r\n\r\nOKU\r\nWEEKDAYS - RM 10.00\r\nWEEKENDS, SCHOOL HOLIDAYS & PUBLIC HOLIDAYS - RM 15.00\r\n\r\n** Prices are including 12.5% Entertainment Tax', 1, 'Wildlife Theatre provides engaging wild classroom style featuring sea lions'' educational programs and reptiles performances. Participants will be able to have a close-up interactive encounter with various types of animals thus enhancing learning experience.', 1, 2.1812, 102.251, 'http://www.destinationmelaka.my/listing/wildlife-theatre-melaka.html', NULL, 3, 2, '2018-08-29 22:47:02', 2, '2018-09-25 06:37:40'),
(18, 'Melaka Bird Park', '06-233 0333', '00:00:00', 'Melaka Bird Park', '', '', '75450 ', 'Durian Tunggal', 4, '', 1, 'Melaka Bird Park, hailed as the first open aviary bird park in Malacca, is built on a 1.8 hectare site at the Botanical Garden in Ayer Keroh, Malacca. It has the largest aviary in Malaysia and will be the only bird park to house such a large collection of Malaysian bird species. With nearly 700 bird species calling the lands and waterways of Malaysia home, the bird park aims to protect every species and educate the public by hosting a number of educational and recreational programmes. The exterior of Melaka Bird Park is impressive: it resembles a downscaled stadium with plenty of greenery and a massive model of a parrot perched on a welcome sign. A pond and mini river (with a few fish swimming inside) is also built with wooden bridges leading to the entrance. The bird park houses a collection of almost 700 bird species.\r\nOther facilities here include a restaurant, a souvenir kiosk, a herb garden, a man-made waterfall and, outside the park entrance, a boardwalk and a viewing tower (which was closed for repairs).', 1, 2.2856, 102.296, 'https://www.malaysia-traveller.com/melaka-bird-park.html', NULL, 3, 2, '2018-08-29 22:51:01', 2, '2018-09-25 06:38:40'),
(19, 'Melacca River', '', '00:00:00', 'Melaka River', '', '', '75300', 'Melaka', 4, '', 1, 'The Malacca River which flows through the middle of Malacca City in Malacca, Malaysia, was a vital trade route during the heyday of Malacca Sultanate in the 15th Century \r\nMelaka River was once dubbed the ‘Venice of the East’ by European seafarers as it was a prominent port of entry for traders from Europe and Asia during the late 16th century. Spanning a total distance of 10km, it is also believed to be where Malacca was founded by Sumatran Prince Parameswara, who built his palace along the east side of the riverbank (at the foot of St. Paul’s Hill) in the 1400s', 1, 21145.6, 1021430, '', NULL, 3, 2, '2018-08-29 22:51:15', 2, '2018-09-25 06:39:58'),
(20, 'Melaka Crocodile Park', '06-232 2350', '00:00:00', 'Melaka Crocodile & Recreation Park, Lebuhraya Ayer Keroh', '', '', '75450', 'Ayer Keroh', 4, '', 1, 'The Malacca Crocodile Farm, more commonly known as Taman Buaya Melaka, is one of the most popular tourist attractions in Ayer Keroh, a suburb of Malacca just 10 minutes from the Ayer Keroh toll plaza. Family friendly and filled with a variety of croc species, the farm is not just home to over 100 species of crocs – it also houses five different attractions in the surrounding 3.5ha park: Malaysia in Miniature, Aviary Bird Park, Reptile House, Mamalia House and Water Recreation Park.\r\nFirst opened in 1987, the farm has some pretty cool species of crocodiles and alligators, ranging from an enormous American alligator, a stripy-looking spectacled caiman and pale Indian marsh mugger crocodiles. Featuring incredible live shows every weekend and school holidays at 11:30 and 14:30, the crocodiles are all housed in manmade, swamp-like conditions similar to their natural habitat. The reptile house features various snake species including this American Rattlesna', 1, 2.2769, 102.298, 'http://www.malacca.ws/attractions/malacca-crocodile-farm.htm', NULL, 3, 2, '2018-08-29 22:53:54', 2, '2018-09-25 06:42:05'),
(21, 'Pesona AnggunBeauty Care & Spa', '012-2826843', '00:00:00', '7-1 Jalan Bu 2, Taman Bachang Utama', '', '', '75350', 'batu berendam', 4, '', 1, 'A Beauty Center company that performs activities such as: Facial Sauna Herbal / Aromatheraphy Massage Aromatheraphy / Traditional Mandi aura Milk Lulur scrub / Ear Candling and others. We also offer wedding packages for future bride.', 1, 2.2281, 102.253, '', NULL, 3, 2, '2018-08-29 22:54:26', 2, '2018-09-25 07:01:47'),
(22, 'Philea Resort & Spa Melaka', '06-2893399', '00:00:00', 'Lot 2940, Jalan Ayer Keroh', 'Off Jalan Plaza Tol', '', '75450', 'Ayer Keroh', 4, '', 1, 'A 20-minute drive from the UNESCO World Heritage Site of Melaka, Philea Resort offers unique pine log accommodation with free Wi-Fi. It houses an outdoor pool and 4 dining options. Surrounded by greenery, rooms at Philea Resort & Spa are equipped with a flat-screen TV and a safety deposit box. The attached bathroom provides a bathtub and separate shower facility. Guests can relax with a spa massage or enjoy garden views while exercising in the fitness room. Travel arrangements can be made at the tour desk. Other facilities include karaoke rooms and a 24-hour business centre. Cravo Cravo serves European specialities, while Nusantara offers Asian and international delights. Beverages and snacks are available at the resort’s ChillOut Bar and Tropics Lounge. Located in Ayer Keroh, Philea Resort & Spa is a 90-minute drive from Kuala Lumpur International Airport. Several 27-hole golf courses are within a 15-minute drive from the resort.', 1, 2.2981, 102.312, '', NULL, 3, 2, '2018-08-29 22:57:31', 2, '2018-09-25 07:02:15'),
(23, '''AdnVilla Muslim-Adult-Only Honeymoon Homestay & Spa Melaka', '012-2223070', '00:00:00', 'lot 187,12, Jalan Melaka Raya 28', '', '', '75000', 'bandar hilir', 4, '', 1, 'Suitable for newly bride or couples that want to celebrate their meaningful days. There are variety of packages includes in this villa with affordable price. ', 1, 2.1812, 102.26, '', NULL, 3, 2, '2018-08-29 23:00:44', 2, '2018-09-25 07:15:59'),
(24, 'Taming Sari Tower', '06-2881100', '00:00:00', 'Jalan Merdeka, ', '', '', '75000', 'Bandar Hilir', 4, '                           Dewasa (RM)	Kanak-Kanak (RM)\r\nWarganegara	        RM17	            RM11\r\nPelancong Asing	RM23	            RM15', 1, 'From a height of 80 meters, the ride offers you a spectacular and panoramic view of Melaka UNESCO World Heritage City and a far with a host of interesting sights such as St.Paul''s Hill, Independence Memorial Building, Samudra Museum and the ship, Flor De La Mar, Dataran Pahlawan, Pulau Selat Mosque, Pulau Besar and the Straits of Melaka. That apart you will also see the fast and rapid development taking place in the state.\r\n\r\nMelaka Menara Taming Sari (Taming Sari Tower) officially opened for business on the 18th of April 2008, Menara Taming Sari is the first and only gyro tower in Malaysia so far. Measuring 110 meters in height, its revolving structure offers a 360-degree panoramic view of Malacca town and beyond. Located in the popular district of Bandar Hilir on Jalan Merdeka, only 3 minutes'' walk from Mahkota Parade Shopping Complex and Dataran Pahlawan Megamall, the tower is named after the Taming Sari keris, a mythical weapon said to possess mystical powers belonging to the legendary Malay warrior, Hang Tuah. Even the structure''s design follows the shape of the keris, with the peak of the tower resembling the hilt.', 1, 21127, 1021450, 'https://menaratamingsari.com/', NULL, 3, 2, '2018-08-30 09:04:34', 2, '2018-09-14 07:50:02'),
(25, 'A Famosa', '065520888', '00:00:00', 'Jalan Parameswara, Bandar Hilir', '', '', '78000', 'Alor Gajah', 4, '', 1, 'A''Famosa is more than just quick photo stop opportunity for tourists. Built in 1511, the settlement used to sprawl across a whole hillside but now only a lone gate (Porta de Santiago) remains. \r\nOne of the oldest surviving European architectural remains in Asia; it is set beside the Istana to Sultanan on Jalan Kota. A''Famosa is perhaps Malacca''s best known sightseeing spot. \r\nOriginally constructed by Alfonso de Albuquerque (who led the Portuguese invasion on the Malacca Sultanate), the remains of the fort is now a crumbling whitewashed gatehouse and is located downhill from St. Paul''s Church.', 1, 2.4466, 102.209, 'https://www.afamosa.com/', NULL, 3, 2, '2018-08-30 09:18:36', 2, '2018-09-25 07:16:47'),
(26, 'Pak Putra Tandoori', '012-601 5876', '00:00:00', 'Jalan Kota Laksamana 2/3 & Jalan Kota Laksamana 2/4, ', 'Taman Kota Laksamana ', '', '75200', 'Melaka', 4, '', 1, 'Restaurant Pak Putra Tandoori is a restaurant that comes with the cuisine from North Indian and Pakistan. The menu at Pak Putra is pretty affordable, apart from the different types of naans and tandoori chicken available, there is also a good selection of vegetables, meats and seafood cooked in various styles.', 1, 2.1951, 102.244, 'https://travelling-foodies.com/2016/11/28/pak-putra/ http://www.rebeccasaw.com/march-2011-melaka-trip-pak-putra-tandoori-naan-restaurant/', NULL, 3, 2, '2018-08-30 09:22:11', 2, '2018-09-25 07:17:30'),
(27, 'Hang Jebat Mausoleum', '062836538', '00:00:00', 'Melaka City', '', '', '75450', 'Melaka', 4, '', 1, 'Hang Jebat Mausoleum is a burial ground along Jalan Kampung Kuli in Malacca dedicated to the legendary warrior Hang Jebat. Also known as Hang Jebat tomb, the crypt clearly predates the 1512 Portuguese occupation, but its exact date of construction (as well as its authenticity) remains a mystery. The well-maintained grave is an Acehnese-style tomb usually used to mark the burial grounds of sultans and high-ranking ministers. Jawi writings above the front entrance of the crypt mark it as the ''actual'' burial ground of Hang Jebat: but there is another crypt in Terendak Camp, an army base northwest of the state billed as Hang Jebat''s final resting place as well.\r\nHang Jebat''s famous words ''Raja adil raja disembah, raja raja zalim disanggah'' (a fair king is a king to obey, a cruel king is a king to fight against), defined his character, life and death. According to the Sejarah Melayu (Malay Annals), Hang Jebat was a high ranking Malay warrior who was part of a legendary quintet that included Hang Tuah, Hang Kasturi, Hang Lekiu and Hang Lekir. He defied the Sultan when fellow warrior and sworn brother, Hang Tuah was falsely accused of seducing the Sultan''s favorite dayang (consort).', 1, 2.1966, 102.249, 'http://www.malacca.ws/attractions/hang-jebat-mausoleum.htm', NULL, 3, 2, '2018-08-30 09:23:21', 2, '2018-09-25 07:18:13'),
(28, 'Hang Tuah’s Well (Perigi Hang Tuah)', '06-282 6526', '00:00:00', 'Kampung Duyong', '', '', ' 75430', 'Melaka', 4, '', 1, 'The story of the ultimate Malacca warrior, Hang Tuah, is the stuff of legends. Faithful serving Sultan Mansur back in the 15th century, this infamous historical figure had an exemplary record of achievements, which included successful warding off advances from Siam and Acheh, and a winning duel with Taming Sari, a warrior from the court of Majapahit. Born in Bentam Island, Sumatra, Hang Tuah was brought up in Kampong Duyong just 5 kilometers away along with his four good friends, Hang Jebat, Hang Kasturi, Hang Lekir and Hang Lekiu, who were also warriors in the Sultan''s court. Being the Sultan''s favorite, Hang Tuah''s loyalty and determination to the Sultan is unquestionable.\r\nThe Hang Tuah well is located in the town of his birthplace, Kampung Duyung. According to folklore, the great man himself had dug the well for his own personal use. After he had passed away, it is said that the well has become the dwelling of his spirit, which took on the form of a white crocodile. It is also believed that the albino crocodile cannot be seen by just anyone; only the holy and the pure-hearted ever get the opportunity to catch a glimpse of it. Another mystical aspect to the Hang Tuah well is that its water remains clear even after all these years, which is said to never dry up, even during long periods of drought. The locals believe that the well''s water contains special healing properties, able to cure all sorts of ailments. It was also said that the well, used to be much smaller in size originally, and that it had grown over time.', 1, 2.2001, 102.297, 'http://www.malacca.ws/attractions/perigi-hang-tuah.htm', NULL, 3, 2, '2018-08-30 09:28:59', 2, '2018-09-25 06:55:23'),
(29, 'Cendol Jonker 88', '012-7564441', '00:00:00', '88, Jalan Hang Jebat', 'Km. 2,  Tengkera', '', '75200', 'Melaka', 4, '', 1, 'Bask in the historical vibes of Jonker Street and then drop by Jonker 88, a modern kopitiam that is famous for its creamy and satisfying cendol. Cendol is a kind of dessert made from rice flour served with ice-shavings, red beans, and drizzled with a generous amount of coconut milk and palm sugar syrup.', 1, 2.1951, 102.247, 'https://www.havehalalwilltravel.com/blog/the-ultimate-guide-to-halal-food-in-melaka/', NULL, 3, 2, '2018-08-30 09:31:03', 2, '2018-09-25 06:56:10'),
(30, 'Proclamation of Independence Memorial', '062841231', '00:00:00', 'Memorial Pengisytiharan Kemerdekaan, Jalan Parameswara', '', '', '75000', 'Bandar Hilir ', 4, '', 1, 'Malacca Proclamation of Independence Memorial displays records and photographs on the early history of the Malay Sultanate. It''s divided into several sections, the memorial also houses an extensive timeline covering the country''s journey to independence and the development of modern Malaysia. Set up in 1912 in an elegant Dutch colonial mansion that used to house the Malacca Club making it the social center of British colonial Melaka. Previous guests included novelist Somerset Maugham as well as an extensive selection of Malacca''s glitterati back in those days. The two golden onion domes flanking the top of the Proclamation of Independence Memorial''s portico beautifully embellish the already-stylish stark white stucco.\r\nThe Proclamation of Independence Memorial has permanent exhibits of historical photographs, dioramas, manuscripts, old currency, slide shows and films chronicling Malaysia''s fight for independence, all displayed in chronological order. Check out the beautiful table that the first Prime Minister Tunku Abdul Rahman used during the declaration of Merdeka. There is also an outdoor exhibit of the limousine used to convey the first prime minister to the site where he declared Malaysia''s independence. ', 1, 2.1919, 102.251, 'http://www.malacca.ws/attractions/proclamation-independence-malacca.html', NULL, 3, 2, '2018-08-30 09:34:45', 2, '2018-09-25 06:57:34'),
(31, 'Medan Ikan Bakar Muara Sungai Duyong', '6012-636 2339', '00:00:00', 'KM4-5, Jalan Padang Temu, Permatang Pasir', 'Km. 2,  Tengkera', '', '75460', 'Melaka', 4, '', 1, 'The queue to get a table is long.Suggested to book first before arriving.Have to order at the order station and pick out the seafood and everything looked fresh. We didnt wait too long for the food and everything was so tasty and fresh. What amazing is how fast they clear the table after each customer. They were like 6/7 guys clearing and cleaning the table. They even sweep and mop the area. Overall a good place for dinner.\r\nIf not booking before arriving, after you are seated, you will need go and queue to order the food dishes, pick your preferred seafood, etc. Food will be sent to you based on the table number. \r\nThe wait can be quite long so i would suggest for you to eat something light before heading here.', 1, 2.1747, 102.289, '', NULL, 3, 2, '2018-08-30 09:38:03', 2, '2018-09-25 07:05:06'),
(32, 'Dataran Pahlawan Megamall', '06-282 1828', '00:00:00', 'F4-04, Dataran Pahlawan Melaka Megamall, Jalan Merdeka', '', '', '75000 ', 'Bandar Hilir', 4, '', 1, 'Well located near major tourist attractions. ample parking available. traffic is a hassle. many outlets available for shopping and food.\r\nHoliday crowd makes the Pahlawan Mall vibrant and busy. Good place to hangout as many choices for food and coffee or shopping. Many branded as well as local stalls for shoppers. Connected to Mahkota Parade (another big mall) and besides a few hotels. Also located in sight seeing areas e.g. the Portuguese Fort and St Paul''s hill...great location.', 1, 2.1901, 102.253, 'http://www.dataranpahlawan.com/', NULL, 3, 2, '2018-08-30 09:41:51', 2, '2018-09-25 07:08:18'),
(36, 'Klebang Original Coconut Shake', '013-3994016', '00:00:00', 'Lot 130, Solok Kampung Bahagia, Klebang Besar, ', 'Km. 2,  Tengkera', '', '75200', 'Melaka', 4, '', 1, 'Klebang Original Coconut Shake is an establishment at Klebang Besar, Malacca. Naming themselves the original coconut shake may come with more pressure than one might think, but no matter because this once a small stall, delivers and more. One of the more famous coconut shake establishment and one of the most famous go to places in Malacca, Klebang Original have been enjoying their success for quite some time now. Creamy, fragrant and refreshing, a Klebang Coconut Shake. The reason behind the popularity of Klebang Original lies with their perfectly concocted cup of coconut shake. Comes in two options, regular and special, each cup is filled with fresh coconut juice, a few coconut pieces, vanilla ice cream, and ice. The only difference between the two other than the price is the addition of a scoop of vanilla ice cream in the ‘special’ version. They also offer the normal coconut juice, sugarcane drink, and a few titbits to accompany your shake. ', 1, 2.2157, 102.207, 'https://www.facebook.com/KlebangOriginalCoconutShake/ http://www.foodadvisor.my/best-coconut-shake-melaka', NULL, 3, 2, '2018-08-30 09:43:37', 2, '2018-09-25 07:20:02'),
(37, 'AEON Bandaraya Melaka', '1-300-80-2366', '00:00:00', 'No.2 Jalan Legenda, Taman 1 - Legenda', '', '', '75400 ', 'Melaka', 4, '', 1, 'AEON Melaka is a sprawling complete shopping experience for consumer goods at very reasonable prices. the shopping experience is further enhanced by presence of good eateries. the drawback is the location away from the town centre; personal transportation is a necessity.It is big, with good & wide retail mix, has a nice ''upmarket'' ambience , a big nice Aeon departmental store & supermart. Here, one can find alot of mid market quality branded shops & dining not found in the other malls here.', 1, 2.2141, 102.247, 'http://www.aeonretail.com.my/promotion/store-locations/store-and-shopping-centre/location_detailBM.php', NULL, 3, 2, '2018-08-30 09:44:36', 2, '2018-09-25 07:22:50'),
(38, 'Elements Mall Melaka', '017-981 3966', '00:00:00', 'Jalan Melaka Raya 23, Hatten City', '', '', '75000 ', 'Melaka', 4, '', 1, 'Elements Mall is Melaka''s largest shopping mall strategically located on prime acreage within a historic city centre listed as UNESCO World Heritage City.\r\nNestled along the straits waterfront, Elements Mall will bring together the immediate catchment and beyond to meet, explore, shop, eat and drink. A carefully curated mix of inspiring tenants will ensure a vibrant, new retail and leisure destination.\r\nDedicated to being the ultimate value-shopping destination, shoppers could enjoy upscale outlet shopping at a wide range of retailers from desiger apparel, accessories, footwear and home furnishing with savings of 25 to 65 per cent every day.', 1, 2.183, 102.263, 'http://elementsmall.com.my/', NULL, 3, 2, '2018-08-30 09:47:22', 2, '2018-09-25 07:23:25'),
(39, 'Calanthe Art Café ', '06-292 2960 ', '00:00:00', 'Jalan Hang Kasturi, ', 'Km. 2,  Tengkera', '', '75200 ', 'Melaka', 4, '', 1, 'Forget Starbucks when you’re in Malacca, and head to one of the best coffee places in the city, Calanthe Art Cafe. Located in a narrow alley just off the well-known Jonker Street, the cafe is like a quiet oasis, even though the decor doesn’t quite say the same thing – it is vibrantly furnished, with eclectic and creative touches such as rattan furniture hanging from the ceiling, hand-painted walls, and little trinkets on shelves throughout the property. It might not sport the famous modern minimalist look every cafe is going for nowadays, but we can’t help but love this feast for wandering eyes.\r\nCalanthe might be one good-looking cafe, but its wide array of coffee selections is what makes it famous. With the tagline ‘The First and Only One’, Calanthe is unique, serving specialty coffee drinks from all the 13 states of the country, so if you’re an adventurous coffee aficionado, this is definitely something you want to try. Besides that, the cafe also offers one-of-a-kind blends such as green tea coffee (green tea and coffee with whipped cream on top) and ‘Jealousy,’ a rich espresso blend with a dollop of mint paddy syrup; and ice-blended drinks such as ‘Cendol Ice Blenz’ (palm sugar and coconut milk topped with red beans and cendol) and ‘Pink Lady’, a concoction of vanilla, ice, whipped cream and strawberry sauce. Prices are more than attractive, with a glass of one of these servings priced less than RM9!', 1, 2.1958, 102.247, 'https://www.facebook.com/calanthe.melaka/ http://www.malacca.ws/dining/calanthe-art-cafe.htm http://eatdrinkkl.blogspot.com/2015/08/calanthe-art-cafe-kaya-kaya-malacca.html', NULL, 3, 2, '2018-08-30 09:50:40', 2, '2018-09-25 07:24:19'),
(40, 'Asam Pedas Selera Kampung', '+606-2881799 +6', '00:00:00', 'No. G2, Jalan PM3, Mahkota Square, Bandar Hilir', 'Km. 2,  Tengkera', '', '75200 Melaka', 'Melaka', 4, '', 1, 'This restaurant was founded by Saharudin Mohd Nor. The 38-year-old settler''s son is talented teasing customers'' taste with spicy sour dishes that are a craze to the historic city''s Melaka residents. He began operating the business since July 2014 with the intention to introduce and promote Malay cuisine to foreign tourists and local patrons. The restaurant provides a special recipe of spicy sour milk specials that can be enjoyed with hot rice dishes and 10 different types of fish. Other fishes are also available according to customer preferences and preferences.', 1, 2.2006, 102.259, 'https://saji.my/restoran-asam-pedas-selera-kampung-di-melaka/ https://www.facebook.com/AsamPedasSeleraKampung/', NULL, 3, 2, '2018-08-30 09:56:45', 2, '2018-09-25 07:25:15'),
(41, 'Pantai Klebang', '012-3456789', '00:00:00', 'Klebang Besar', '', '', '75200', 'Melaka', 4, '', 1, 'Things to do at Pantai Klebang:\r\n1.Bustel-It is a budget accommodation concept of old buses in Melaka. Rates for the normal day are RM90 per night while on weekends and public holidays are RM130 per night.\r\nThe place is strategic. Located on the 1Malaysia Square, beside Klebang Beach and there are plenty of food stalls.\r\n2.Riding a pony-The rate charged in RM10 & RM15\r\n3.Horse Carriage-The carriage is decorated with colourful and lively lights. The horse cart also uses paddy horses and the rate charged is RM5 & RM10 per person\r\n4.ATV-The ATV is located on the coast. The rate is RM25 for seven rounds.', 1, 2.2164, 102.192, 'http://www.sayabackpacker.com/uncategorized/8-perkara-syok-layan-di-pantai-klebang-melaka/', NULL, 3, 2, '2018-08-30 09:58:15', 2, '2018-09-25 07:27:27'),
(42, 'Pantai Puteri Melaka', '', '00:00:00', 'Pantai Puteri, Mukim Tanjung Kling', '', '', '76400', 'Daerah Melaka Tengah', 4, '', 1, 'The Puteri Beach is a beach in Tanjung Kling, Central Melaka, Melaka, Malaysia. The beach used to be named Kundor Beach. In addition to the comfortable beach atmosphere, the cleanliness and splendor of Pantai Puteri are among the main attractions of visitors. The beach is equipped with prayer room, public toilets, restaurants, food stalls and hotels. Coupled with a variety of side activities including fishing, leisure, camping and picnicking, the beach is located about 15 kilometers from the center of the city with a wide range of family activities especially during the weekend, public holidays and school holidays. \r\nAlthough Malacca has many exciting tourist destinations in the three districts, namely Jasin, Alor Gajah and Central Melaka, Pantai Puteri has never lost in the number of visitors. Relaxing waves and panoramic views of the sun rise and sunset over the Malacca Strait along its 2.5-kilometer beach give more satisfaction to visitors.', 1, 2.2485, 102.137, 'http://www.utusan.com.my/berita/wilayah/melaka/keindahan-pantai-puteri-memukau-1.78021', NULL, 3, 2, '2018-08-30 10:00:44', 2, '2018-09-25 07:28:29'),
(43, 'Asam Pedas Claypot Melaka', '', '00:00:00', 'Restoran Kota Laksamana, Jalan Laksamana 1, Tamana Kota Laksamana', 'Km. 2,  Tengkera', '', '75000', 'Melaka', 4, '', 1, 'Claypot Spicy Acid is located at Jalan Laksamana 1, like its name, spicy sour claypot, something rare in a restaurant that provides spicy sour dishes. When it comes to ''asam pedas'', it is certain that the types of fish that are often used as the main ingredients of the cuisine. What''s more, this restaurant serves a ‘asam pedas’ tetel or a soft meat section mixed with veins and fats.', 1, 2.196, 102.243, 'https://saji.my/asam-pedas-melaka-famous/', NULL, 3, 2, '2018-08-30 10:02:27', 2, '2018-09-25 07:29:04'),
(44, 'Pantai Tanjung Bidara', '', '00:00:00', 'Pantai Tanjung Bidara, Terendak', '', '', '78300', 'Masjid Tanah', 4, '', 1, 'The beach is located 10km from Masjid Tanah town and 2 km from Terendak Camp is the famous picnic beach in Melaka. The rugged, rugged and rugged coastal beaches of the country and the breeze of the language make the atmosphere very peaceful and pleasant.\r\n The Civil Defense Department''s Coast Guard Tower (JPAM) is also here to help people who are injured or worried while they are in the vicinity of Tanjug Bidara Beach.\r\n Facilities: Parking, children''s playground, surau, food court, public ', 1, 2.2921, 102.087, 'https://www.tempatmenarik.my/pantai-tanjung-bidara-tempat-peranginan-menarik-di-melaka/', NULL, 3, 2, '2018-08-30 10:03:15', 2, '2018-09-25 07:29:55'),
(45, 'Mamee Jonker House', '06-286 7666', '00:00:00', 'No. 46 & 48, Jalan Hang Jebat (Jonker Street), ', 'Km. 2,  Tengkera', '', '75200', 'Melaka', 4, '', 1, 'For those who are visiting Melaka, Jonker Walk/Street is a must visit place to shop, walk around, eat and take in the city’s heritage views. Among the many attractions available in this area, the Mamee Jonker House is probably the one of the more interesting ones where you can spend a few hours in.\r\nThe Mamee Jonker House is suitable for people of all ages. Adults like me will want to take a walk down the memory lane to learn more about this dearest childhood snack. As for the kids, they will definitely enjoy the workshops. It operates from two adjoining shop houses that still retain their original characteristics despite having gone through years of refurbishment and strengthening of structures. The ground floor has three sections: Merchandise, Mamee Museum and Mamee Cafe, while the first floor is for hands-on workshops.', 1, 2.196, 102.247, 'http://www.mameejonkerhouse.com/#', NULL, 3, 2, '2018-08-30 10:07:14', 2, '2018-09-25 07:30:34'),
(46, 'Pantai Pengkalan Balak', '03848666', '00:00:00', 'Pantai Pengkalan Balak', '', '', '76300', 'Masjid Tanah', 4, '', 1, 'Balak Pengkalan is an old town in Melaka. Pantai Pengkalan Balak is a tourist destination. This beach is an attractive destination in Malaysia. The extensive coastline accompanied by the beauty and uniqueness of fishermen''s villages has made the environment safer and peaceful. Besides being a famous picnic place in Melaka, locals or outdoors will come here to relax with their families.\r\nBy the Pengkalan Balak Beach, there are many restaurants and stalls selling a variety of food, clothing and games, so besides relaxing, you can also shop for shopping.\r\nThe beach is mostly visited by students from Melaka Matriculation College on weekends. Memorable events occur.', 1, 2.3054, 102.075, '', NULL, 3, 2, '2018-08-30 10:08:03', NULL, '2018-11-27 12:46:33'),
(47, 'Trishaw Ride', ' 06-232 6411', '00:00:00', 'Jalan Merdeka, ', '', '', '75000', 'Bandar Hilir', 4, 'RM 25 for four (4) site views – almost 20 minutes ride\r\nRM 35 for six (6) site views – almost 40 minutes ride\r\nRM 50 for a one hour ride', 1, 'Trishaws or becas can still be seen in Malacca town, but the trishaws now are tourist driven and beautifully decorated. The cost of the ride is RM40 per hour- each trishaw can sit by 2 average weight adult with probably a child. And there is a clear signboard located near Jonker street entrance and the clock tower that states clearly the rental rates.\r\n\r\nThis trishaws are quite happening- some come fully equipped with sound systems and PA speakers so as the rider takes the customers around, you can hear music coming out from the trishaw. There was a Malay family and one trishaw played Malay songs (which I love to listen), and another one was blasting Michael Jackson’s songs (that week I went happened to be his death anniversary).\r\n', 1, 21127, 1021450, 'http://runawaybella.com/travel/trishaw-ride-malacca/', NULL, 3, 2, '2018-08-30 10:10:11', 2, '2018-09-21 07:25:10'),
(48, 'Teddy Bear Café', '+606-281 9423', '00:00:00', 'F3-021,  F3-022, Level 3 Terminal Pahlawan', 'Km. 2,  Tengkera', '', '75000', 'Melaka', 4, '', 1, 'Teddy bear cafe Melaka is a unique cafe that allows customers to socialize in a comfortable and relaxing environment. They welcome their guests with cute teddy bears of many different sizes. For savoury dishes they serve dishes such as fried noodles, fried rice, mac and cheese, spaghetti, and burgers. For desserts, they have many different types of cakes at their dessert counter and they also serve unique waffles that comes in different colours and flavours. For drinks, they serve coffee with different types of bear latte art. Besides that, Coffee bear cafe Melaka also sells teddy bear merchandise such as teddy bear key chains, teddy bear snow globe and teddy bears in different colours and sizes.', 1, 2.8192, 102.2, 'https://www.facebook.com/pg/tbcmelaka/photos/?ref=page_internal http://vmo.rocks/item/coffee-bear-cafe-melaka/', NULL, 3, 2, '2018-08-30 10:11:28', 2, '2018-09-25 07:32:29'),
(49, 'Kuih Keria Antarabangsa Melaka', '012-6914026', '00:00:00', '3123C, Jalan Limbongan, Kampung Limbongan', 'Km. 2,  Tengkera', '', '75250', 'Melaka', 4, '', 1, 'Kuih keria is a traditional doughnut shaped Malay kuih made from flour and sweet potatoes, which are then fried and glazed with sugar. What’s different about Kuih Keria Antarabangsa is that they use palm sugar syrup (gula Melaka) to make the glaze – a clever way that adds more taste as compared to the conventional kuih keria.', 1, 2.2064, 102.219, 'https://www.havehalalwilltravel.com/blog/the-ultimate-guide-to-halal-food-in-melaka/', NULL, 3, 2, '2018-08-30 10:16:02', 2, '2018-09-25 07:33:03'),
(50, 'Kompleks Falak Al-Khawarizmi', '+606-385 2671', '00:00:00', 'Kampung Balik Batu,', ' Tanjung Bidara, ', '', '78300', ' Alor Gajah', 4, '', 1, 'Kompleks Falak Al-Khawarizmi is a facility provided by the Melaka State Government to study and explore the events and secrets of the celestial world. The complex was developed by the Melaka State Government through Melaka State Mufti Department in an effort to develop astronomy / astronomy in the community.\r\n\r\nThere are three main buildings in the Falak Al-Khawarizmi Complex, Observatory, Planetarium and Training Center. The complex is also the official location for hilal observations for Melaka state.\r\n\r\nWith the establishment of the Falak Al-Khawarizmi Complex, people will be able to study the Falak Science more deeply. In addition, it is hoped that the knowledge almost forgotten by today''s generation can be re-acclaimed as it was during its glorious past\r\n', 1, 21740.2, 1020500, 'https://www.alkhawarizmimelaka.com.my/', NULL, 3, 2, '2018-08-30 10:36:01', 2, '2018-09-21 07:25:10'),
(51, 'Malacca Zoo', '+60 6-232 4053', '00:00:00', 'Zoo Melaka', '', '', '75450', 'Ayer Keroh', 4, '                                 Dewasa (RM)	    Kanak-Kanak (RM)      Warga Emas (RM)\r\nWarganegara	           17.80	                7.10	                              7.10\r\nPelancong Asing	   23.70	              17.80	                            17.80', 1, 'The zoo was established in 1963, along with the National Zoo of Malaysia (Zoo Negara), but its management was later taken over by the Department of Wildlife and National Parks of Malaysia in 1979. This enhanced its operations and upgraded its services for the public''s benefit. It was used as a wildlife rescue base before it was opened to the public by the then Prime Minister, Tun Dr. Mahathir bin Mohammad on 13 August 1987. Although it has long been opened to the public, its primary roles as a rescue base and an animal sanctuary remain unchanged.[5][6][7] It now houses many birds, amphibians, reptiles, mammals, insects, and is the second largest zoo behind the National Zoo in Ulu Klang.\r\n\r\nMalacca Zoo was the first zoo in Malaysia to exhibit the critically endangered Sumatran rhinoceros. It also houses overseas as well as local beauties such as the white rhinoceros, Asian elephants, red panda, Malaysian gaur, the serow, the squirrel monkey, the molurus python, the grey wolf, the Mongolian wild horse, the green tree python, the giraffe, the blue-and-yellow macaw and also the Indochinese tiger as well as the Malayan tiger[8][9][10]\r\nNow, it is visited annually by 400,000 visitors; averaging 1093 visitors a day.[11][2] Although, in 2007, the zoo was visited by 619,194 visitors with an average of 1619 visitors per day; the highest number since it was opened. In that year, the zoo collected as much as RM 3.99 million (US$886,666); collecting a gain of RM 130,000.[4] It also maintains a healthy, cool green background by planting exotic and rare plants while labelling them. This can potentially cool the zoo in very hot days and symbolically mimics the rainforest. It also has a tram facility to help its visitors get from one place to another with the ticket prices as low as RM 2.00. In addition to that, it also has eating facilities at the A&Wrestaurant and a cafeteria near the watch tower at the Mini Safari located near Marina Zenorai.\r\n', 1, 2.27654, 102.299, 'http://www.melakazoo.com/', NULL, 3, 2, '2018-08-30 11:55:37', 2, '2018-09-21 07:25:10'),
(52, 'Malacca Tropical Fruit Farm', '+606-351 0130', '00:00:00', 'Pusat Latihan Pertanian Sungai Udang, ', '', '', '76300 ', 'Melaka', 4, 'TICKETS\r\nADULT = RM 4.00\r\nCHILDREN = RM 2.00\r\nBICYCLE = RM 5.00/hour\r\nFISHING = RM 10.00 / 3 hours', 1, 'MELAKA TROPICAL FRUIT FARM (MTFF), Sungai Udang, is a concept of Agro Tourism product with the "Farm Stay" concept in Melaka. Located in Sungai Udang on 173 acres of land, the journey is about 20 minutes from Melaka Town. The farm provides visitors an opportunity to experience nature up close at the same time catering to family outing. \r\n\r\nSports and recreation facilities include bicycle & jogging tracks, lake for kayaking and fishing, a mini zoo with ostrich, deer, and horses and accommodation in the form of chalets and hostel.\r\n\r\n Most exciting event during fruit season which is visitors will have the opportunity to experience for themselves the cool & serene surroundings at the farm apart having the opportunity to savoir fresh tropical fruits from the tree like durian, duku, rambai, rambutan and others. There are several packages and facilities are offered at minimum pax.\r\n', 1, 2.2909, 102.131, 'http://melakatropicalfruitfarm.blogspot.com/', NULL, 3, 2, '2018-08-30 12:07:45', 2, '2018-09-21 07:25:10');
INSERT INTO `place` (`id`, `name`, `contact_no`, `duration`, `address1`, `address2`, `address3`, `poscode`, `city`, `state_id`, `price_range`, `parking_type_id`, `detail`, `spending_type_id`, `latitude`, `longitude`, `url`, `operating_remark`, `status`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(53, 'Shore Oceanarium', '+606-282 9966', '00:00:00', 'Jalan Persisiran | 2F-01, ', 'The Shore Shopping Gallery, ', '', '75100 ', 'Melaka', 4, 'Adult \r\n(13 - 59 years old)\r\nChild \r\n(90cm & Above or 3 - 12 years old)\r\nSenior \r\n(60 years old & above)\r\n\r\nSingle Entry	\r\nRM33.00	RM23.50	RM23.50\r\n\r\n\r\nOptional Add On \r\n\r\nRM10.00	\r\n3d2 3D Interactive Package \r\n\r\nRM10.00\r\n Feeding Frenzy Package', 1, 'The Shore Oceanarium describes itself as the ''new premier tourist attraction in Melaka''. It is a fun and educational place to bring the kids and enjoyable for adults too.\r\n\r\nBeing brand new (opened in 2015) it looks clean and fresh and contains some of the latest in aquarium technology such as 3D movies and information boards (3D glasses are provided with a combo ticket).\r\n\r\nVisitors are encouraged to find answers to a quiz form using the information boards provided as you walk around the oceanarium. A completed form with the correct answers can be redeemed for a souvenir at the gift shop.\r\n\r\nHighlights\r\n•	Interactive Touch Pool where you are allowed to gently touch some of the creatures such as sea cucumbers (very squidgy) and bamboo sharks (rough skin).\r\n•	Jungle Quest. This place is not just about fish but also has exhibits on land creatures such as snakes, frogs, lizards and so on.\r\n•	Ocean Journey & Ocean Theatre is where they have the large fish tanks and an overhead aquarium where you can view patrolling sharks from underneath. There is also a sea shell museum and 3D movie theatre.\r\n•	Fresh water fish have their own section called River Story. The aquarium full of piranha fish is impressive.\r\n•	Kampung Penyu is a large pool containing sea turtles which you can feed with raw fish (purchased at the entrance). Nice to be able to see turtles close up but my daughter said it seems a bit cruel to keep turtles in a shopping mall. This exhibit is co-organised with the Fisheries Department Malaysia so hopefully the welfare of the turtles has been taken into account when designing the pool.\r\n', 1, 2.2029, 102.25, 'http://www.oceanariummelaka.com/', NULL, 3, 2, '2018-08-30 12:21:08', 2, '2018-09-25 07:34:57'),
(55, 'Malacca Stamp Museum', '+606) 333 3333', '00:00:00', 'Kompleks Warisan Melaka Jalan Kota, ', '', '', '75000', 'Bandar Hilir', 4, '                  Dewasa	                                        \r\nWarganegara	Bukan Warganegara\r\n                  RM3.00\r\n\r\n           Kanak-kanak\r\nWarganegara	Bukan Warganegara\r\n              RM 2.00', 1, 'Stamp is a very valuable asset in terms of art, finance and history to those who appreciate it. His fans will spend a lot of money, energy and time to ensure that rare stamps, because they have long been unpublished, are in their possession. Historically, stamps are created to facilitate payments for letters and packages sent through government-owned postal systems. \r\n\r\nAs such, it has a long and colourful history, as a diverse design. Stamps are one of the best and relatively inexpensive containers for recording events, nature and important figures in history in a country. Not surprisingly, the older the age of a stamp, the value is also higher. It is hoped that the Malacca Stamp Museum will indirectly reveal and disseminate knowledge on development, culture, education, sports, flora and fauna as well as the achievement of the country in the realm of the field to the public through stamps. \r\n\r\nAdditionally, the filling of the museum is expected to be a driving force for the people in all walks of life to appreciate and appreciate historical heritage through reading and understanding of the materials on display. The objectives are: Provide awareness to the public, especially children, about the history of stamp development in Malaysia and the rest of the world Encourage and develop stamp collection among Malaysians Spread information to the public on various historical events as well as those who are seeking either in Malaysia or elsewhere in the world, all of which are immortalized in stamps.\r\n', 1, 2.19373, 102.248, 'http://www.perzim.gov.my/ms/portfolio/melaka-stamps-museum/', NULL, 3, 2, '2018-08-30 12:37:01', 2, '2018-09-21 07:25:10'),
(56, 'Muzium Kapal/ Samudera (Flora De La Mar)', '+606 333 3333', '00:00:00', 'Kompleks Warisan Melaka Jalan Kota, ', '', '', '75000', 'Bandar Hilir', 4, '             Dewasa	                                               \r\nWarganegara	Bukan Warganegara	\r\n    RM5.00	      RM10.00	               \r\n\r\n             Kanak-kanak\r\nWarganegara	Bukan Warganegara\r\n   RM3.00	         RM6.00', 1, 'The Museum of the Ocean is a replica of the Flor de La Mar vessel built in early 1990 and was inaugurated by Prime Minister of Malaysia, Y.A.B Tun Dr. Mahathir Mohamad on June 13, 1994. Next to the Phase II Ocean Museum.\r\n\r\nThe Ocean Museum is located on Quayside Street near the mouth of the Melaka River. Built on the replica of Portuguese Ship measuring 34 meters high, 36 meters long and 8 meters wide.\r\n\r\nThe focus of this museum is the maritime history of Malacca and the splendor of Malacca as the Eastern Emporium. It also revealed the maritime political outcry in Melaka which proved that the disappearance of political power lost everything.\r\n\r\nThe exhibition at this museum reflects the development of trade from the Malay sultanate to the colonial period of Portuguese, Dutch, English and the Japanese occupation era in Malaya. Also exhibited were paintings, old maps, ship models and maritime-related and artefacts in Malacca.\r\n', 1, 2.19373, 102.248, 'https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjZ3f2ai63cAhXJpI8KHSQ9AW8Qjhx6BAgBEAM&url=http%3A%2F%2Fwww.perzim.gov.my%2Fen%2Fportfolio%2Fmuzium-samudera-flor-de-la-mar%2F&psig=AOvVaw24YBQLnEAqy1n4gyz0jV-f&ust=153215576091592', NULL, 3, 2, '2018-08-30 12:51:02', 2, '2018-09-21 07:25:10'),
(57, 'Malacca Sultanate Palace (Replika Istana Kesultanan Melayu Melaka)', '+606-282 6526', '00:00:00', 'Jalan Kota, ', '', '', '75000', 'Bandar Hilir', 4, '              Dewasa	\r\nWarganegara	Bukan Warganegara\r\n   RM3.00	   RM5.00	\r\n\r\n          Kanak-kanak\r\nWarganegara	Bukan Warganegara\r\n                RM2.00\r\n', 1, 'Palace of the Sultanate of Melaka (replica Palace of the Malay Sultanate of Melaka) is located at the foot of the St. Paul. The replica of the Palace was rebuilt based on a Malay History note that portrays the uniqueness of the Malay Palace architecture, during the reign of Sultan Mansur Shah who reigned from 1456 to 1477.\r\n\r\nTo showcase the general atmosphere of the Malacca Malay Sultanate Palace and certain spaces such as Balairong Seri, Balai Beradu and Characteristics of Traditional Malay Rulers during the Malay Sultanate of Malacca.\r\n\r\nTo show the majesty of the monarchy as a cultural heritage of the Malay Nation which once led the State of Malacca once again recalls the history of Hang Tuah and Hang Jebat''s histories and the teaching of the event.\r\n\r\nThe replicas of the castle were built in 1984 and served as the Cultural Museum when officiated by the Prime Minister of Malaysia on July 17, 1986. The whole building is made of hardwood while its roofs are purchased from timber. * The construction is characterized by the Melaka Malay House artwork where building joints are reinforced with pegs only.\r\n\r\nIn total, this museum exhibits the function and role of the castle at the time by displaying the Balairong Seri, Royal Jubilee Room and the halls representing the civilization of the Malay community.\r\n', 1, 2.1929, 102.25, 'http://www.pakuajihomestay.com/senarai-muzium-di-melaka.html ', NULL, 3, 2, '2018-08-30 12:59:28', 1, '2018-10-15 16:27:21'),
(58, 'Malacca Islamic Museum (Muzium Islam Melaka)', '+606-282 1303', '00:00:00', 'Jalan Kota, ', '', '', '75000', 'Bandar Hilir', 4, '', 1, 'The establishment of the Melaka Islamic Museum aims to make it a center for collecting, researching, conserving and exhibition of Islamic-oriented materials in line with the role of Melaka as the Center for the spread of Islam in the archipelago and the development of Islam in Malaysia today.\r\n\r\nThe Melaka Islamic Museum is housed in the old building of the Melaka Islamic Council (MAIM) Office, Jalan Kota, Melaka. The interior design of this museum is based on a glimpse of the beauty of Islam in Melaka and Nusantara by illustrating the combination of Islamic artistic identity globally and Islamic art in the Nusantara especially Malacca.\r\n\r\nThe exhibition and information delivery methods are processed through eight main exhibition halls that are clearly in line with certain themes. This is to make it easier for visitors to understand the message, historical journey and experience they want to deliver.\r\n\r\nAmong the exhibits on display are the earliest replica of the Quran, the history of the mosque, the replica of Rasulullah S.A.W sword.\r\n', 1, 21132.7, 1021460, 'http://www.pakuajihomestay.com/senarai-muzium-di-melaka.html', NULL, 3, 2, '2018-08-30 13:07:32', 2, '2018-09-21 07:25:10'),
(59, 'Malacca Literature Museum', '+606-284 1934', '00:00:00', 'Jalan Kota, ', '', '', '75000', 'Bandar Hilir', 4, 'Dewasa	\r\nWarganegara	  Bukan Warganegara\r\n    RM5.00	       RM10.00	\r\n\r\nKanak-kanak\r\nWarganegara	  Bukan Warganegara\r\n    RM2.00	      RM4.00', 1, 'The Literary Museum is located in the old building of the Malacca State Development Corporation (PKNM) which is close to the Stadthuys Building. \r\n\r\nThe premier museum in Malaysia that emphasizes on the history of writing and works from the early years of writing, People''s stories, Malay History, Hikayat Hang Tuah, Melaka Code of Law, Munsyi Abdullah to the development of Modern Malay Literature. \r\n\r\nThe main exhibition materials consist of paintings that illustrate important events based on the quotation of Malay History and the forms of punishment executed under the Law of the Melaka Code as well as manuscript manuscript collection of handwriting and all over the world. \r\n\r\nHonour is also given to traditional forms of writing including pantun, gurindam, nazam and poetry. National Literature Gallery and Melaka Writers are also part of the main attraction for visitors.\r\n', 1, 21137.7, 1021460, 'https://en.wikipedia.org/wiki/Melaka_Literature_Museum', NULL, 3, 2, '2018-08-30 14:21:50', 2, '2018-09-21 07:25:10'),
(60, 'Muzium Tuan Yang Terutama', '06-284 1934', '00:00:00', 'Jalan Kota,', '', '', '75000', 'Bandar Hilir', 4, '', 1, 'Formerly known as Seri Melaka was the former official residence and office of the Dutch Governor in Melaka in the 17th century. The function of the building continued during the English period until independence. In September 1996, the official residence and office of His Excellency moved from Bukit St. Paul to Melaka Palace in Bukit Beruang, Melaka.\r\n\r\nThe Function of His Excellency''s Institution (TYT) as the Supreme State Government is displayed to recall the existence of this institution. The Museum Principal featured on the Brief History of the Sultanate Institution, the Formation of the Governor''s Establishment to the Role and Institution of HE. In general, the role of the Chief Minister of Malacca is also infused because their role is of no importance in leading the state government.\r\n\r\nThe main focus will be on the Governor / Chief of Staff after independence who has served in Malacca from Tun Leong Yew Koh (First Governor), Tun Haji Abdul Malek bin Haji Yusuf (Second Governor), Tun Haji Abdul Aziz bin Abdul Majid (Third Governor), Tun Syed Zahiruddin bin Syed Hassan (Fourth Governor), and Tun Syed Ahmad Shahabudin Al-Haj (5th Fifth) by displaying various personal collections belonging to His Excellency.\r\n\r\nThe exhibition in this museum retains part of the use of the original building space. Apart from that, he also highlighted the contribution of HE to the development of the state such as industrialization, agriculture, tourism, administration, religion, etc. which grew from time to time while assisted by every Chief Minister under his leadership in developing the state of Malacca.\r\n', 1, 21135, 1021460, 'http://www.pakuajihomestay.com/senarai-muzium-di-melaka.html', NULL, 3, 2, '2018-08-30 14:34:40', 2, '2018-09-21 07:25:10'),
(61, 'Muzium Sejarah & Ethnografi @ Stadthuys (Museum of History and Ethnography)', '+606-282 6526Ja', '00:00:00', 'Jalan Gereja, ', '', '', '75000', 'Bandar Hilir', 4, 'Dewasa	\r\nWarganegara	  Bukan Warganegara\r\n    RM5.00	        RM10.00	\r\n\r\nKanak-kanak\r\nWarganegara	  Bukan Warganegara\r\n   RM2.00	        RM4.00', 1, 'The History and Ethnography Museum is located in the Stadthuys Building, its location at the junction of the City Road and Church Road, Melaka. In front of this building, better known as the ''Dutch Square'', there is the Victoria Jewel and the Tan Beng Seng Clock Tower which was built in 1886. \r\n\r\nThe Ethnographic Museum gives emphasis to the local life style of the various races - Malay, Chinese, Indian, Baba Nyonya, Chetti and Portuguese. An overview of wedding customs, the way of life, the equipment used, the choice of musical instruments and the form of farming is the main focus of this museum. \r\n\r\nThe exhibits are a collection of periodical porcelain, various types of weapons, currency from within and abroad and various another collection during the Dutch occupation in Melaka.\r\n\r\nThe History Museum is located on the first floor of Melaka''s history which is a pillar of Malaysian history today from the opening of Malacca to the colonial era of Dutch, English, and Japanese until independence. Based on historical paintings and artifacts, visitors will appreciate Melaka''s excellence from the beginning of the establishment, excellence enjoyed, struggle for the independence and the modernity of the existing\r\n', 1, 21138.6, 1021460, 'http://www.pakuajihomestay.com/senarai-muzium-di-melaka.html', NULL, 3, 2, '2018-08-30 14:57:18', 2, '2018-09-21 07:25:10'),
(62, 'Galeri Laksamana Cheng Ho', ' 06-284 1934', '00:00:00', 'Jalan Kota,', '', '', '75000', 'Bandar Hilir', 4, 'Dewasa	\r\nWarganegara	  Bukan Warganegara\r\n    RM5.00	        RM10.00	\r\n\r\nKanak-kanak\r\nWarganegara	  Bukan Warganegara\r\n   RM2.00	        RM4.00', 1, 'Admiral Cheng Ho''s Gallery is inspired by the story of China''s famous sailor Admiral Cheng Ho (Zheng Ho) who was born in 1371 in Hodai, a village in Bao San District in Yunnan. He was named Ma Ho and his surname San Bao. \r\n\r\nThe gallery features her cruising story to the Southern Ocean as well as her success in fostering a brotherhood relationship between China and the African and Asian Countries and opening the door to conducting a fair business. \r\n\r\nThe position of this gallery is on the right side of the Stadthyus Building (History and Ethnography Museum) and is located between the Literary Museum, Ethnographic & Ethnographic Museum and the Democratic Government Museum.\r\n\r\nAmong the exhibits are porcelain, ship replicas, book collections narrating the history of Admiral Cheng Ho and related articles. Admiral Cheng Ho''s Gallery was opened to the public in February 2003 along with the opening of the Malaysian History & Ethnography Museum and the Malaysian Youth Museum which has been reopened after being temporarily closed for the upgrade process.\r\n', 1, 21137.4, 1021460, 'https://en.wikipedia.org/wiki/Gallery_of_Admiral_Cheng_Ho', NULL, 3, 2, '2018-08-30 15:01:48', 2, '2018-09-21 07:25:10'),
(63, 'Muzium Adat Istiadat', '06-282 1303', '00:00:00', 'Dataran Keris, ', '', '', '78000 ', ' Alor Gajah', 4, ' Dewasa\r\n Warganegara \r\n RM5.00	   \r\n\r\nBukan Warganegara\r\n  RM10.00	\r\n\r\nKanak-Kanak\r\nWarganegara    \r\n  RM2.00\r\n \r\nBukan Warganegara\r\n  RM4.00', 1, 'Muzium Adat Istiadat terletak di Dataran Alor Gajah, Melaka, Malaysia, bersebelahan Tugu Keris Majlis Perbandaran Alor Gajah, kira-kira 25 km dari Bandar Melaka. Senibina tradisional Rumah Melayu adalah identiti utama muzium ini.\r\nFokus utama muzium merangkumi sejarah awal Daerah Alor Gajah termasuk gambar tokoh-tokoh daerah dan adat-istiadat perkahwinan dengan penekanan kepada Melayu Serantau. Bahan pameran termasuk pelamin, alat perhiasan diri, porselin dan peralatan adat istiadat.\r\nMuzium Alor Gajah dirasmikan oleh Ketua Menteri Melaka Tan Seri Abdul Rahim bin Datuk Tamby Chik pada 19 Oktober 1990, Bangunan ini dahulunya di bawah pengawasan Majlis Daerah Alor Gajah sebelum diambil alih oleh Perbadanan Muzium Melaka.\r\n', 1, 22300.1, 1021230, 'https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwj19YX7jK3cAhXLrI8KHaqaCm8Qjhx6BAgBEAM&url=http%3A%2F%2Ffairusmamat.blogspot.com%2F2010%2F09%2Fmuzium-adat-istiadat-melaka.html&psig=AOvVaw1mezX1Wbl979u-jdNev7aO&ust=15321562512122', NULL, 3, 2, '2018-08-30 15:15:59', 2, '2018-09-21 07:25:10'),
(64, 'Muzium Warisan Baba & Nyonya', '06-282 1273', '00:00:00', '48-50, Jalan Tun Tan Cheng Lock, ', '', '', '75200', 'Melaka', 4, 'Tiket Masuk - RM16', 1, 'The Chinese Straitsman, also known as Baba Nyonya, is a Chinese royalty who has embraced the value of Malay culture in their daily lives. It is a gradual process that takes more than 400 years since the great Chinese adventurer, Admiral Cheng Ho who started bringing the Chinese people to Melaka. \r\n\r\nAfter centuries, Baba Nyonya residents have formed a distinct and unique and unique culture on the west coast of Malaysia, especially in Melaka. \r\n\r\nThe public can now see the unique cultural artifacts associated with this culture in a charming private museum, operated by the Baba and Nyonya Melaka communities. The existence of this heritage building wall allows you to know more about this unique culture.\r\n', 1, 2.44303, 101.993, 'tps://www.caridestinasi.com/muzium-warisan-baba-dan-nyonya-melaka/', NULL, 3, 2, '2018-08-30 15:24:44', 2, '2018-09-21 07:25:10'),
(65, 'Submarine Muzium (Muzium Kapal Selam)', '+606 333 3333', '00:00:00', 'Pantai Klebang, Melaka, ', 'Klebang Besar, ', '', '75200', 'Melaka', 4, 'Harga Tiket\r\n\r\nDewasa- RM5.00/seorang\r\n\r\nKanak-Kanak (bawah 12 tahun) - RM2.00/seorang\r\n\r\nKanak-kanak (bawah 6 tahun) - Percuma', 1, 'The Submarine Museum is a museum about submarine in Klebang, Melaka. FS Ouessant Agosta 70 Submarine is a submarine training crew of the Royal Malaysian Navy''s Prime Minister''s Class (RMN). The acquisition of the FS Ouessant Agosta 70 submarine belonging to the former French Navy. The submarine was brought back to Malaysia by the Ministry of Defense and handed over to the Melaka State Government to be preserved as a museum at the Maritime Museum Complex, Berlian Beach, Klebang, Melaka.\r\nThe work to bring home the submarine FS Ouessant Agosta 70 from its base in Brest, France was started on October 9, 2011 and arrived in Melaka waters on November 12, 2011. The main focus of the FD Submarine Museum FU Ouessant Agosta 70 is the history of submarines and development especially in Malaysia\r\n', 1, 21250.4, 1021150, 'http://www.perzim.gov.my/en/portfolio/muzium-kapal-selam/', NULL, 3, 2, '2018-08-30 15:34:33', 2, '2018-09-21 07:25:10'),
(66, 'Legoland', '07-597 8888', '02:00:00', '7', 'Jalan Legoland', 'Medini', '79250', 'Nusajaya', 1, '', 1, '', 1, 1.42982, 103.639, 'https://www.legoland.com.my/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(67, 'Desaru Coast', '03-2203 9696', '02:00:00', 'Desaru Coast Adventure Waterpark', 'Persiaran Pantai, Desaru Coast', '', '81930', 'Bandar Penawar', 1, '', 1, '', 1, 1.53967, 104.263, 'https://desarucoast.com/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(68, 'Grand Palace Park', '07-223 0555', '02:00:00', 'Kurator', 'Muzium Diraja Abu Bakar', 'Istana Besar Johor', '80500', 'Johor Bahru ', 1, '', 1, '', 1, 1.45505, 103.756, 'http://www.malaysiahere.com/malaysia_museum/abu_bakar_museum.php', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:26:44'),
(69, 'Tanjung Piai Johor National Park', '07-696 9712', '02:00:00', 'Tanjung Piai Johor National Park', '', '', '82030', 'Pontian', 1, '', 1, '', 1, 1.28357, 103.502, 'https://www.malaysia-traveller.com/tanjung-piai.html', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:28:01'),
(70, 'The Old Temple of Johor Bahru', '07 - 277 7858', '02:00:00', 'Lot 653', 'Jalan Trus', 'Bandar Johor Bahru', '80000', 'Johor Bahru ', 1, '', 1, '', 1, 1.46061, 103.763, '', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:29:43'),
(71, 'Sultan Abu Bakar State Mosque', '07-223 4935', '02:00:00', 'Jalan Gertak Merah', 'Masjid Sultan Abu Bakar', '', '80000', 'Johor Bahru ', 1, '', 1, '', 1, 1.45685, 103.751, 'http://www.malaysia.travel/en/places/states-of-malaysia/johor/sultan-abu-bakar-mosque', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:31:12'),
(72, 'Pulau Besar', '', '02:00:00', 'Pulau Besar', '', '', '', '', 1, '', 2, '', 1, 2.43773, 103.982, 'http://johor.attractionsinmalaysia.com/The-Large-Island.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(73, 'Danga Bay', '', '02:00:00', 'Skudai', '', '', '80200', 'Johor Bahru ', 1, '', 1, '', 1, 1.47985, 103.72, 'http://www.countrygardendangabay.com.my', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:32:30'),
(74, 'Kota Tinggi Firefly Park', '017-460 7775', '02:00:00', 'Jalan Kota Tinggi', '', '', '81900', 'Kota Tinggi', 1, '', 1, '', 1, 1.70319, 103.913, 'https://www.facebook.com/ktfireflypark/', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:33:36'),
(75, 'Forestry Museum, Gunung Jerai', '06-651 2954', '02:00:00', 'Jalan Gunung Jerai', '', '', '08300', 'Gurun', 2, '', 1, '', 1, 5.80607, 100.435, 'https://www.timothytye.com/malaysia/kedah/gunung-jerai/muzium-perhutanan-gunung-jerai.htm', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:34:51'),
(76, 'Kota Kuala Kedah Museum', '04-741 2152', '02:00:00', 'Kota Kuala Kedah Museum', 'Jabatan Muzium Malaysia', 'Kampung Seberang Kota', '06600', 'Kuala Kedah', 2, '', 1, '', 1, 6.10871, 100.286, 'www.jmm.gov.my/en/museum/kota-kuala-kedah-museum', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:35:27'),
(77, 'Bujang Valley Archaeological Museum', '04-457 2005', '02:00:00', 'Lembah Bujang Archeological Museum', '', '', '08400', 'Pekan Merbuk', 2, '', 1, '', 1, 5.73834, 100.414, 'http://www.jmm.gov.my/en/museum/lembah-bujang-archaeological-museum', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:36:41'),
(78, 'Kedah Paddy Museum ', '04-735 1315', '02:00:00', 'Gunung Keriang', 'Alor Setar', '', '05150', 'Alor Setar', 2, '', 1, '', 1, 6.19168, 100.326, 'https://www.malaysia-traveller.com/kedah-paddy-museum.html', '', 3, 1, '2019-12-18 13:00:00', 1, '2019-02-18 07:37:23'),
(79, 'Galeria Perdana Langkawi', '04-959 1498', '02:00:00', 'Kampung Kiling', '', '', '07000', 'Langkawi', 2, '', 1, '', 1, 6.4033, 99.8539, 'http://www.jmm.gov.my/en/museum/galeria-perdana', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(80, 'Langkawi SkyCab', '04 - 959 4225', '02:00:00', 'Panorama Langkawi Sdn Bhd Cable Car Station', 'Oriental Village', 'Burau Bay', '07000', 'Langkawi', 2, '', 1, '', 1, 6.37143, 99.6714, 'http://www.panoramalangkawi.com/skycab/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(81, 'Bird Paradise Wildlife Park, Langkawi', '04 - 966 5855', '02:00:00', 'Lot 1485', 'Kampung Belanga Pecah', 'Jalan Ayer Hangat', '07000', 'Langkawi', 2, '', 1, '', 1, 6.38803, 99.862, 'https://langkawiwildlifepark.com/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(82, 'Tree Top Walk Sungai Sedim', '04-490 1588', '02:00:00', 'Kampung Sedim', '', '', '09700', 'Karangan', 2, '', 1, '', 1, 5.41349, 100.784, 'https://www.tripadvisor.com.my/Attraction_Review-g298281-d1733716-Reviews-Tree_Top_Walk_Sungai_Sedim-Kedah.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(83, 'Gunung Keriang Recreation Park', '04-733 8504', '02:00:00', 'Kampung Gunung Hulu', '', '', '65550', 'Alor Setar', 2, '', 1, '', 1, 6.18718, 100.335, 'https://www.malaysia-traveller.com/gunung-keriang.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(84, 'Kedah State Art Gallery ', '04 - 732 5752', '02:00:00', 'Bandar Alor Setar', '', '', '05000', 'Alor Setar', 2, '', 1, '', 1, 6.11904, 100.366, 'https://www.timothytye.com/malaysia/kedah/alor-setar/balai-seni-negeri.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(85, 'Pasar Besar Siti Khadijah', '', '02:00:00', 'Jalan Buluh Kubu', 'Bandar Kota Bahru', '', '15000', 'Kota Bahru', 3, '', 1, '', 1, 6.13037, 102.239, 'http://kelantan.attractionsinmalaysia.com/SitiKhadijahMarket.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(86, 'Kota Seri Mutiara', '09-7433221', '02:00:00', 'Jalan Pasir Puteh', 'Kota Sri Mutiara 97', 'Bandar Kota Bahru', '15200', 'Kota Bahru', 3, '', 1, '', 1, 6.11381, 102.25, 'http://kelantan.attractionsinmalaysia.com/Shopping.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(87, 'Masjid Kampung Laut (Mosque Kampung Laut)', '09-7412400', '02:00:00', 'Jalan Kuala Krai', '', '', '16010', 'Kota Bahru', 3, '', 1, '', 1, 6.02746, 102.241, 'http://kelantan.attractionsinmalaysia.com/MasjidKampungLaut.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(88, 'Museum Islamic', '', '02:00:00', 'Kelantan State Museum Corporation', 'Jalan Hospital', '', '15000', 'Kota Bahru', 3, '', 1, '', 1, 6.13218, 102.236, 'http://kelantan.attractionsinmalaysia.com/MuseumIslamic.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(89, 'Moonlight Beach', '', '02:00:00', 'Pantai Cahaya Bulan', '', '', '15350', 'Kota Bahru', 3, '', 1, '', 1, 6.19653, 102.274, 'http://kelantan.attractionsinmalaysia.com/PantaiCahayaBulan.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(90, 'Temenggong Street', '09-7477520', '02:00:00', 'Jalan Temenggung', '', '', '15000', 'Kota Bahru', 3, '', 1, '', 1, 6.12782, 102.237, 'http://kelantan.attractionsinmalaysia.com/TemenggongStreet.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(91, 'Kuala Krai Dabong River Cruise ', '09-7477520', '02:00:00', 'Kuala Krai', '', '', '15050', 'Kota Bahru', 3, '', 1, '', 1, 5.53337, 102.199, 'http://kelantan.attractionsinmalaysia.com/KualaKraiDabongRiverCruise.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(92, 'Bank Kerapu (War Museum)', '09-7482266', '02:00:00', 'Perbadanan Muzium Negeri Kelantan', 'Jalan Hospital', '', '15000', 'Kota Bahru', 3, '', 1, '', 1, 6.13207, 102.235, 'http://kelantan.attractionsinmalaysia.com/WarMuseumKelantan.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(93, 'The Royal Museum Istana Batu ', '', '02:00:00', 'Jalan Istana', 'Bandar Kota Bahru', '', '15000', 'Kota Bahru', 3, '', 1, '', 1, 6.13238, 102.237, 'http://kelantan.attractionsinmalaysia.com/TheRoyalMuseum.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(94, 'Gunung Stong State Park', '09-7482140', '02:00:00', 'Dabong Forest Reserve', '', '', '18200', 'Kota Bahru', 3, '', 1, '', 1, 5.33979, 101.974, 'http://kelantan.attractionsinmalaysia.com/GunungStongStatePark.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(95, 'Kota Lukut', '06-6512954', '02:00:00', 'Lot 730', 'Jalan Besar', 'Taman Pd Jaya', '71010', 'Port Dickson', 5, '', 1, '', 1, 2.5685, 101.823, 'http://portdickson.info/kota-lukut.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(96, 'Gunung Datuk Recreation Forest', '017-2278003', '02:00:00', 'Gunung Datuk Recreation Forest', '', '', '71350', 'Kota', 5, '', 1, '', 1, 2.54334, 102.169, 'https://www.malaysia-traveller.com/gunung-datuk.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(97, 'Alive 3D Art Gallery', '06-6515400', '02:00:00', 'No 38-40', 'Jalan Ds 2/1', 'Bandar Dataran Segar', '71010', 'Port Dickson', 5, '', 1, '', 1, 2.55986, 101.817, 'https://www.facebook.com/alive3dartgallery/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(98, 'Upside Down Port Dickson (Rumah Terbalik Port Dickson) ', '016-3368751', '02:00:00', 'Rumah Terbalik Port Dickson', 'Jalan Pantai', '', '71050', 'Port Dickson', 5, '', 1, '', 1, 2.44707, 101.86, 'https://upside-down-port-dickson-rumah-terbalik-port.business.site/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(99, 'Jelita Ostrich Farm', '06-767 0707', '02:00:00', 'Lot 1504', 'Batu 6', 'Mukim', '70100', 'Pantai', 5, '', 1, '', 1, 2.75999, 101.993, 'https://www.facebook.com/Jelita-Ostrich-Show-Farm-178226442231879/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(100, 'Pusat Ikan Hiasan Teluk Kemang', '06-6621089', '02:00:00', 'Jalan Kemang 8', 'Kampung Baharu', '', '71050', 'Port Dickson', 5, '', 1, '', 1, 2.46284, 101.851, 'https://www.facebook.com/Pusat-Ikan-Hiasan-PD-217337065279115/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(101, 'Muzium Tentera Darat', '06-6462359', '02:00:00', 'Kem Sirusa', 'Persiaran Pahlawan', 'Kampung Baru Sirusa', '71050', 'Port Dickson', 5, '', 1, '', 1, 2.49655, 101.847, 'http://portdickson.info/army-museum.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(102, 'Pusat Rekreasi Ulu Bendul', '06-4811036', '02:00:00', 'Pusat Rekreasi Ulu Bendul', 'Hutan Lipur Ulu Bendul', '', '71500', 'Tanjung Ipoh', 5, '', 1, '', 1, 2.73051, 102.078, 'http://www.mdkp.gov.my/ms/hutan-lipur-ulu-bendul-kuala-pilah', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(103, 'Wet World Air Panas Pedas Resort ', '06-6857199', '02:00:00', 'Lot 603', 'Jalan Tampin', 'Mukim Pedas', '71400', 'Pedas', 5, '', 1, '', 1, 2.63249, 102.055, 'http://wetworld.my/air-panas-pedas/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(104, 'Masjid Negeri ', '', '02:00:00', 'Jalan Dato Hamzah', '', '', '70000', 'Seremban', 5, '', 1, '', 1, 2.72203, 101.943, 'https://www.facebook.com/pages/Masjid-Istiqlal/1453059284922136?rf=252383441528933', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(105, 'Kompleks Tenun Pahang DiRaja Sultan Haji Ahmad Shah Sungai Soi ', '09-5562344', '02:00:00', 'Lot 1-47', 'Kompleks Teruntum', '', '25000', 'Kuantan', 6, '', 1, '', 1, 3.73734, 103.317, 'https://www.gayatravel.com.my/9-places-and-attractions-in-kuantan-pekan-pahang/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(106, 'Chini Lake', '', '02:00:00', 'Tasik Chini', '', '', '26650', 'Chini', 6, '', 1, '', 1, 3.4337, 102.919, 'https://thriftytraveller.wordpress.com/2012/03/11/lake-chini/ ', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(107, 'Fraser''s Hill ', '', '02:00:00', 'Bukit Fraser', '', '', '49000', 'Bukit Fraser', 6, '', 1, '', 1, 3.71306, 101.737, 'http://www.fraserhill.info/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(108, 'Bukit Tinggi Berjaya Hills Resort', '09-2888888', '02:00:00', 'Km 48', 'Karak Expy', 'Persimpangan Bertingkat', '28750', 'Bentong ', 6, '', 1, '', 1, 3.39145, 101.833, 'https://www.malaysia-traveller.com/bukit-tinggi.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(109, 'Kuala Gandah Elephant Sanctuary ', '03-90866800', '02:00:00', 'Kuala Gandah', 'Lanchang', 'Pahang', '28500', 'Lanchang', 6, '', 1, '', 1, 3.59227, 102.145, 'https://www.malaysia-traveller.com/kuala-gandah.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(110, 'Tioman Paya Resort', '09-4197073', '02:00:00', 'Kampung Paya', 'Mersing', '', '81000', 'Tioman Island', 6, '', 1, '', 1, 2.79025, 104.17, 'https://www.holidaygogogo.com/pahang/ ', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(111, 'Genting Highlands Premium Outlets', '03-64338888', '02:00:00', 'Km 13', '', '', '69000', 'Genting Highland ', 6, '', 1, '', 1, 3.4031, 101.782, 'http://www.kuala-lumpur.ws/magazine/genting-highlands-premium-outlets.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(112, 'Chamang Waterfall ', '017-3666223', '02:00:00', 'Chamang Waterfall', '', '', '28700', 'Bentong ', 6, '', 1, '', 1, 3.50966, 101.858, 'http://www.gobentong.com/en/attraction/attraction/chamang-waterfall', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(113, 'Teluk Cempedak Beach', '', '02:00:00', 'Pantai Teluk Cempedak', '', '', '25050', 'Kuantan', 6, '', 1, '', 1, 3.81151, 103.372, 'https://www.malaysiavacationguide.com/teluk-chempedak.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(114, 'Genting Highlands Themes Park', '03-2718 1118', '02:00:00', 'Genting Highlands Resort', 'Genting Highlands ', '', '69000', 'Genting Highland ', 6, '', 1, '', 1, 3.42477, 101.795, 'http://www.gentinghighlands.info/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(115, 'Kota Kayang Museum', '04-977 0027', '02:00:00', '', 'Kuala Perlis', '', '02000', 'Kuala Perlis', 9, '', 1, '', 1, 6.41583, 100.149, 'http://www.malaysia-hotels.net/perlis/perlis.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(116, 'Gua Kelam', '012-455 2121', '02:00:00', '33 km of Kangar', 'Kaki Bukit', '', '02200', 'Kaki Bukit', 9, '', 1, '', 1, 6.64444, 100.203, 'http://www.malaysia-hotels.net/perlis/perlis.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(117, 'Masjid Terapung Al-Hussain Mosque', '011-3545 1301', '02:00:00', '1', 'Persiaran Putra Timur', '', '02000', 'Kuala Perlis', 9, '', 1, '', 1, 6.3973, 100.127, 'https://www.expatgo.com/my/2013/11/13/six-must-see-attractions-in-perlis/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(118, 'Rosliza Mini Market', '04-9457633', '02:00:00', 'R15 ', 'Kampung Wang Kelian', '', '02200', 'Kaki Bukit', 9, '', 1, '', 1, 6.67633, 100.187, 'https://www.facebook.com/KGWANGKELIAN', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(119, 'The Perlis Snake and Reptile Farm', '04-976 8511 ', '02:00:00', 'Jalan Sungai Batu Pahat', 'Kangar ', '', '01000', 'Kangar', 9, '', 1, '', 1, 6.47293, 100.204, 'https://www.expatgo.com/my/2013/11/13/six-must-see-attractions-in-perlis/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(120, 'Tasik Melati Recreational Park', '014-908 6267', '02:00:00', 'Taman Melati', 'Kangar', '', '02400', 'Kangar', 9, '', 1, '', 1, 6.5066, 100.243, 'https://www.expatgo.com/my/2013/11/13/six-must-see-attractions-in-perlis/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(121, 'Padang Besar', '', '02:00:00', 'Padang Besar', '', '', '', '', 9, '', 1, '', 1, 0, 0, 'https://www.tripzilla.com/tigerair-kedah-perlis-malaysia-tourism-less-travelled/15832', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(122, 'Bukit Keteri', '04-978 1235', '02:00:00', 'Arau Perlis', '', '', '02450', 'Kangar', 9, '', 1, '', 1, 6.51864, 100.263, 'http://www.malaysia.travel/en/my/places/states-of-malaysia/perlis/bukit-keteri?page=3 ', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(123, 'Bukit Temiang Agriculture Centre', '04-9381376', '02:00:00', 'Jabatan Pertanian Negeri Perlis ', ', KM11', 'Jalan Kaki Bukit', '02400', 'Kangar', 9, '', 1, '', 1, 6.5233, 100.23, 'https://www.tripadvisor.com.my/Attraction_Review-g677302-d2526108-Reviews-Taman_Anggur_Perlis-Kangar_Perlis.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(124, 'Perlis State Park', '04-977 7578 ', '02:00:00', 'KM2', 'Jalan Kaki Bukit', '', '01000', 'Kangar', 9, '', 1, '', 1, 6.628, 100.188, 'http://www.malaysia.travel/en/my/places/states-of-malaysia/perlis/perlis-state-park?page=2', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(125, 'Gua Tempurung', '0 14-220 4142', '02:00:00', 'Jalan Gopeng', 'Pusat Pelancongan Gua Tempurung', '', '31600', 'Gopeng', 8, '', 1, '', 1, 4.41604, 101.188, 'http://www.ipoh-city.com/attraction/Gua_Tempurung/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(126, 'Han Chin Pet Soo', '05-241 4541', '02:00:00', '3', 'Jalan Bijeh Timah', '', '30100', 'Ipoh', 8, '', 1, '', 1, 4.59637, 101.079, 'https://www.ipohworld.org/reservation/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(127, 'Funtasy House Trick Art', '05-255 0007', '02:00:00', '16', 'Jalan Market', '', '30000', 'Ipoh', 8, '', 1, '', 1, 4.59602, 101.077, 'http://funtasyhouse.com/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(128, 'Kellie?s Castle', '05-365 3381', '02:00:00', 'Kellie''s Castle', '', '', '31000', 'Batu Gajah', 8, '', 1, '', 1, 4.47442, 101.088, 'http://www.ipoh-city.com/attraction/Kellies_Castle/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(129, 'Teluk Intan Leaning Tower', '', '02:00:00', 'Lot 1&2, Komplex Menara Condong', 'Jalan Bandar', 'Pekan Teluk Intan', '36000', 'Teluk Intan', 8, '', 1, '', 1, 4.02516, 101.019, 'https://www.malaysiavacationguide.com/leaningtower.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(130, 'Zoo Taiping', '05-808 6577', '02:00:00', 'Jalan Taman Tasik Taiping', 'Taman Tasik Taiping', '', '34000', 'Taiping', 8, '', 1, '', 1, 4.85481, 100.75, 'http://www.zootaiping.gov.my/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(131, 'Lost World of Tambun Theme Park', '05-542 8888', '02:00:00', '1', 'Persiaran Lagun Sunway 1', 'Sunway City Ipoh', '31150', 'Ipoh', 8, '', 1, '', 1, 4.62486, 101.155, 'https://sunwaylostworldoftambun.com/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(132, 'Gunung Lang Recreational Park', '05-208 3333', '02:00:00', 'Jalan Damai', '', '', '30100', 'Ipoh', 8, '', 1, '', 1, 4.62585, 101.089, 'http://www.ipoh-city.com/attraction/Gunung_Lang_Recreational_Park/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(133, 'Movie Animation Park Studios', '', '02:00:00', 'Plot E-BO3', 'Persiaran Meru Raya 3', 'Bandar Meru Raya', '30020', 'Ipoh', 8, '', 1, '', 1, 4.66366, 101.083, 'http://www.mapsperak.com/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(134, 'Perak Museum', '', '02:00:00', 'Jalan Taming Sari', '', '', '34000', 'Taiping', 8, '', 1, '', 1, 4.86077, 100.745, 'http://www.jmm.gov.my/en/museum/perak-museum', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(135, 'Islamic Civilization Park ', '09 627 8888', '02:00:00', 'Pulau Wan Man', '', '', '21000', 'Kuala Terengganu', 11, '', 1, '', 1, 5.32205, 103.116, 'http://terengganu.attractionsinmalaysia.com/Islamic-Civilisation.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(136, 'Mesra Mall', '09-8649000', '02:00:00', 'Lot 6490', 'Jalan Kemaman-Dungun', 'Kemasik', '24200', 'Kemaman', 11, '', 1, '', 1, 4.44046, 103.448, 'http://terengganu.attractionsinmalaysia.com/Shopping.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(137, 'Rantau Abang', '', '02:00:00', 'Kampung Rantau Abang', '', '', '23050', 'Kuala Dungun', 11, '', 1, '', 1, 4.87015, 103.392, 'http://terengganu.attractionsinmalaysia.com/Rantau-Abang.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(138, 'Merang', '', '02:00:00', 'Kuala Terengganu', '', '', '21010', 'Setiu', 11, '', 1, '', 1, 5.52579, 102.954, 'http://terengganu.attractionsinmalaysia.com/Merang.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(139, 'Perhentian Island ', '', '02:00:00', 'Pulau Perhentian Besar', '', '', '', 'Kuala Besut', 11, '', 1, '', 1, 5.89635, 102.739, 'http://terengganu.attractionsinmalaysia.com/Perhentian-Island.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(140, 'Lang Tengah Island', '', '02:00:00', 'Pulau Lang Tengah', '', '', '', 'Kuala Terengganu', 11, '', 1, '', 1, 5.79605, 102.896, 'http://terengganu.attractionsinmalaysia.com/Lang-Tengah-Island.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(141, 'Kenyir Lake ', '', '02:00:00', 'Tasik Kenyir', '', '', '21700', 'Kuala Berang', 11, '', 1, '', 1, 5.00609, 102.639, 'http://terengganu.attractionsinmalaysia.com/Lake-Kenyir.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(142, 'Bukit Keluang', '', '02:00:00', 'Bukit Keluang Beach', '', '', '22200', 'Kampung Raja', 11, '', 1, '', 1, 5.79947, 102.603, 'http://terengganu.attractionsinmalaysia.com/Bukit-Keluang.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(143, 'Tanjung Jara Beach', '', '02:00:00', 'Batu 8', 'Off Jalan Dungun,', '', '23000', 'Dungun', 11, '', 1, '', 1, 4.81184, 103.423, 'http://terengganu.attractionsinmalaysia.com/Tanjung-Jara-Beach.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(144, 'Restaurant Tong Juan (Stuffed Crab)', '09-8591346', '02:00:00', 'K117', 'Jalan Sulaimani', 'Chukai', '', 'Kemaman', 11, '', 1, '', 1, 4.23155, 103.428, 'http://terengganu.attractionsinmalaysia.com/Restaurant.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(145, 'i-City Shah Alam', '03-5521 8866 / ', '02:00:00', 'i-Gallery', 'Persiaran Multimedia', 'i-Citu\\y', '40000', 'Shah Alam', 10, '', 1, '', 1, 3.0629, 101.482, 'https://themepark.i-city.my/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(146, 'White Water Rafting Selangor River - Kuala Kubu Bharu', '017-227 8003', '02:00:00', 'Xventure Mind,', 'No.1032,Kampung Jelutong Tambahan', '', '44100', 'Kalumpang', 10, '', 1, '', 1, 3.56169, 101.631, 'https://www.malaysia-whitewater-rafting.com/contactUs.php', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(147, 'Zoo Negara', '03-4108 3422', '02:00:00', 'Zoo Negara', 'Hulu Kelang', '', '68000', 'Ampang', 10, '', 1, '', 1, 3.2067, 101.757, 'http://www.zoonegaramalaysia.my/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(148, 'Gold Coast Morib International Resort', '03-3198 1028', '02:00:00', 'Gold Coast Morib International Resort', 'PT 294, ', 'Kawasan Kanchong Laut, Morib Beach,', '42700', 'Banting', 10, '', 1, '', 1, 2.71526, 101.46, 'http://gcr.com.my/morib/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(149, 'Skytrex Shah Alam', '01 3-276 9841', '02:00:00', 'Management & Administration Office', 'C33-2-7 Block C Jaya One', 'No. 72A Jalan Universiti', '46200', 'Petaling Jaya', 10, '', 1, '', 1, 3.0954, 101.511, 'https://www.skytrex-adventure.org/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(150, 'Sungai Congkak', '017-309 0190', '02:00:00', 'CONGKAK ECO RESORT (002084285-A)', 'Batu 20 1/2 Kampung Padang,', 'Sg. Congkak, Hulu Langat', '43100', 'Kajang', 10, '', 1, '', 1, 3.20965, 101.843, 'http://www.congkakecoresort.com', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(151, 'Sunway Lagoon', '03-5639 0000', '02:00:00', '3', 'Jalan PJS 11/11, ', 'Bandar Sunway', '47500', 'Bandar Sunway', 10, '', 1, '', 1, 3.07202, 101.604, 'https://sunwaylagoon.com/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(152, 'Batu Caves', '', '02:00:00', 'Gombak', '', '', '68100', 'Batu Caves', 10, '', 1, '', 1, 3.23794, 101.684, 'https://asnitours.com/home', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(153, 'Galeri Sultan Abdul Aziz', '03-3373 6500', '02:00:00', 'Galeri Diraja Sultan Abdul Aziz', 'Bangunan Sultan Suleiman', '', '41000', 'Klang', 10, '', 1, '', 1, 3.04154, 101.449, 'http://www.galeridiraja.com/Page_1.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(154, 'Masjid Sultan Salahuddin Abdul Aziz Shah', '', '02:00:00', 'Persiaran Masjid St', 'Sekysen 14, ', '', '40000', 'Shah Alam', 10, '', 1, '', 1, 3.0787, 101.521, 'https://www.facebook.com/pages/Masjid-Istiqlal/1453059284922136?rf=252383441528933', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(155, 'Escape Adventureland Teluk Bahang', '04-881 1106', '02:00:00', '828 Jalan Teluk Bahang', '', '', '11050', 'Penang', 7, '', 1, '', 1, 5.44938, 100.215, 'http://www.penang.ws/penang-attractions/sim-leisure-excape.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(156, 'Adventure Zone Theme Park Batu Ferringhi', '04-886 1911', '02:00:00', 'Centre Shangri-La Rasa Sayang Resort & Spa and Shangri La Golden Sands Resort', 'Jalan Batu Ferringhi?', 'Kampung Tanjung Huma', '11100', 'Batu Ferringhi', 7, '', 1, '', 1, 5.47766, 100.252, 'https://www.justgola.com/a/adventure-zone-1195266', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(157, 'Made In Penang Interactive Museum, Georgetown', '04-262 6119', '02:00:00', 'No.3', 'Pengkalan Weld', '', '10300', 'George Town', 7, '', 1, '', 1, 5.41645, 100.344, 'https://www.tripadvisor.com.my/Attraction_Review-g298303-d5772856-', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(158, 'Penang War Museum, Bayan Lepas ', '04-626 5142 / 0', '02:00:00', 'Jalan Batu Maung', '', '', '11960', 'Batu Maung', 7, '', 1, '', 1, 5.2815, 100.289, 'http://www.penang.ws/penang-attractions/war-museum.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(159, 'Penang Fort Cornwallis, Georgetown', '02-61 0262', '02:00:00', 'Jalan Tun Syed Sheh Barakbah', '', '', '10200', 'George Town', 7, '', 1, '', 1, 5.42065, 100.344, 'http://www.penang.ws/penang-attractions/fort-cornwallis.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(160, 'Penang Botanic Gardens, Georgetown', '04-226 4401 / 0', '02:00:00', '673A', 'Jalan Kebun Bunga', 'Pulau Tikus', '10350', 'George Town', 7, '', 1, '', 1, 5.43779, 100.291, 'http://www.penang.ws/penang-attractions/penang-botanic-gardens.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(161, 'Penang Hill', '04-828 8880', '02:00:00', 'Jalan Stesen Bukit Bendera', 'Air Itam', '', '11500', 'George Town', 7, '', 1, '', 1, 5.4335, 100.267, 'https://www.tripadvisor.com.my/Attraction_Review-g298303-d455283-Reviews-Penang_Hill-George_Town_Penang_Island_Penang.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(162, 'Kek Lok Si Temple, Georgetown', '04-828 3317', '02:00:00', '1000-L', 'Tingkat Lembah Ria 1', '', '11500', 'Ayer Itam', 7, '', 1, '', 1, 5.3982, 100.273, 'http://kekloksitemple.com/', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(163, 'Penang National Park', '04-8813500 / 04', '02:00:00', 'Pejabat Taman Negara P. Pinang', ' Jalan Hassan Abbas', '', '11050', 'George Town', 7, '', 1, '', 1, 5.46209, 100.19, 'https://www.travel-penang-malaysia.com/penang-national-park.html', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00'),
(164, 'Cheong Fatt Tze Mansion ', '04 262 0006', '02:00:00', 'Ground Floor', '14', 'Leith Street', '10200', 'George Town', 7, '', 1, '', 1, 5.42144, 100.335, 'http://www.penang.ws/penang-attractions/cheong-fatt-tze.htm', NULL, 3, 1, '2019-12-18 13:00:00', 1, '2019-12-18 05:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `place_category`
--

CREATE TABLE `place_category` (
  `id` int(11) NOT NULL,
  `place_id` int(11) DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `place_category`
--

INSERT INTO `place_category` (`id`, `place_id`, `category_id`) VALUES
(2, 3, '1,2,6'),
(3, 4, '2,3'),
(4, 5, '2,3'),
(5, 6, '1,10'),
(6, 7, '1'),
(7, 8, '1,10'),
(8, 9, '1,10'),
(9, 10, '1,6'),
(10, 11, '1,6,10'),
(11, 12, '1,10'),
(12, 13, '1,6'),
(13, 14, '1,6'),
(14, 15, '1,10'),
(15, 16, '9'),
(16, 17, '9'),
(17, 18, '9'),
(18, 19, '2,3,6'),
(19, 20, '9'),
(20, 21, '2,3'),
(21, 22, '1,2,3'),
(22, 23, '3'),
(23, 24, '1,2,6'),
(24, 25, '6'),
(25, 26, '2'),
(26, 27, '6'),
(27, 28, '6'),
(28, 29, '2'),
(29, 30, '6'),
(30, 31, '2'),
(31, 32, '8'),
(32, 36, '2'),
(33, 37, '8'),
(34, 38, '8'),
(35, 39, '2'),
(36, 40, '2'),
(37, 41, '5'),
(38, 42, '5'),
(39, 43, '2'),
(40, 44, '5'),
(41, 45, '2'),
(42, 46, '5'),
(43, 47, '1,2,6'),
(44, 48, '2'),
(45, 49, '2'),
(46, 50, '2,6'),
(47, 51, '2,9'),
(48, 52, '2,9'),
(49, 53, '2,9'),
(51, 55, '6,7'),
(52, 56, '6,9'),
(53, 57, '1,6,7'),
(54, 58, '6,7'),
(55, 59, '6,7'),
(56, 60, '6,7'),
(57, 61, '1,6,7'),
(58, 62, '1,6'),
(59, 63, '6,7'),
(60, 64, '6,7'),
(61, 65, '2,6,7'),
(62, 66, '2'),
(63, 67, '5, 2'),
(64, 68, '2,6'),
(65, 69, '9'),
(66, 70, '1'),
(67, 71, '10'),
(68, 72, '5, 2'),
(69, 73, '2'),
(70, 74, '9'),
(71, 75, '6'),
(72, 76, '6'),
(73, 77, '3'),
(74, 78, '6'),
(75, 79, '2'),
(76, 80, '3'),
(77, 81, '9'),
(78, 82, '2'),
(79, 83, '2'),
(80, 84, '6'),
(81, 85, '8'),
(82, 86, '8'),
(83, 87, '10'),
(84, 88, '6'),
(85, 89, '5, 2'),
(86, 90, '2'),
(87, 91, '3'),
(88, 92, '6'),
(89, 93, '6'),
(90, 94, '2'),
(91, 95, '6'),
(92, 96, '9'),
(93, 97, '3'),
(94, 98, '3'),
(95, 99, '2'),
(96, 100, '3'),
(97, 101, '6'),
(98, 102, '2'),
(99, 103, '2'),
(100, 104, '10'),
(101, 105, '6'),
(102, 106, '2'),
(103, 107, '2'),
(104, 108, '2'),
(105, 109, '2'),
(106, 110, '2'),
(107, 111, '8'),
(108, 112, '2'),
(109, 113, '2'),
(110, 114, '2, 8'),
(111, 115, '6'),
(112, 116, '2'),
(113, 117, '11'),
(114, 118, '8'),
(115, 119, '6'),
(116, 120, '11'),
(117, 121, '11, 8, 4'),
(118, 122, '2'),
(119, 123, '11'),
(120, 124, '11'),
(121, 125, '9'),
(122, 126, '1'),
(123, 127, '3'),
(124, 128, '6'),
(125, 129, '6'),
(126, 130, '9'),
(127, 131, '2'),
(128, 132, '2'),
(129, 133, '3'),
(130, 134, '1'),
(131, 135, '1'),
(132, 136, '8'),
(133, 137, '3, 5'),
(134, 138, '3, 5'),
(135, 139, '3'),
(136, 140, '3, 5'),
(137, 141, '2'),
(138, 142, '2'),
(139, 143, '3'),
(140, 144, '4'),
(141, 145, '12'),
(142, 146, '2'),
(143, 147, '3'),
(144, 148, '3'),
(145, 149, '2'),
(146, 150, '13'),
(147, 151, '12'),
(148, 152, '14'),
(149, 153, '7'),
(150, 154, '14'),
(151, 155, '2'),
(152, 156, '2'),
(153, 157, '6'),
(154, 158, '6'),
(155, 159, '6'),
(156, 160, '2'),
(157, 161, '2'),
(158, 162, '2,9'),
(159, 163, '2'),
(160, 164, '2');

-- --------------------------------------------------------

--
-- Table structure for table `place_image`
--

CREATE TABLE `place_image` (
  `id` int(11) NOT NULL,
  `place_id` int(11) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `file_size` varchar(100) DEFAULT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `detail` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `place_image`
--

INSERT INTO `place_image` (`id`, `place_id`, `file_name`, `file_size`, `file_type`, `file_path`, `detail`) VALUES
(7, NULL, NULL, NULL, NULL, NULL, '-'),
(8, 3, '15355502725b86a340d931c9.97047461.jpg', '59751', 'image/jpeg', '/upload/place_image/3/15355502725b86a340d931c9.97047461.jpg', 'the center street of Chinatown '),
(9, 4, '15355510215b86a62de98511.15639029.jpg', '23854', 'image/jpeg', '/upload/place_image/4/15355510215b86a62de98511.15639029.jpg', '-'),
(10, 4, '15355510215b86a62dea28f8.68557002.jpg', '56457', 'image/jpeg', '/upload/place_image/4/15355510215b86a62dea28f8.68557002.jpg', '-'),
(11, 6, '15355520965b86aa602aa2b7.26781670.jpg', '112670', 'image/jpeg', '/upload/place_image/6/15355520965b86aa602aa2b7.26781670.jpg', '-'),
(12, 7, '15355526755b86aca34cff36.95839365.jpg', '717503', 'image/jpeg', '/upload/place_image/7/15355526755b86aca34cff36.95839365.jpg', '-'),
(13, NULL, NULL, NULL, NULL, NULL, '-'),
(14, 9, '15355530485b86ae18a6a172.52539868.jpg', '216646', 'image/jpeg', '/upload/place_image/9/15355530485b86ae18a6a172.52539868.jpg', 'Its harmonious blend of styles and design elements from different cultures that results in an aesthetically-pleasing whole very much reflects the multicultural state'),
(15, 10, '15355531885b86aea4256fe7.80162951.jpg', '301395', 'image/jpeg', '/upload/place_image/10/15355531885b86aea4256fe7.80162951.jpg', '-'),
(16, 11, '15355532575b86aee9ec6455.86199668.jpg', '97830', 'image/jpeg', '/upload/place_image/11/15355532575b86aee9ec6455.86199668.jpg', '-'),
(17, 12, '15355534665b86afba4067d9.02147492.jpg', '122054', 'image/jpeg', '/upload/place_image/12/15355534665b86afba4067d9.02147492.jpg', '-'),
(18, 13, '15355534665b86afba8d8841.83849303.jpg', '130517', 'image/jpeg', '/upload/place_image/13/15355534665b86afba8d8841.83849303.jpg', '-'),
(19, 14, '15355536075b86b04730a839.42335488.jpg', '54428', 'image/jpeg', '/upload/place_image/14/15355536075b86b04730a839.42335488.jpg', '-'),
(20, 15, '15355537225b86b0ba836337.40908618.jpg', '62899', 'image/jpeg', '/upload/place_image/15/15355537225b86b0ba836337.40908618.jpg', '-'),
(21, 16, '15355538585b86b14259e302.53875051.jpg', '11015', 'image/jpeg', '/upload/place_image/16/15355538585b86b14259e302.53875051.jpg', '-'),
(22, 17, '15355540225b86b1e6741535.17753362.jpg', '76275', 'image/jpeg', '/upload/place_image/17/15355540225b86b1e6741535.17753362.jpg', '-'),
(23, 18, '15355542615b86b2d5c75190.01983698.jpg', '15747', 'image/jpeg', '/upload/place_image/18/15355542615b86b2d5c75190.01983698.jpg', '-'),
(24, 19, '15355542755b86b2e32b29a0.33929914.jpg', '40624', 'image/jpeg', '/upload/place_image/19/15355542755b86b2e32b29a0.33929914.jpg', '-'),
(25, 20, '15355544345b86b382c49d81.00704281.jpg', '85757', 'image/jpeg', '/upload/place_image/20/15355544345b86b382c49d81.00704281.jpg', '-'),
(26, 25, '15355919165b8745ecb2cba0.27553013.png', '578948', 'image/png', '/upload/place_image/25/15355919165b8745ecb2cba0.27553013.png', '-'),
(27, 5, '15355920395b874667758179.13364399.jpg', '39284', 'image/jpeg', '/upload/place_image/5/15355920395b874667758179.13364399.jpg', '-'),
(28, 26, '15355921315b8746c32e14d9.95682910.jpg', '48815', 'image/jpeg', '/upload/place_image/26/15355921315b8746c32e14d9.95682910.jpg', '-'),
(29, 26, '15355921315b8746c32eb507.07217478.jpg', '21445', 'image/jpeg', '/upload/place_image/26/15355921315b8746c32eb507.07217478.jpg', '-'),
(30, 27, '15355922015b874709c697f8.97831201.png', '434836', 'image/png', '/upload/place_image/27/15355922015b874709c697f8.97831201.png', '-'),
(31, 28, '15355925395b87485bd35401.39486878.png', '512230', 'image/png', '/upload/place_image/28/15355925395b87485bd35401.39486878.png', '-'),
(32, 28, '15355925395b87485bd40821.02357939.png', '610543', 'image/png', '/upload/place_image/28/15355925395b87485bd40821.02357939.png', '-'),
(34, NULL, NULL, NULL, NULL, NULL, '-'),
(35, 29, '15355926635b8748d74ac178.15425191.jpg', '102457', 'image/jpeg', '/upload/place_image/29/15355926635b8748d74ac178.15425191.jpg', '-'),
(36, 29, '15355926635b8748d74b6351.17551445.jpg', '207324', 'image/jpeg', '/upload/place_image/29/15355926635b8748d74b6351.17551445.jpg', '-'),
(37, 30, '15355928855b8749b5c13448.48231711.png', '455493', 'image/png', '/upload/place_image/30/15355928855b8749b5c13448.48231711.png', '-'),
(38, 30, '15355928855b8749b5c1d576.29884837.png', '562255', 'image/png', '/upload/place_image/30/15355928855b8749b5c1d576.29884837.png', '-'),
(39, 31, '15355930835b874a7b3e0219.06668462.jpg', '57471', 'image/jpeg', '/upload/place_image/31/15355930835b874a7b3e0219.06668462.jpg', '-'),
(40, 31, '15355930835b874a7b3ea785.50610366.jpg', '45824', 'image/jpeg', '/upload/place_image/31/15355930835b874a7b3ea785.50610366.jpg', '-'),
(41, 32, '15355933115b874b5f6bb4a9.06614765.png', '251384', 'image/png', '/upload/place_image/32/15355933115b874b5f6bb4a9.06614765.png', '-'),
(42, 32, '15355933115b874b5f6c5686.74587582.png', '417308', 'image/png', '/upload/place_image/32/15355933115b874b5f6c5686.74587582.png', '-'),
(48, 36, '15355934175b874bc9efa8d9.40309986.jpg', '38192', 'image/jpeg', '/upload/place_image/36/15355934175b874bc9efa8d9.40309986.jpg', '-'),
(49, 37, '15355934765b874c04e74530.74464185.png', '222016', 'image/png', '/upload/place_image/37/15355934765b874c04e74530.74464185.png', '-'),
(50, 38, '15355936425b874caac5e9a8.29064012.png', '336382', 'image/png', '/upload/place_image/38/15355936425b874caac5e9a8.29064012.png', '-'),
(51, 38, '15355936425b874caac7f773.12584785.png', '241218', 'image/png', '/upload/place_image/38/15355936425b874caac7f773.12584785.png', '-'),
(52, 39, '15355938405b874d703569d8.75947883.jpg', '569447', 'image/jpeg', '/upload/place_image/39/15355938405b874d703569d8.75947883.jpg', '-'),
(53, 24, '15355941125b874e80d868f2.30190819.jpg', '38099', 'image/jpeg', '/upload/place_image/24/15355941125b874e80d868f2.30190819.jpg', '-'),
(54, 40, '15355942055b874edd8fc874.59130753.jpg', '58622', 'image/jpeg', '/upload/place_image/40/15355942055b874edd8fc874.59130753.jpg', '-'),
(55, 40, '15355942055b874edd907535.46748274.jpg', '245003', 'image/jpeg', '/upload/place_image/40/15355942055b874edd907535.46748274.jpg', '-'),
(56, 41, '15355942955b874f375ba971.48972347.png', '98497', 'image/png', '/upload/place_image/41/15355942955b874f375ba971.48972347.png', '-'),
(57, 41, '15355942955b874f375ca938.31543766.png', '66493', 'image/png', '/upload/place_image/41/15355942955b874f375ca938.31543766.png', '-'),
(58, 41, '15355942955b874f375d7b69.23620470.png', '115790', 'image/png', '/upload/place_image/41/15355942955b874f375d7b69.23620470.png', '-'),
(59, 42, '15355944445b874fcc68d5b0.46470841.png', '249657', 'image/png', '/upload/place_image/42/15355944445b874fcc68d5b0.46470841.png', '-'),
(60, 42, '15355944445b874fcc6a9f52.39006055.png', '154070', 'image/png', '/upload/place_image/42/15355944445b874fcc6a9f52.39006055.png', '-'),
(61, 43, '15355945475b875033400624.89358337.jpg', '51564', 'image/jpeg', '/upload/place_image/43/15355945475b875033400624.89358337.jpg', '-'),
(62, 43, '15355945475b87503340a8c3.34369344.jpg', '90340', 'image/jpeg', '/upload/place_image/43/15355945475b87503340a8c3.34369344.jpg', '-'),
(63, 44, '15355945955b8750638dda47.89042749.png', '104623', 'image/png', '/upload/place_image/44/15355945955b8750638dda47.89042749.png', '-'),
(64, 44, '15355945955b8750638eb1b4.08457149.png', '280934', 'image/png', '/upload/place_image/44/15355945955b8750638eb1b4.08457149.png', '-'),
(65, 45, '15355948345b8751522b40b9.10653053.jpg', '336734', 'image/jpeg', '/upload/place_image/45/15355948345b8751522b40b9.10653053.jpg', '-'),
(66, 45, '15355948345b8751522beab8.14375339.jpg', '63084', 'image/jpeg', '/upload/place_image/45/15355948345b8751522beab8.14375339.jpg', '-'),
(67, 46, '15355948835b8751838a9af2.93250067.png', '319204', 'image/png', '/upload/place_image/46/15355948835b8751838a9af2.93250067.png', '-'),
(68, 46, '15355948835b8751838b7913.02062851.png', '173296', 'image/png', '/upload/place_image/46/15355948835b8751838b7913.02062851.png', '-'),
(69, 47, '15355950115b8752038549f8.40271981.jpg', '83946', 'image/jpeg', '/upload/place_image/47/15355950115b8752038549f8.40271981.jpg', '-'),
(70, 48, '15355950885b875250a1cdc6.59716585.jpg', '116849', 'image/jpeg', '/upload/place_image/48/15355950885b875250a1cdc6.59716585.jpg', '-'),
(71, 48, '15355950885b875250a2bb64.04023298.jpg', '28711', 'image/jpeg', '/upload/place_image/48/15355950885b875250a2bb64.04023298.jpg', '-'),
(72, 49, '15355953625b8753625df4f1.55768337.jpg', '163493', 'image/jpeg', '/upload/place_image/49/15355953625b8753625df4f1.55768337.jpg', '-'),
(73, 49, '15355953625b8753625ecc37.56152495.jpg', '392364', 'image/jpeg', '/upload/place_image/49/15355953625b8753625ecc37.56152495.jpg', '-'),
(74, 8, '15355955635b87542b6e43f0.32754100.jpg', '55019', 'image/jpeg', '/upload/place_image/8/15355955635b87542b6e43f0.32754100.jpg', '-'),
(75, 50, '15355965615b8758110e0644.26371736.jpg', '19747', 'image/jpeg', '/upload/place_image/50/15355965615b8758110e0644.26371736.jpg', 'Solar System Zone'),
(76, 51, '15356013375b876ab9edff19.39838897.jpg', '133668', 'image/jpeg', '/upload/place_image/51/15356013375b876ab9edff19.39838897.jpg', '-'),
(77, 52, '15356021205b876dc82dd5b5.77800293.jpg', '78134', 'image/jpeg', '/upload/place_image/52/15356021205b876dc82dd5b5.77800293.jpg', 'Fruit Fram'),
(79, 55, '15356038215b87746dc4b036.82028046.jpg', '49805', 'image/jpeg', '/upload/place_image/55/15356038215b87746dc4b036.82028046.jpg', '-'),
(80, 56, '15356046625b8777b6b24996.34717963.jpg', '63133', 'image/jpeg', '/upload/place_image/56/15356046625b8777b6b24996.34717963.jpg', '-'),
(81, 57, '15356051685b8779b03f5991.73347096.jpg', '69256', 'image/jpeg', '/upload/place_image/57/15356051685b8779b03f5991.73347096.jpg', '-'),
(82, 58, '15356056525b877b94d9f537.73889412.jpg', '45201', 'image/jpeg', '/upload/place_image/58/15356056525b877b94d9f537.73889412.jpg', '-'),
(83, 59, '15356101105b878cfe1ed143.70695833.jpg', '40258', 'image/jpeg', '/upload/place_image/59/15356101105b878cfe1ed143.70695833.jpg', '-'),
(84, 60, '15356108805b879000742495.46248566.jpg', '50637', 'image/jpeg', '/upload/place_image/60/15356108805b879000742495.46248566.jpg', '-'),
(85, 61, '15356122385b87954e481639.50545081.jpg', '40117', 'image/jpeg', '/upload/place_image/61/15356122385b87954e481639.50545081.jpg', '-'),
(86, 62, '15356125085b87965c4508c8.09649531.jpg', '44234', 'image/jpeg', '/upload/place_image/62/15356125085b87965c4508c8.09649531.jpg', '-'),
(87, 63, '15356133595b8799af180a27.83127845.jpg', '43130', 'image/jpeg', '/upload/place_image/63/15356133595b8799af180a27.83127845.jpg', '-'),
(88, 64, '15356138845b879bbc6f9dd0.39840919.jpg', '42536', 'image/jpeg', '/upload/place_image/64/15356138845b879bbc6f9dd0.39840919.jpg', '-'),
(89, 65, '15356144735b879e09d26074.32599185.jpg', '51769', 'image/jpeg', '/upload/place_image/65/15356144735b879e09d26074.32599185.jpg', '-'),
(90, NULL, NULL, NULL, NULL, NULL, '-'),
(91, NULL, NULL, NULL, NULL, NULL, '-'),
(92, NULL, NULL, NULL, NULL, NULL, '-'),
(93, NULL, NULL, NULL, NULL, NULL, '-'),
(94, NULL, NULL, NULL, NULL, NULL, '-'),
(95, NULL, NULL, NULL, NULL, NULL, '-'),
(96, NULL, NULL, NULL, NULL, NULL, '-'),
(97, NULL, NULL, NULL, NULL, NULL, '-'),
(99, 21, '15378589075ba9dd5bf013f0.72478472.jpg', '53786', 'image/jpeg', '/upload/place_image/21/15378589075ba9dd5bf013f0.72478472.jpg', '-'),
(100, 22, '15378589355ba9dd775f9fe4.99574718.png', '222531', 'image/png', '/upload/place_image/22/15378589355ba9dd775f9fe4.99574718.png', '-'),
(101, 22, '15378589355ba9dd77606620.60973835.png', '251336', 'image/png', '/upload/place_image/22/15378589355ba9dd77606620.60973835.png', '-'),
(102, 23, '15378597595ba9e0af1cd181.48784712.jpg', '28998', 'image/jpeg', '/upload/place_image/23/15378597595ba9e0af1cd181.48784712.jpg', '-'),
(103, 23, '15378597595ba9e0af1d8ae0.26938949.jpg', '140340', 'image/jpeg', '/upload/place_image/23/15378597595ba9e0af1d8ae0.26938949.jpg', '-'),
(104, 53, '15378608975ba9e52122a2b2.06363826.png', '281558', 'image/png', '/upload/place_image/53/15378608975ba9e52122a2b2.06363826.png', '-'),
(105, 53, '15378608975ba9e521237c53.42196104.png', '237327', 'image/png', '/upload/place_image/53/15378608975ba9e521237c53.42196104.png', '-'),
(106, 53, '15378608975ba9e521241be7.82896039.png', '300785', 'image/png', '/upload/place_image/53/15378608975ba9e521241be7.82896039.png', '-'),
(107, NULL, NULL, NULL, NULL, NULL, '-'),
(108, NULL, NULL, NULL, NULL, NULL, '-'),
(109, NULL, NULL, NULL, NULL, NULL, '-'),
(110, NULL, NULL, NULL, NULL, NULL, '-'),
(111, NULL, NULL, NULL, NULL, NULL, '-'),
(112, NULL, NULL, NULL, NULL, NULL, '-'),
(113, NULL, NULL, NULL, NULL, NULL, '-'),
(114, NULL, NULL, NULL, NULL, NULL, '-'),
(115, NULL, NULL, NULL, NULL, NULL, '-'),
(116, NULL, NULL, NULL, NULL, NULL, '-');

-- --------------------------------------------------------

--
-- Table structure for table `place_islamic_element`
--

CREATE TABLE `place_islamic_element` (
  `id` int(11) NOT NULL,
  `place_id` int(11) DEFAULT NULL,
  `islamic_element_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `place_islamic_element`
--

INSERT INTO `place_islamic_element` (`id`, `place_id`, `islamic_element_id`) VALUES
(1, 3, '1,2'),
(2, 57, '1,2');

-- --------------------------------------------------------

--
-- Table structure for table `place_operation_hour`
--

CREATE TABLE `place_operation_hour` (
  `id` int(11) NOT NULL,
  `place_id` int(11) DEFAULT NULL,
  `day_id` int(11) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `place_operation_hour`
--

INSERT INTO `place_operation_hour` (`id`, `place_id`, `day_id`, `start_time`, `end_time`) VALUES
(85, 7, 1, '07:30:00', '07:30:00'),
(86, 7, 2, '07:30:00', '07:30:00'),
(87, 7, 3, '07:30:00', '07:30:00'),
(88, 7, 4, '07:30:00', '07:30:00'),
(89, 7, 5, '07:30:00', '07:30:00'),
(90, 7, 6, '07:30:00', '07:30:00'),
(91, 7, 7, '07:30:00', '07:30:00'),
(421, 24, 1, '10:00:00', '23:00:00'),
(422, 24, 2, '10:00:00', '23:00:00'),
(423, 24, 3, '10:00:00', '23:00:00'),
(424, 24, 4, '10:00:00', '23:00:00'),
(425, 24, 5, '10:00:00', '23:00:00'),
(426, 24, 6, '10:00:00', '23:00:00'),
(427, 24, 7, '10:00:00', '23:00:00'),
(477, 47, 1, '08:00:00', '22:00:00'),
(478, 47, 2, '08:00:00', '22:00:00'),
(479, 47, 3, '08:00:00', '22:00:00'),
(480, 47, 4, '08:00:00', '22:00:00'),
(481, 47, 5, '08:00:00', '22:00:00'),
(482, 47, 6, '08:00:00', '22:00:00'),
(483, 47, 7, '08:00:00', '22:00:00'),
(610, 50, 1, '09:00:00', '17:00:00'),
(611, 50, 2, '09:00:00', '17:00:00'),
(612, 50, 3, '09:00:00', '17:00:00'),
(613, 50, 4, '09:00:00', '17:00:00'),
(614, 50, 5, '09:00:00', '17:00:00'),
(615, 50, 6, '09:00:00', '17:00:00'),
(616, 50, 7, '09:00:00', '17:00:00'),
(631, 51, 1, '09:00:00', '18:00:00'),
(632, 51, 2, '09:00:00', '18:00:00'),
(633, 51, 3, '09:00:00', '18:00:00'),
(634, 51, 4, '09:00:00', '18:00:00'),
(635, 51, 5, '09:00:00', '18:00:00'),
(636, 51, 6, '09:00:00', '18:00:00'),
(637, 51, 7, '09:00:00', '18:00:00'),
(645, 52, 1, '09:00:00', '17:00:00'),
(646, 52, 2, '09:00:00', '17:00:00'),
(647, 52, 3, '09:00:00', '17:00:00'),
(648, 52, 4, '09:00:00', '17:00:00'),
(649, 52, 5, '09:00:00', '17:00:00'),
(650, 52, 6, '09:00:00', '17:00:00'),
(651, 52, 7, '09:00:00', '17:00:00'),
(666, 55, 1, '09:00:00', '17:00:00'),
(667, 55, 2, '09:00:00', '17:00:00'),
(668, 55, 3, '09:00:00', '17:00:00'),
(669, 55, 4, '09:00:00', '17:00:00'),
(670, 55, 5, '09:00:00', '17:00:00'),
(671, 55, 6, '09:00:00', '17:00:00'),
(672, 55, 7, '09:00:00', '17:00:00'),
(680, 56, 1, '09:00:00', '17:30:00'),
(681, 56, 2, '09:00:00', '17:30:00'),
(682, 56, 3, '09:00:00', '17:30:00'),
(683, 56, 4, '09:00:00', '21:00:00'),
(684, 56, 5, '09:00:00', '21:00:00'),
(685, 56, 6, '09:00:00', '21:00:00'),
(686, 56, 7, '09:00:00', '21:00:00'),
(694, 58, 1, '09:00:00', '17:00:00'),
(695, 58, 2, '09:00:00', '17:00:00'),
(696, 58, 3, '09:00:00', '17:00:00'),
(697, 58, 4, '09:00:00', '17:00:00'),
(698, 58, 5, '09:00:00', '17:00:00'),
(699, 58, 6, '09:00:00', '17:00:00'),
(700, 58, 7, '09:00:00', '17:00:00'),
(701, 59, 1, '09:00:00', '17:30:00'),
(702, 59, 2, '09:00:00', '17:30:00'),
(703, 59, 3, '09:00:00', '17:30:00'),
(704, 59, 4, '09:00:00', '17:30:00'),
(705, 59, 5, '09:00:00', '17:30:00'),
(706, 59, 6, '09:00:00', '17:30:00'),
(707, 59, 7, '09:00:00', '17:30:00'),
(708, 60, 1, '07:30:00', '07:30:00'),
(709, 60, 2, '07:30:00', '07:30:00'),
(710, 60, 3, '07:30:00', '07:30:00'),
(711, 60, 4, '07:30:00', '07:30:00'),
(712, 60, 5, '07:30:00', '07:30:00'),
(713, 60, 6, '07:30:00', '07:30:00'),
(714, 60, 7, '07:30:00', '07:30:00'),
(715, 61, 1, '09:00:00', '17:30:00'),
(716, 61, 2, '09:00:00', '17:30:00'),
(717, 61, 3, '09:00:00', '17:30:00'),
(718, 61, 4, '09:00:00', '17:30:00'),
(719, 61, 5, '14:45:00', '17:30:00'),
(720, 61, 6, '09:00:00', '17:30:00'),
(721, 61, 7, '09:00:00', '17:30:00'),
(729, 62, 1, '09:00:00', '17:30:00'),
(730, 62, 2, '09:00:00', '17:30:00'),
(731, 62, 3, '09:00:00', '17:30:00'),
(732, 62, 4, '09:00:00', '17:30:00'),
(733, 62, 5, '09:00:00', '17:30:00'),
(734, 62, 6, '09:00:00', '17:30:00'),
(735, 62, 7, '09:00:00', '17:30:00'),
(743, 63, 1, '09:00:00', '17:30:00'),
(744, 63, 2, '09:00:00', '17:30:00'),
(745, 63, 3, '09:00:00', '17:30:00'),
(746, 63, 4, '09:00:00', '17:30:00'),
(747, 63, 5, '09:00:00', '17:30:00'),
(748, 63, 6, '09:00:00', '17:30:00'),
(749, 63, 7, '09:00:00', '17:30:00'),
(750, 64, 1, '10:00:00', '18:00:00'),
(751, 64, 2, '10:00:00', '18:00:00'),
(752, 64, 3, '10:00:00', '18:00:00'),
(753, 64, 4, '10:00:00', '18:00:00'),
(754, 64, 5, '10:00:00', '18:00:00'),
(755, 64, 6, '10:00:00', '18:00:00'),
(756, 64, 7, '10:00:00', '18:00:00'),
(757, 65, 1, '09:00:00', '17:30:00'),
(758, 65, 2, '09:00:00', '17:30:00'),
(759, 65, 3, '09:00:00', '17:30:00'),
(760, 65, 4, '09:00:00', '17:30:00'),
(761, 65, 5, '09:00:00', '17:30:00'),
(762, 65, 6, '09:00:00', '17:30:00'),
(763, 65, 7, '09:00:00', '17:30:00'),
(869, 4, 1, '10:00:00', '22:00:00'),
(870, 4, 2, '10:00:00', '22:00:00'),
(871, 4, 3, '10:00:00', '22:00:00'),
(872, 4, 4, '10:00:00', '22:00:00'),
(873, 4, 5, '10:00:00', '22:00:00'),
(874, 4, 6, '10:00:00', '22:00:00'),
(875, 4, 7, '10:00:00', '22:00:00'),
(876, 5, 1, '10:00:00', '19:00:00'),
(877, 5, 2, '10:00:00', '19:00:00'),
(878, 5, 3, '10:00:00', '19:00:00'),
(879, 5, 4, '10:00:00', '19:00:00'),
(880, 5, 5, '10:00:00', '21:00:00'),
(881, 5, 6, '10:00:00', '21:00:00'),
(882, 5, 7, '10:00:00', '21:00:00'),
(1058, 6, 1, '07:30:00', '07:30:00'),
(1059, 6, 2, '07:30:00', '07:30:00'),
(1060, 6, 3, '07:30:00', '07:30:00'),
(1061, 6, 4, '07:30:00', '07:30:00'),
(1062, 6, 5, '07:30:00', '07:30:00'),
(1063, 6, 6, '07:30:00', '07:30:00'),
(1064, 6, 7, '07:30:00', '07:30:00'),
(1079, 10, 1, '11:00:00', '21:00:00'),
(1080, 10, 2, '11:00:00', '21:00:00'),
(1081, 10, 3, '11:00:00', '21:00:00'),
(1082, 10, 4, '11:00:00', '21:00:00'),
(1083, 10, 5, '11:00:00', '21:00:00'),
(1084, 10, 6, '11:00:00', '21:00:00'),
(1085, 10, 7, '11:00:00', '21:00:00'),
(1086, 11, 1, '07:30:00', '07:30:00'),
(1087, 11, 2, '07:30:00', '07:30:00'),
(1088, 11, 3, '07:30:00', '07:30:00'),
(1089, 11, 4, '07:30:00', '07:30:00'),
(1090, 11, 5, '07:30:00', '07:30:00'),
(1091, 11, 6, '07:30:00', '07:30:00'),
(1092, 11, 7, '07:30:00', '07:30:00'),
(1093, 12, 1, '07:30:00', '07:30:00'),
(1094, 12, 2, '07:30:00', '07:30:00'),
(1095, 12, 3, '07:30:00', '07:30:00'),
(1096, 12, 4, '07:30:00', '07:30:00'),
(1097, 12, 5, '07:30:00', '07:30:00'),
(1098, 12, 6, '07:30:00', '07:30:00'),
(1099, 12, 7, '07:30:00', '07:30:00'),
(1100, 13, 1, '07:30:00', '07:30:00'),
(1101, 13, 2, '07:30:00', '07:30:00'),
(1102, 13, 3, '07:30:00', '07:30:00'),
(1103, 13, 4, '07:30:00', '07:30:00'),
(1104, 13, 5, '07:30:00', '07:30:00'),
(1105, 13, 6, '07:30:00', '07:30:00'),
(1106, 13, 7, '07:30:00', '07:30:00'),
(1107, 14, 1, '09:00:00', '18:00:00'),
(1108, 14, 2, '09:00:00', '18:00:00'),
(1109, 14, 3, '09:00:00', '18:00:00'),
(1110, 14, 4, '09:00:00', '18:00:00'),
(1111, 14, 5, '09:00:00', '18:00:00'),
(1112, 14, 6, '09:00:00', '18:00:00'),
(1113, 14, 7, '09:00:00', '18:00:00'),
(1114, 15, 1, '07:30:00', '07:30:00'),
(1115, 15, 2, '07:30:00', '07:30:00'),
(1116, 15, 3, '07:30:00', '07:30:00'),
(1117, 15, 4, '07:30:00', '07:30:00'),
(1118, 15, 5, '07:30:00', '07:30:00'),
(1119, 15, 6, '07:30:00', '07:30:00'),
(1120, 15, 7, '07:30:00', '07:30:00'),
(1121, 16, 1, '08:30:00', '17:30:00'),
(1122, 16, 2, '08:30:00', '17:30:00'),
(1123, 16, 3, '08:30:00', '17:30:00'),
(1124, 16, 4, '08:30:00', '17:30:00'),
(1125, 16, 5, '08:30:00', '17:30:00'),
(1126, 16, 6, '08:30:00', '17:30:00'),
(1127, 16, 7, '08:30:00', '17:30:00'),
(1128, 17, 1, '09:00:00', '18:00:00'),
(1129, 17, 2, '09:00:00', '18:00:00'),
(1130, 17, 3, '09:00:00', '21:00:00'),
(1131, 17, 4, '09:00:00', '18:00:00'),
(1132, 17, 5, '09:00:00', '18:00:00'),
(1133, 17, 6, '09:00:00', '18:00:00'),
(1134, 17, 7, '09:00:00', '18:00:00'),
(1135, 18, 1, '09:00:00', '18:00:00'),
(1136, 18, 2, '09:00:00', '18:00:00'),
(1137, 18, 3, '09:00:00', '18:00:00'),
(1138, 18, 4, '09:00:00', '18:00:00'),
(1139, 18, 5, '09:00:00', '18:00:00'),
(1140, 18, 6, '09:00:00', '18:00:00'),
(1141, 18, 7, '09:00:00', '18:00:00'),
(1142, 19, 1, '07:30:00', '07:30:00'),
(1143, 19, 2, '07:30:00', '07:30:00'),
(1144, 19, 3, '07:30:00', '07:30:00'),
(1145, 19, 4, '07:30:00', '07:30:00'),
(1146, 19, 5, '07:30:00', '07:30:00'),
(1147, 19, 6, '07:30:00', '07:30:00'),
(1148, 19, 7, '07:30:00', '07:30:00'),
(1149, 20, 1, '09:00:00', '19:00:00'),
(1150, 20, 2, '09:00:00', '19:00:00'),
(1151, 20, 3, '09:00:00', '19:00:00'),
(1152, 20, 4, '09:00:00', '19:00:00'),
(1153, 20, 5, '09:00:00', '19:00:00'),
(1154, 20, 6, '09:00:00', '19:00:00'),
(1155, 20, 7, '09:00:00', '19:00:00'),
(1170, 28, 1, '07:30:00', '07:30:00'),
(1171, 28, 2, '07:30:00', '07:30:00'),
(1172, 28, 3, '07:30:00', '07:30:00'),
(1173, 28, 4, '07:30:00', '07:30:00'),
(1174, 28, 5, '07:30:00', '07:30:00'),
(1175, 28, 6, '07:30:00', '07:30:00'),
(1176, 28, 7, '07:30:00', '07:30:00'),
(1177, 29, 1, '09:30:00', '17:30:00'),
(1178, 29, 2, '09:30:00', '17:30:00'),
(1179, 29, 3, '09:30:00', '17:30:00'),
(1180, 29, 4, '09:30:00', '05:30:00'),
(1181, 29, 5, '09:30:00', '17:30:00'),
(1182, 29, 6, '09:30:00', '17:30:00'),
(1183, 29, 7, '09:30:00', '17:30:00'),
(1184, 30, 1, '00:00:00', '00:00:00'),
(1185, 30, 2, '09:00:00', '18:00:00'),
(1186, 30, 3, '09:00:00', '18:00:00'),
(1187, 30, 4, '09:00:00', '18:00:00'),
(1188, 30, 5, '09:00:00', '18:00:00'),
(1189, 30, 6, '09:00:00', '18:00:00'),
(1190, 30, 7, '09:00:00', '18:00:00'),
(1198, 21, 1, '07:30:00', '07:30:00'),
(1199, 21, 2, '07:30:00', '07:30:00'),
(1200, 21, 3, '07:30:00', '07:30:00'),
(1201, 21, 4, '07:30:00', '07:30:00'),
(1202, 21, 5, '07:30:00', '07:30:00'),
(1203, 21, 6, '07:30:00', '07:30:00'),
(1204, 21, 7, '07:30:00', '07:30:00'),
(1205, 22, 1, '07:30:00', '07:30:00'),
(1206, 22, 2, '07:30:00', '07:30:00'),
(1207, 22, 3, '07:30:00', '07:30:00'),
(1208, 22, 4, '07:30:00', '07:30:00'),
(1209, 22, 5, '07:30:00', '07:30:00'),
(1210, 22, 6, '07:30:00', '07:30:00'),
(1211, 22, 7, '07:30:00', '07:30:00'),
(1212, 31, 1, '17:00:00', '23:00:00'),
(1213, 31, 2, '17:00:00', '23:00:00'),
(1214, 31, 3, '17:00:00', '23:00:00'),
(1215, 31, 4, '17:00:00', '23:00:00'),
(1216, 31, 5, '17:00:00', '23:00:00'),
(1217, 31, 6, '17:00:00', '23:00:00'),
(1218, 31, 7, '17:00:00', '23:00:00'),
(1219, 32, 1, '10:00:00', '22:00:00'),
(1220, 32, 2, '10:00:00', '22:00:00'),
(1221, 32, 3, '10:00:00', '22:00:00'),
(1222, 32, 4, '10:00:00', '22:00:00'),
(1223, 32, 5, '10:00:00', '22:00:00'),
(1224, 32, 6, '10:00:00', '22:00:00'),
(1225, 32, 7, '10:00:00', '22:00:00'),
(1226, 23, 1, '07:30:00', '07:30:00'),
(1227, 23, 2, '07:30:00', '07:30:00'),
(1228, 23, 3, '07:30:00', '07:30:00'),
(1229, 23, 4, '07:30:00', '07:30:00'),
(1230, 23, 5, '07:30:00', '07:30:00'),
(1231, 23, 6, '07:30:00', '07:30:00'),
(1232, 23, 7, '07:30:00', '07:30:00'),
(1233, 25, 1, '09:00:00', '17:00:00'),
(1234, 25, 2, '09:00:00', '17:00:00'),
(1235, 25, 3, '09:00:00', '17:00:00'),
(1236, 25, 4, '09:00:00', '17:00:00'),
(1237, 25, 5, '09:00:00', '17:00:00'),
(1238, 25, 6, '09:05:00', '17:00:00'),
(1239, 25, 7, '09:00:00', '17:00:00'),
(1240, 26, 1, '00:00:00', '07:30:00'),
(1241, 26, 2, '17:30:00', '00:00:00'),
(1242, 26, 3, '17:00:00', '00:00:00'),
(1243, 26, 4, '17:00:00', '00:00:00'),
(1244, 26, 5, '17:00:00', '00:00:00'),
(1245, 26, 6, '17:00:00', '00:00:00'),
(1246, 26, 7, '07:30:00', '07:30:00'),
(1247, 27, 1, '07:30:00', '07:30:00'),
(1248, 27, 2, '07:30:00', '07:30:00'),
(1249, 27, 3, '07:30:00', '07:30:00'),
(1250, 27, 4, '07:30:00', '07:30:00'),
(1251, 27, 5, '07:30:00', '07:30:00'),
(1252, 27, 6, '07:30:00', '07:30:00'),
(1253, 27, 7, '07:30:00', '07:30:00'),
(1254, 36, 1, '11:00:00', '18:30:00'),
(1255, 36, 2, '11:00:00', '18:30:00'),
(1256, 36, 3, '11:00:00', '18:30:00'),
(1257, 36, 4, '11:00:00', '06:30:00'),
(1258, 36, 5, '14:30:00', '18:30:00'),
(1259, 36, 6, '11:00:00', '18:30:00'),
(1260, 36, 7, '11:00:00', '18:30:00'),
(1261, 37, 1, '10:00:00', '22:00:00'),
(1262, 37, 2, '10:00:00', '22:00:00'),
(1263, 37, 3, '10:00:00', '22:00:00'),
(1264, 37, 4, '10:00:00', '22:00:00'),
(1265, 37, 5, '10:00:00', '22:00:00'),
(1266, 37, 6, '10:00:00', '22:00:00'),
(1267, 37, 7, '10:00:00', '22:00:00'),
(1268, 38, 1, '10:00:00', '22:00:00'),
(1269, 38, 2, '10:00:00', '22:00:00'),
(1270, 38, 3, '10:00:00', '22:00:00'),
(1271, 38, 4, '10:00:00', '22:00:00'),
(1272, 38, 5, '10:00:00', '22:00:00'),
(1273, 38, 6, '10:00:00', '22:00:00'),
(1274, 38, 7, '10:00:00', '22:00:00'),
(1275, 39, 1, '09:00:00', '23:00:00'),
(1276, 39, 2, '09:00:00', '23:00:00'),
(1277, 39, 3, '09:00:00', '23:00:00'),
(1278, 39, 4, '07:30:00', '07:30:00'),
(1279, 39, 5, '00:00:00', '07:30:00'),
(1280, 39, 6, '09:00:00', '00:00:00'),
(1281, 39, 7, '09:00:00', '00:00:00'),
(1282, 40, 1, '07:00:00', '18:00:00'),
(1283, 40, 2, '07:00:00', '18:00:00'),
(1284, 40, 3, '07:00:00', '18:00:00'),
(1285, 40, 4, '07:00:00', '18:00:00'),
(1286, 40, 5, '07:00:00', '18:00:00'),
(1287, 40, 6, '07:00:00', '18:00:00'),
(1288, 40, 7, '07:00:00', '18:00:00'),
(1289, 41, 1, '07:30:00', '07:30:00'),
(1290, 41, 2, '07:30:00', '07:30:00'),
(1291, 41, 3, '07:30:00', '07:30:00'),
(1292, 41, 4, '07:30:00', '07:30:00'),
(1293, 41, 5, '07:30:00', '07:30:00'),
(1294, 41, 6, '07:30:00', '07:30:00'),
(1295, 41, 7, '07:30:00', '07:30:00'),
(1296, 42, 1, '07:30:00', '07:30:00'),
(1297, 42, 2, '07:30:00', '07:30:00'),
(1298, 42, 3, '07:30:00', '07:30:00'),
(1299, 42, 4, '07:30:00', '07:30:00'),
(1300, 42, 5, '07:30:00', '07:30:00'),
(1301, 42, 6, '07:30:00', '07:30:00'),
(1302, 42, 7, '07:30:00', '07:30:00'),
(1303, 43, 1, '17:00:00', '05:00:00'),
(1304, 43, 2, '17:00:00', '05:00:00'),
(1305, 43, 3, '17:00:00', '05:00:00'),
(1306, 43, 4, '17:00:00', '05:00:00'),
(1307, 43, 5, '17:00:00', '05:00:00'),
(1308, 43, 6, '17:00:00', '05:00:00'),
(1309, 43, 7, '17:00:00', '05:00:00'),
(1310, 44, 1, '07:30:00', '07:30:00'),
(1311, 44, 2, '07:30:00', '07:30:00'),
(1312, 44, 3, '07:30:00', '07:30:00'),
(1313, 44, 4, '07:30:00', '07:30:00'),
(1314, 44, 5, '07:30:00', '07:30:00'),
(1315, 44, 6, '07:30:00', '07:30:00'),
(1316, 44, 7, '07:30:00', '07:30:00'),
(1317, 45, 1, '10:00:00', '17:00:00'),
(1318, 45, 2, '07:30:00', '07:30:00'),
(1319, 45, 3, '10:00:00', '17:00:00'),
(1320, 45, 4, '10:00:00', '05:00:00'),
(1321, 45, 5, '10:00:00', '19:00:00'),
(1322, 45, 6, '10:00:00', '19:00:00'),
(1323, 45, 7, '10:00:00', '19:00:00'),
(1331, 48, 1, '10:00:00', '22:00:00'),
(1332, 48, 2, '10:00:00', '22:00:00'),
(1333, 48, 3, '10:00:00', '22:00:00'),
(1334, 48, 4, '10:00:00', '22:00:00'),
(1335, 48, 5, '10:00:00', '22:00:00'),
(1336, 48, 6, '10:00:00', '22:00:00'),
(1337, 48, 7, '10:00:00', '22:00:00'),
(1338, 49, 1, '13:00:00', '19:00:00'),
(1339, 49, 2, '13:00:00', '19:00:00'),
(1340, 49, 3, '13:00:00', '19:00:00'),
(1341, 49, 4, '13:00:00', '19:00:00'),
(1342, 49, 5, '07:30:00', '07:30:00'),
(1343, 49, 6, '13:00:00', '19:00:00'),
(1344, 49, 7, '13:00:00', '19:00:00'),
(1345, 53, 1, '07:30:00', '07:30:00'),
(1346, 53, 2, '07:30:00', '07:30:00'),
(1347, 53, 3, '07:30:00', '07:30:00'),
(1348, 53, 4, '07:30:00', '07:30:00'),
(1349, 53, 5, '07:30:00', '07:30:00'),
(1350, 53, 6, '07:30:00', '07:30:00'),
(1351, 53, 7, '07:30:00', '07:30:00'),
(1366, 9, 1, '07:30:00', '07:30:00'),
(1367, 9, 2, '07:30:00', '07:30:00'),
(1368, 9, 3, '07:30:00', '07:30:00'),
(1369, 9, 4, '07:30:00', '07:30:00'),
(1370, 9, 5, '07:30:00', '07:30:00'),
(1371, 9, 6, '07:30:00', '07:30:00'),
(1372, 9, 7, '07:30:00', '07:30:00'),
(1373, 8, 1, '07:30:00', '07:30:00'),
(1374, 8, 2, '07:30:00', '07:30:00'),
(1375, 8, 3, '07:30:00', '07:30:00'),
(1376, 8, 4, '07:30:00', '07:30:00'),
(1377, 8, 5, '07:30:00', '07:30:00'),
(1378, 8, 6, '07:30:00', '07:30:00'),
(1379, 8, 7, '07:30:00', '07:30:00'),
(1387, 57, 1, '09:00:00', '18:00:00'),
(1388, 57, 2, '09:00:00', '18:00:00'),
(1389, 57, 3, '09:00:00', '18:00:00'),
(1390, 57, 4, '09:00:00', '18:00:00'),
(1391, 57, 5, '09:00:00', '18:00:00'),
(1392, 57, 6, '09:00:00', '18:00:00'),
(1393, 57, 7, '09:00:00', '18:00:00'),
(1408, 46, 1, '07:30:00', '07:30:00'),
(1409, 46, 2, '07:30:00', '07:30:00'),
(1410, 46, 3, '07:30:00', '07:30:00'),
(1411, 46, 4, '07:30:00', '07:30:00'),
(1412, 46, 5, '07:30:00', '07:30:00'),
(1413, 46, 6, '07:30:00', '07:30:00'),
(1414, 46, 7, '07:30:00', '07:30:00'),
(1415, 3, 1, '07:30:00', '07:30:00'),
(1416, 3, 2, '07:30:00', '07:30:00'),
(1417, 3, 3, '07:30:00', '07:30:00'),
(1418, 3, 4, '07:30:00', '07:30:00'),
(1419, 3, 5, '07:30:00', '07:30:00'),
(1420, 3, 6, '07:30:00', '07:30:00'),
(1421, 3, 7, '07:30:00', '07:30:00'),
(1423, 66, 2, '10:00:00', '18:00:00'),
(1424, 66, 3, '10:00:00', '18:00:00'),
(1425, 66, 4, '10:00:00', '18:00:00'),
(1426, 66, 5, '10:00:00', '18:00:00'),
(1427, 66, 6, '10:00:00', '18:00:00'),
(1428, 66, 7, '10:00:00', '18:00:00'),
(1429, 67, 1, '10:00:00', '18:00:00'),
(1430, 67, 2, '10:00:00', '18:00:00'),
(1431, 67, 3, '10:00:00', '18:00:00'),
(1432, 67, 4, '10:00:00', '18:00:00'),
(1433, 67, 5, '10:00:00', '18:00:00'),
(1434, 67, 6, '10:00:00', '18:00:00'),
(1435, 67, 7, '10:00:00', '18:00:00'),
(1464, 72, 1, '10:00:00', '18:00:00'),
(1465, 72, 2, '10:00:00', '18:00:00'),
(1466, 72, 3, '10:00:00', '18:00:00'),
(1467, 72, 4, '10:00:00', '18:00:00'),
(1468, 72, 5, '10:00:00', '18:00:00'),
(1469, 72, 6, '10:00:00', '18:00:00'),
(1470, 72, 7, '10:00:00', '18:00:00'),
(1513, 79, 1, '10:00:00', '18:00:00'),
(1514, 79, 2, '10:00:00', '18:00:00'),
(1515, 79, 3, '10:00:00', '18:00:00'),
(1516, 79, 4, '10:00:00', '18:00:00'),
(1517, 79, 5, '10:00:00', '18:00:00'),
(1518, 79, 6, '10:00:00', '18:00:00'),
(1519, 79, 7, '10:00:00', '18:00:00'),
(1520, 80, 1, '10:00:00', '18:00:00'),
(1521, 80, 2, '10:00:00', '18:00:00'),
(1522, 80, 3, '10:00:00', '18:00:00'),
(1523, 80, 4, '10:00:00', '18:00:00'),
(1524, 80, 5, '10:00:00', '18:00:00'),
(1525, 80, 6, '10:00:00', '18:00:00'),
(1526, 80, 7, '10:00:00', '18:00:00'),
(1527, 81, 1, '10:00:00', '18:00:00'),
(1528, 81, 2, '10:00:00', '18:00:00'),
(1529, 81, 3, '10:00:00', '18:00:00'),
(1530, 81, 4, '10:00:00', '18:00:00'),
(1531, 81, 5, '10:00:00', '18:00:00'),
(1532, 81, 6, '10:00:00', '18:00:00'),
(1533, 81, 7, '10:00:00', '18:00:00'),
(1534, 82, 1, '10:00:00', '18:00:00'),
(1535, 82, 2, '10:00:00', '18:00:00'),
(1536, 82, 3, '10:00:00', '18:00:00'),
(1537, 82, 4, '10:00:00', '18:00:00'),
(1538, 82, 5, '10:00:00', '18:00:00'),
(1539, 82, 6, '10:00:00', '18:00:00'),
(1540, 82, 7, '10:00:00', '18:00:00'),
(1541, 83, 1, '10:00:00', '18:00:00'),
(1542, 83, 2, '10:00:00', '18:00:00'),
(1543, 83, 3, '10:00:00', '18:00:00'),
(1544, 83, 4, '10:00:00', '18:00:00'),
(1545, 83, 5, '10:00:00', '18:00:00'),
(1546, 83, 6, '10:00:00', '18:00:00'),
(1547, 83, 7, '10:00:00', '18:00:00'),
(1548, 84, 1, '10:00:00', '18:00:00'),
(1549, 84, 2, '10:00:00', '18:00:00'),
(1550, 84, 3, '10:00:00', '18:00:00'),
(1551, 84, 4, '10:00:00', '18:00:00'),
(1552, 84, 5, '10:00:00', '18:00:00'),
(1553, 84, 6, '10:00:00', '18:00:00'),
(1554, 84, 7, '10:00:00', '18:00:00'),
(1555, 85, 1, '10:00:00', '18:00:00'),
(1556, 85, 2, '10:00:00', '18:00:00'),
(1557, 85, 3, '10:00:00', '18:00:00'),
(1558, 85, 4, '10:00:00', '18:00:00'),
(1559, 85, 5, '10:00:00', '18:00:00'),
(1560, 85, 6, '10:00:00', '18:00:00'),
(1561, 85, 7, '10:00:00', '18:00:00'),
(1562, 86, 1, '10:00:00', '18:00:00'),
(1563, 86, 2, '10:00:00', '18:00:00'),
(1564, 86, 3, '10:00:00', '18:00:00'),
(1565, 86, 4, '10:00:00', '18:00:00'),
(1566, 86, 5, '10:00:00', '18:00:00'),
(1567, 86, 6, '10:00:00', '18:00:00'),
(1568, 86, 7, '10:00:00', '18:00:00'),
(1569, 87, 1, '10:00:00', '18:00:00'),
(1570, 87, 2, '10:00:00', '18:00:00'),
(1571, 87, 3, '10:00:00', '18:00:00'),
(1572, 87, 4, '10:00:00', '18:00:00'),
(1573, 87, 5, '10:00:00', '18:00:00'),
(1574, 87, 6, '10:00:00', '18:00:00'),
(1575, 87, 7, '10:00:00', '18:00:00'),
(1576, 88, 1, '10:00:00', '18:00:00'),
(1577, 88, 2, '10:00:00', '18:00:00'),
(1578, 88, 3, '10:00:00', '18:00:00'),
(1579, 88, 4, '10:00:00', '18:00:00'),
(1580, 88, 5, '10:00:00', '18:00:00'),
(1581, 88, 6, '10:00:00', '18:00:00'),
(1582, 88, 7, '10:00:00', '18:00:00'),
(1583, 89, 1, '10:00:00', '18:00:00'),
(1584, 89, 2, '10:00:00', '18:00:00'),
(1585, 89, 3, '10:00:00', '18:00:00'),
(1586, 89, 4, '10:00:00', '18:00:00'),
(1587, 89, 5, '10:00:00', '18:00:00'),
(1588, 89, 6, '10:00:00', '18:00:00'),
(1589, 89, 7, '10:00:00', '18:00:00'),
(1590, 90, 1, '10:00:00', '18:00:00'),
(1591, 90, 2, '10:00:00', '18:00:00'),
(1592, 90, 3, '10:00:00', '18:00:00'),
(1593, 90, 4, '10:00:00', '18:00:00'),
(1594, 90, 5, '10:00:00', '18:00:00'),
(1595, 90, 6, '10:00:00', '18:00:00'),
(1596, 90, 7, '10:00:00', '18:00:00'),
(1597, 91, 1, '10:00:00', '18:00:00'),
(1598, 91, 2, '10:00:00', '18:00:00'),
(1599, 91, 3, '10:00:00', '18:00:00'),
(1600, 91, 4, '10:00:00', '18:00:00'),
(1601, 91, 5, '10:00:00', '18:00:00'),
(1602, 91, 6, '10:00:00', '18:00:00'),
(1603, 91, 7, '10:00:00', '18:00:00'),
(1604, 92, 1, '10:00:00', '18:00:00'),
(1605, 92, 2, '10:00:00', '18:00:00'),
(1606, 92, 3, '10:00:00', '18:00:00'),
(1607, 92, 4, '10:00:00', '18:00:00'),
(1608, 92, 5, '10:00:00', '18:00:00'),
(1609, 92, 6, '10:00:00', '18:00:00'),
(1610, 92, 7, '10:00:00', '18:00:00'),
(1611, 93, 1, '10:00:00', '18:00:00'),
(1612, 93, 2, '10:00:00', '18:00:00'),
(1613, 93, 3, '10:00:00', '18:00:00'),
(1614, 93, 4, '10:00:00', '18:00:00'),
(1615, 93, 5, '10:00:00', '18:00:00'),
(1616, 93, 6, '10:00:00', '18:00:00'),
(1617, 93, 7, '10:00:00', '18:00:00'),
(1618, 94, 1, '10:00:00', '18:00:00'),
(1619, 94, 2, '10:00:00', '18:00:00'),
(1620, 94, 3, '10:00:00', '18:00:00'),
(1621, 94, 4, '10:00:00', '18:00:00'),
(1622, 94, 5, '10:00:00', '18:00:00'),
(1623, 94, 6, '10:00:00', '18:00:00'),
(1624, 94, 7, '10:00:00', '18:00:00'),
(1625, 95, 1, '10:00:00', '18:00:00'),
(1626, 95, 2, '10:00:00', '18:00:00'),
(1627, 95, 3, '10:00:00', '18:00:00'),
(1628, 95, 4, '10:00:00', '18:00:00'),
(1629, 95, 5, '10:00:00', '18:00:00'),
(1630, 95, 6, '10:00:00', '18:00:00'),
(1631, 95, 7, '10:00:00', '18:00:00'),
(1632, 96, 1, '10:00:00', '18:00:00'),
(1633, 96, 2, '10:00:00', '18:00:00'),
(1634, 96, 3, '10:00:00', '18:00:00'),
(1635, 96, 4, '10:00:00', '18:00:00'),
(1636, 96, 5, '10:00:00', '18:00:00'),
(1637, 96, 6, '10:00:00', '18:00:00'),
(1638, 96, 7, '10:00:00', '18:00:00'),
(1639, 97, 1, '10:00:00', '18:00:00'),
(1640, 97, 2, '10:00:00', '18:00:00'),
(1641, 97, 3, '10:00:00', '18:00:00'),
(1642, 97, 4, '10:00:00', '18:00:00'),
(1643, 97, 5, '10:00:00', '18:00:00'),
(1644, 97, 6, '10:00:00', '18:00:00'),
(1645, 97, 7, '10:00:00', '18:00:00'),
(1646, 98, 1, '10:00:00', '18:00:00'),
(1647, 98, 2, '10:00:00', '18:00:00'),
(1648, 98, 3, '10:00:00', '18:00:00'),
(1649, 98, 4, '10:00:00', '18:00:00'),
(1650, 98, 5, '10:00:00', '18:00:00'),
(1651, 98, 6, '10:00:00', '18:00:00'),
(1652, 98, 7, '10:00:00', '18:00:00'),
(1653, 99, 1, '10:00:00', '18:00:00'),
(1654, 99, 2, '10:00:00', '18:00:00'),
(1655, 99, 3, '10:00:00', '18:00:00'),
(1656, 99, 4, '10:00:00', '18:00:00'),
(1657, 99, 5, '10:00:00', '18:00:00'),
(1658, 99, 6, '10:00:00', '18:00:00'),
(1659, 99, 7, '10:00:00', '18:00:00'),
(1660, 100, 1, '10:00:00', '18:00:00'),
(1661, 100, 2, '10:00:00', '18:00:00'),
(1662, 100, 3, '10:00:00', '18:00:00'),
(1663, 100, 4, '10:00:00', '18:00:00'),
(1664, 100, 5, '10:00:00', '18:00:00'),
(1665, 100, 6, '10:00:00', '18:00:00'),
(1666, 100, 7, '10:00:00', '18:00:00'),
(1667, 101, 1, '10:00:00', '18:00:00'),
(1668, 101, 2, '10:00:00', '18:00:00'),
(1669, 101, 3, '10:00:00', '18:00:00'),
(1670, 101, 4, '10:00:00', '18:00:00'),
(1671, 101, 5, '10:00:00', '18:00:00'),
(1672, 101, 6, '10:00:00', '18:00:00'),
(1673, 101, 7, '10:00:00', '18:00:00'),
(1674, 102, 1, '10:00:00', '18:00:00'),
(1675, 102, 2, '10:00:00', '18:00:00'),
(1676, 102, 3, '10:00:00', '18:00:00'),
(1677, 102, 4, '10:00:00', '18:00:00'),
(1678, 102, 5, '10:00:00', '18:00:00'),
(1679, 102, 6, '10:00:00', '18:00:00'),
(1680, 102, 7, '10:00:00', '18:00:00'),
(1681, 103, 1, '10:00:00', '18:00:00'),
(1682, 103, 2, '10:00:00', '18:00:00'),
(1683, 103, 3, '10:00:00', '18:00:00'),
(1684, 103, 4, '10:00:00', '18:00:00'),
(1685, 103, 5, '10:00:00', '18:00:00'),
(1686, 103, 6, '10:00:00', '18:00:00'),
(1687, 103, 7, '10:00:00', '18:00:00'),
(1688, 104, 1, '10:00:00', '18:00:00'),
(1689, 104, 2, '10:00:00', '18:00:00'),
(1690, 104, 3, '10:00:00', '18:00:00'),
(1691, 104, 4, '10:00:00', '18:00:00'),
(1692, 104, 5, '10:00:00', '18:00:00'),
(1693, 104, 6, '10:00:00', '18:00:00'),
(1694, 104, 7, '10:00:00', '18:00:00'),
(1695, 105, 1, '10:00:00', '18:00:00'),
(1696, 105, 2, '10:00:00', '18:00:00'),
(1697, 105, 3, '10:00:00', '18:00:00'),
(1698, 105, 4, '10:00:00', '18:00:00'),
(1699, 105, 5, '10:00:00', '18:00:00'),
(1700, 105, 6, '10:00:00', '18:00:00'),
(1701, 105, 7, '10:00:00', '18:00:00'),
(1702, 106, 1, '10:00:00', '18:00:00'),
(1703, 106, 2, '10:00:00', '18:00:00'),
(1704, 106, 3, '10:00:00', '18:00:00'),
(1705, 106, 4, '10:00:00', '18:00:00'),
(1706, 106, 5, '10:00:00', '18:00:00'),
(1707, 106, 6, '10:00:00', '18:00:00'),
(1708, 106, 7, '10:00:00', '18:00:00'),
(1709, 107, 1, '10:00:00', '18:00:00'),
(1710, 107, 2, '10:00:00', '18:00:00'),
(1711, 107, 3, '10:00:00', '18:00:00'),
(1712, 107, 4, '10:00:00', '18:00:00'),
(1713, 107, 5, '10:00:00', '18:00:00'),
(1714, 107, 6, '10:00:00', '18:00:00'),
(1715, 107, 7, '10:00:00', '18:00:00'),
(1716, 108, 1, '10:00:00', '18:00:00'),
(1717, 108, 2, '10:00:00', '18:00:00'),
(1718, 108, 3, '10:00:00', '18:00:00'),
(1719, 108, 4, '10:00:00', '18:00:00'),
(1720, 108, 5, '10:00:00', '18:00:00'),
(1721, 108, 6, '10:00:00', '18:00:00'),
(1722, 108, 7, '10:00:00', '18:00:00'),
(1723, 109, 1, '10:00:00', '18:00:00'),
(1724, 109, 2, '10:00:00', '18:00:00'),
(1725, 109, 3, '10:00:00', '18:00:00'),
(1726, 109, 4, '10:00:00', '18:00:00'),
(1727, 109, 5, '10:00:00', '18:00:00'),
(1728, 109, 6, '10:00:00', '18:00:00'),
(1729, 109, 7, '10:00:00', '18:00:00'),
(1730, 110, 1, '10:00:00', '18:00:00'),
(1731, 110, 2, '10:00:00', '18:00:00'),
(1732, 110, 3, '10:00:00', '18:00:00'),
(1733, 110, 4, '10:00:00', '18:00:00'),
(1734, 110, 5, '10:00:00', '18:00:00'),
(1735, 110, 6, '10:00:00', '18:00:00'),
(1736, 110, 7, '10:00:00', '18:00:00'),
(1737, 111, 1, '10:00:00', '18:00:00'),
(1738, 111, 2, '10:00:00', '18:00:00'),
(1739, 111, 3, '10:00:00', '18:00:00'),
(1740, 111, 4, '10:00:00', '18:00:00'),
(1741, 111, 5, '10:00:00', '18:00:00'),
(1742, 111, 6, '10:00:00', '18:00:00'),
(1743, 111, 7, '10:00:00', '18:00:00'),
(1744, 112, 1, '10:00:00', '18:00:00'),
(1745, 112, 2, '10:00:00', '18:00:00'),
(1746, 112, 3, '10:00:00', '18:00:00'),
(1747, 112, 4, '10:00:00', '18:00:00'),
(1748, 112, 5, '10:00:00', '18:00:00'),
(1749, 112, 6, '10:00:00', '18:00:00'),
(1750, 112, 7, '10:00:00', '18:00:00'),
(1751, 113, 1, '10:00:00', '18:00:00'),
(1752, 113, 2, '10:00:00', '18:00:00'),
(1753, 113, 3, '10:00:00', '18:00:00'),
(1754, 113, 4, '10:00:00', '18:00:00'),
(1755, 113, 5, '10:00:00', '18:00:00'),
(1756, 113, 6, '10:00:00', '18:00:00'),
(1757, 113, 7, '10:00:00', '18:00:00'),
(1758, 114, 1, '10:00:00', '18:00:00'),
(1759, 114, 2, '10:00:00', '18:00:00'),
(1760, 114, 3, '10:00:00', '18:00:00'),
(1761, 114, 4, '10:00:00', '18:00:00'),
(1762, 114, 5, '10:00:00', '18:00:00'),
(1763, 114, 6, '10:00:00', '18:00:00'),
(1764, 114, 7, '10:00:00', '18:00:00'),
(1765, 115, 1, '10:00:00', '18:00:00'),
(1766, 115, 2, '10:00:00', '18:00:00'),
(1767, 115, 3, '10:00:00', '18:00:00'),
(1768, 115, 4, '10:00:00', '18:00:00'),
(1769, 115, 5, '10:00:00', '18:00:00'),
(1770, 115, 6, '10:00:00', '18:00:00'),
(1771, 115, 7, '10:00:00', '18:00:00'),
(1772, 116, 1, '10:00:00', '18:00:00'),
(1773, 116, 2, '10:00:00', '18:00:00'),
(1774, 116, 3, '10:00:00', '18:00:00'),
(1775, 116, 4, '10:00:00', '18:00:00'),
(1776, 116, 5, '10:00:00', '18:00:00'),
(1777, 116, 6, '10:00:00', '18:00:00'),
(1778, 116, 7, '10:00:00', '18:00:00'),
(1779, 117, 1, '10:00:00', '18:00:00'),
(1780, 117, 2, '10:00:00', '18:00:00'),
(1781, 117, 3, '10:00:00', '18:00:00'),
(1782, 117, 4, '10:00:00', '18:00:00'),
(1783, 117, 5, '10:00:00', '18:00:00'),
(1784, 117, 6, '10:00:00', '18:00:00'),
(1785, 117, 7, '10:00:00', '18:00:00'),
(1786, 118, 1, '10:00:00', '18:00:00'),
(1787, 118, 2, '10:00:00', '18:00:00'),
(1788, 118, 3, '10:00:00', '18:00:00'),
(1789, 118, 4, '10:00:00', '18:00:00'),
(1790, 118, 5, '10:00:00', '18:00:00'),
(1791, 118, 6, '10:00:00', '18:00:00'),
(1792, 118, 7, '10:00:00', '18:00:00'),
(1793, 119, 1, '10:00:00', '18:00:00'),
(1794, 119, 2, '10:00:00', '18:00:00'),
(1795, 119, 3, '10:00:00', '18:00:00'),
(1796, 119, 4, '10:00:00', '18:00:00'),
(1797, 119, 5, '10:00:00', '18:00:00'),
(1798, 119, 6, '10:00:00', '18:00:00'),
(1799, 119, 7, '10:00:00', '18:00:00'),
(1800, 120, 1, '10:00:00', '18:00:00'),
(1801, 120, 2, '10:00:00', '18:00:00'),
(1802, 120, 3, '10:00:00', '18:00:00'),
(1803, 120, 4, '10:00:00', '18:00:00'),
(1804, 120, 5, '10:00:00', '18:00:00'),
(1805, 120, 6, '10:00:00', '18:00:00'),
(1806, 120, 7, '10:00:00', '18:00:00'),
(1807, 121, 1, '10:00:00', '18:00:00'),
(1808, 121, 2, '10:00:00', '18:00:00'),
(1809, 121, 3, '10:00:00', '18:00:00'),
(1810, 121, 4, '10:00:00', '18:00:00'),
(1811, 121, 5, '10:00:00', '18:00:00'),
(1812, 121, 6, '10:00:00', '18:00:00'),
(1813, 121, 7, '10:00:00', '18:00:00'),
(1814, 122, 1, '10:00:00', '18:00:00'),
(1815, 122, 2, '10:00:00', '18:00:00'),
(1816, 122, 3, '10:00:00', '18:00:00'),
(1817, 122, 4, '10:00:00', '18:00:00'),
(1818, 122, 5, '10:00:00', '18:00:00'),
(1819, 122, 6, '10:00:00', '18:00:00'),
(1820, 122, 7, '10:00:00', '18:00:00'),
(1821, 123, 1, '10:00:00', '18:00:00'),
(1822, 123, 2, '10:00:00', '18:00:00'),
(1823, 123, 3, '10:00:00', '18:00:00'),
(1824, 123, 4, '10:00:00', '18:00:00'),
(1825, 123, 5, '10:00:00', '18:00:00'),
(1826, 123, 6, '10:00:00', '18:00:00'),
(1827, 123, 7, '10:00:00', '18:00:00'),
(1828, 124, 1, '10:00:00', '18:00:00'),
(1829, 124, 2, '10:00:00', '18:00:00'),
(1830, 124, 3, '10:00:00', '18:00:00'),
(1831, 124, 4, '10:00:00', '18:00:00'),
(1832, 124, 5, '10:00:00', '18:00:00'),
(1833, 124, 6, '10:00:00', '18:00:00'),
(1834, 124, 7, '10:00:00', '18:00:00'),
(1835, 125, 1, '10:00:00', '18:00:00'),
(1836, 125, 2, '10:00:00', '18:00:00'),
(1837, 125, 3, '10:00:00', '18:00:00'),
(1838, 125, 4, '10:00:00', '18:00:00'),
(1839, 125, 5, '10:00:00', '18:00:00'),
(1840, 125, 6, '10:00:00', '18:00:00'),
(1841, 125, 7, '10:00:00', '18:00:00'),
(1842, 126, 1, '10:00:00', '18:00:00'),
(1843, 126, 2, '10:00:00', '18:00:00'),
(1844, 126, 3, '10:00:00', '18:00:00'),
(1845, 126, 4, '10:00:00', '18:00:00'),
(1846, 126, 5, '10:00:00', '18:00:00'),
(1847, 126, 6, '10:00:00', '18:00:00'),
(1848, 126, 7, '10:00:00', '18:00:00'),
(1849, 127, 1, '10:00:00', '18:00:00'),
(1850, 127, 2, '10:00:00', '18:00:00'),
(1851, 127, 3, '10:00:00', '18:00:00'),
(1852, 127, 4, '10:00:00', '18:00:00'),
(1853, 127, 5, '10:00:00', '18:00:00'),
(1854, 127, 6, '10:00:00', '18:00:00'),
(1855, 127, 7, '10:00:00', '18:00:00'),
(1856, 128, 1, '10:00:00', '18:00:00'),
(1857, 128, 2, '10:00:00', '18:00:00'),
(1858, 128, 3, '10:00:00', '18:00:00'),
(1859, 128, 4, '10:00:00', '18:00:00'),
(1860, 128, 5, '10:00:00', '18:00:00'),
(1861, 128, 6, '10:00:00', '18:00:00'),
(1862, 128, 7, '10:00:00', '18:00:00'),
(1863, 129, 1, '10:00:00', '18:00:00'),
(1864, 129, 2, '10:00:00', '18:00:00'),
(1865, 129, 3, '10:00:00', '18:00:00'),
(1866, 129, 4, '10:00:00', '18:00:00'),
(1867, 129, 5, '10:00:00', '18:00:00'),
(1868, 129, 6, '10:00:00', '18:00:00'),
(1869, 129, 7, '10:00:00', '18:00:00'),
(1870, 130, 1, '10:00:00', '18:00:00'),
(1871, 130, 2, '10:00:00', '18:00:00'),
(1872, 130, 3, '10:00:00', '18:00:00'),
(1873, 130, 4, '10:00:00', '18:00:00'),
(1874, 130, 5, '10:00:00', '18:00:00'),
(1875, 130, 6, '10:00:00', '18:00:00'),
(1876, 130, 7, '10:00:00', '18:00:00'),
(1877, 131, 1, '10:00:00', '18:00:00'),
(1878, 131, 2, '10:00:00', '18:00:00'),
(1879, 131, 3, '10:00:00', '18:00:00'),
(1880, 131, 4, '10:00:00', '18:00:00'),
(1881, 131, 5, '10:00:00', '18:00:00'),
(1882, 131, 6, '10:00:00', '18:00:00'),
(1883, 131, 7, '10:00:00', '18:00:00'),
(1884, 132, 1, '10:00:00', '18:00:00'),
(1885, 132, 2, '10:00:00', '18:00:00'),
(1886, 132, 3, '10:00:00', '18:00:00'),
(1887, 132, 4, '10:00:00', '18:00:00'),
(1888, 132, 5, '10:00:00', '18:00:00'),
(1889, 132, 6, '10:00:00', '18:00:00'),
(1890, 132, 7, '10:00:00', '18:00:00'),
(1891, 133, 1, '10:00:00', '18:00:00'),
(1892, 133, 2, '10:00:00', '18:00:00'),
(1893, 133, 3, '10:00:00', '18:00:00'),
(1894, 133, 4, '10:00:00', '18:00:00'),
(1895, 133, 5, '10:00:00', '18:00:00'),
(1896, 133, 6, '10:00:00', '18:00:00'),
(1897, 133, 7, '10:00:00', '18:00:00'),
(1898, 134, 1, '10:00:00', '18:00:00'),
(1899, 134, 2, '10:00:00', '18:00:00'),
(1900, 134, 3, '10:00:00', '18:00:00'),
(1901, 134, 4, '10:00:00', '18:00:00'),
(1902, 134, 5, '10:00:00', '18:00:00'),
(1903, 134, 6, '10:00:00', '18:00:00'),
(1904, 134, 7, '10:00:00', '18:00:00'),
(1905, 135, 1, '10:00:00', '18:00:00'),
(1906, 135, 2, '10:00:00', '18:00:00'),
(1907, 135, 3, '10:00:00', '18:00:00'),
(1908, 135, 4, '10:00:00', '18:00:00'),
(1909, 135, 5, '10:00:00', '18:00:00'),
(1910, 135, 6, '10:00:00', '18:00:00'),
(1911, 135, 7, '10:00:00', '18:00:00'),
(1912, 136, 1, '10:00:00', '18:00:00'),
(1913, 136, 2, '10:00:00', '18:00:00'),
(1914, 136, 3, '10:00:00', '18:00:00'),
(1915, 136, 4, '10:00:00', '18:00:00'),
(1916, 136, 5, '10:00:00', '18:00:00'),
(1917, 136, 6, '10:00:00', '18:00:00'),
(1918, 136, 7, '10:00:00', '18:00:00'),
(1919, 137, 1, '10:00:00', '18:00:00'),
(1920, 137, 2, '10:00:00', '18:00:00'),
(1921, 137, 3, '10:00:00', '18:00:00'),
(1922, 137, 4, '10:00:00', '18:00:00'),
(1923, 137, 5, '10:00:00', '18:00:00'),
(1924, 137, 6, '10:00:00', '18:00:00'),
(1925, 137, 7, '10:00:00', '18:00:00'),
(1926, 138, 1, '10:00:00', '18:00:00'),
(1927, 138, 2, '10:00:00', '18:00:00'),
(1928, 138, 3, '10:00:00', '18:00:00'),
(1929, 138, 4, '10:00:00', '18:00:00'),
(1930, 138, 5, '10:00:00', '18:00:00'),
(1931, 138, 6, '10:00:00', '18:00:00'),
(1932, 138, 7, '10:00:00', '18:00:00'),
(1933, 139, 1, '10:00:00', '18:00:00'),
(1934, 139, 2, '10:00:00', '18:00:00'),
(1935, 139, 3, '10:00:00', '18:00:00'),
(1936, 139, 4, '10:00:00', '18:00:00'),
(1937, 139, 5, '10:00:00', '18:00:00'),
(1938, 139, 6, '10:00:00', '18:00:00'),
(1939, 139, 7, '10:00:00', '18:00:00'),
(1940, 140, 1, '10:00:00', '18:00:00'),
(1941, 140, 2, '10:00:00', '18:00:00'),
(1942, 140, 3, '10:00:00', '18:00:00'),
(1943, 140, 4, '10:00:00', '18:00:00'),
(1944, 140, 5, '10:00:00', '18:00:00'),
(1945, 140, 6, '10:00:00', '18:00:00'),
(1946, 140, 7, '10:00:00', '18:00:00'),
(1947, 141, 1, '10:00:00', '18:00:00'),
(1948, 141, 2, '10:00:00', '18:00:00'),
(1949, 141, 3, '10:00:00', '18:00:00'),
(1950, 141, 4, '10:00:00', '18:00:00'),
(1951, 141, 5, '10:00:00', '18:00:00'),
(1952, 141, 6, '10:00:00', '18:00:00'),
(1953, 141, 7, '10:00:00', '18:00:00'),
(1954, 142, 1, '10:00:00', '18:00:00'),
(1955, 142, 2, '10:00:00', '18:00:00'),
(1956, 142, 3, '10:00:00', '18:00:00'),
(1957, 142, 4, '10:00:00', '18:00:00'),
(1958, 142, 5, '10:00:00', '18:00:00'),
(1959, 142, 6, '10:00:00', '18:00:00'),
(1960, 142, 7, '10:00:00', '18:00:00'),
(1961, 143, 1, '10:00:00', '18:00:00'),
(1962, 143, 2, '10:00:00', '18:00:00'),
(1963, 143, 3, '10:00:00', '18:00:00'),
(1964, 143, 4, '10:00:00', '18:00:00'),
(1965, 143, 5, '10:00:00', '18:00:00'),
(1966, 143, 6, '10:00:00', '18:00:00'),
(1967, 143, 7, '10:00:00', '18:00:00'),
(1968, 144, 1, '10:00:00', '18:00:00'),
(1969, 144, 2, '10:00:00', '18:00:00'),
(1970, 144, 3, '10:00:00', '18:00:00'),
(1971, 144, 4, '10:00:00', '18:00:00'),
(1972, 144, 5, '10:00:00', '18:00:00'),
(1973, 144, 6, '10:00:00', '18:00:00'),
(1974, 144, 7, '10:00:00', '18:00:00'),
(1975, 145, 1, '10:00:00', '18:00:00'),
(1976, 145, 2, '10:00:00', '18:00:00'),
(1977, 145, 3, '10:00:00', '18:00:00'),
(1978, 145, 4, '10:00:00', '18:00:00'),
(1979, 145, 5, '10:00:00', '18:00:00'),
(1980, 145, 6, '10:00:00', '18:00:00'),
(1981, 145, 7, '10:00:00', '18:00:00'),
(1982, 146, 1, '10:00:00', '18:00:00'),
(1983, 146, 2, '10:00:00', '18:00:00'),
(1984, 146, 3, '10:00:00', '18:00:00'),
(1985, 146, 4, '10:00:00', '18:00:00'),
(1986, 146, 5, '10:00:00', '18:00:00'),
(1987, 146, 6, '10:00:00', '18:00:00'),
(1988, 146, 7, '10:00:00', '18:00:00'),
(1989, 147, 1, '10:00:00', '18:00:00'),
(1990, 147, 2, '10:00:00', '18:00:00'),
(1991, 147, 3, '10:00:00', '18:00:00'),
(1992, 147, 4, '10:00:00', '18:00:00'),
(1993, 147, 5, '10:00:00', '18:00:00'),
(1994, 147, 6, '10:00:00', '18:00:00'),
(1995, 147, 7, '10:00:00', '18:00:00'),
(1996, 148, 1, '10:00:00', '18:00:00'),
(1997, 148, 2, '10:00:00', '18:00:00'),
(1998, 148, 3, '10:00:00', '18:00:00'),
(1999, 148, 4, '10:00:00', '18:00:00'),
(2000, 148, 5, '10:00:00', '18:00:00'),
(2001, 148, 6, '10:00:00', '18:00:00'),
(2002, 148, 7, '10:00:00', '18:00:00'),
(2003, 149, 1, '10:00:00', '18:00:00'),
(2004, 149, 2, '10:00:00', '18:00:00'),
(2005, 149, 3, '10:00:00', '18:00:00'),
(2006, 149, 4, '10:00:00', '18:00:00'),
(2007, 149, 5, '10:00:00', '18:00:00'),
(2008, 149, 6, '10:00:00', '18:00:00'),
(2009, 149, 7, '10:00:00', '18:00:00'),
(2010, 150, 1, '10:00:00', '18:00:00'),
(2011, 150, 2, '10:00:00', '18:00:00'),
(2012, 150, 3, '10:00:00', '18:00:00'),
(2013, 150, 4, '10:00:00', '18:00:00'),
(2014, 150, 5, '10:00:00', '18:00:00'),
(2015, 150, 6, '10:00:00', '18:00:00'),
(2016, 150, 7, '10:00:00', '18:00:00'),
(2017, 151, 1, '10:00:00', '18:00:00'),
(2018, 151, 2, '10:00:00', '18:00:00'),
(2019, 151, 3, '10:00:00', '18:00:00'),
(2020, 151, 4, '10:00:00', '18:00:00'),
(2021, 151, 5, '10:00:00', '18:00:00'),
(2022, 151, 6, '10:00:00', '18:00:00'),
(2023, 151, 7, '10:00:00', '18:00:00'),
(2024, 152, 1, '10:00:00', '18:00:00'),
(2025, 152, 2, '10:00:00', '18:00:00'),
(2026, 152, 3, '10:00:00', '18:00:00'),
(2027, 152, 4, '10:00:00', '18:00:00'),
(2028, 152, 5, '10:00:00', '18:00:00'),
(2029, 152, 6, '10:00:00', '18:00:00'),
(2030, 152, 7, '10:00:00', '18:00:00'),
(2031, 153, 1, '10:00:00', '18:00:00'),
(2032, 153, 2, '10:00:00', '18:00:00'),
(2033, 153, 3, '10:00:00', '18:00:00'),
(2034, 153, 4, '10:00:00', '18:00:00'),
(2035, 153, 5, '10:00:00', '18:00:00'),
(2036, 153, 6, '10:00:00', '18:00:00'),
(2037, 153, 7, '10:00:00', '18:00:00'),
(2038, 154, 1, '10:00:00', '18:00:00'),
(2039, 154, 2, '10:00:00', '18:00:00'),
(2040, 154, 3, '10:00:00', '18:00:00'),
(2041, 154, 4, '10:00:00', '18:00:00'),
(2042, 154, 5, '10:00:00', '18:00:00'),
(2043, 154, 6, '10:00:00', '18:00:00'),
(2044, 154, 7, '10:00:00', '18:00:00'),
(2045, 155, 1, '10:00:00', '18:00:00'),
(2046, 155, 2, '10:00:00', '18:00:00'),
(2047, 155, 3, '10:00:00', '18:00:00'),
(2048, 155, 4, '10:00:00', '18:00:00'),
(2049, 155, 5, '10:00:00', '18:00:00'),
(2050, 155, 6, '10:00:00', '18:00:00'),
(2051, 155, 7, '10:00:00', '18:00:00'),
(2052, 156, 1, '10:00:00', '18:00:00'),
(2053, 156, 2, '10:00:00', '18:00:00'),
(2054, 156, 3, '10:00:00', '18:00:00'),
(2055, 156, 4, '10:00:00', '18:00:00'),
(2056, 156, 5, '10:00:00', '18:00:00'),
(2057, 156, 6, '10:00:00', '18:00:00'),
(2058, 156, 7, '10:00:00', '18:00:00'),
(2059, 157, 1, '10:00:00', '18:00:00'),
(2060, 157, 2, '10:00:00', '18:00:00'),
(2061, 157, 3, '10:00:00', '18:00:00'),
(2062, 157, 4, '10:00:00', '18:00:00'),
(2063, 157, 5, '10:00:00', '18:00:00'),
(2064, 157, 6, '10:00:00', '18:00:00'),
(2065, 157, 7, '10:00:00', '18:00:00'),
(2066, 158, 1, '10:00:00', '18:00:00'),
(2067, 158, 2, '10:00:00', '18:00:00'),
(2068, 158, 3, '10:00:00', '18:00:00'),
(2069, 158, 4, '10:00:00', '18:00:00'),
(2070, 158, 5, '10:00:00', '18:00:00'),
(2071, 158, 6, '10:00:00', '18:00:00'),
(2072, 158, 7, '10:00:00', '18:00:00'),
(2073, 159, 1, '10:00:00', '18:00:00'),
(2074, 159, 2, '10:00:00', '18:00:00'),
(2075, 159, 3, '10:00:00', '18:00:00'),
(2076, 159, 4, '10:00:00', '18:00:00'),
(2077, 159, 5, '10:00:00', '18:00:00'),
(2078, 159, 6, '10:00:00', '18:00:00'),
(2079, 159, 7, '10:00:00', '18:00:00'),
(2080, 160, 1, '10:00:00', '18:00:00'),
(2081, 160, 2, '10:00:00', '18:00:00'),
(2082, 160, 3, '10:00:00', '18:00:00'),
(2083, 160, 4, '10:00:00', '18:00:00'),
(2084, 160, 5, '10:00:00', '18:00:00'),
(2085, 160, 6, '10:00:00', '18:00:00'),
(2086, 160, 7, '10:00:00', '18:00:00'),
(2087, 161, 1, '10:00:00', '18:00:00'),
(2088, 161, 2, '10:00:00', '18:00:00'),
(2089, 161, 3, '10:00:00', '18:00:00'),
(2090, 161, 4, '10:00:00', '18:00:00'),
(2091, 161, 5, '10:00:00', '18:00:00'),
(2092, 161, 6, '10:00:00', '18:00:00'),
(2093, 161, 7, '10:00:00', '18:00:00'),
(2094, 162, 1, '10:00:00', '18:00:00'),
(2095, 162, 2, '10:00:00', '18:00:00'),
(2096, 162, 3, '10:00:00', '18:00:00'),
(2097, 162, 4, '10:00:00', '18:00:00'),
(2098, 162, 5, '10:00:00', '18:00:00'),
(2099, 162, 6, '10:00:00', '18:00:00'),
(2100, 162, 7, '10:00:00', '18:00:00'),
(2101, 163, 1, '10:00:00', '18:00:00'),
(2102, 163, 2, '10:00:00', '18:00:00'),
(2103, 163, 3, '10:00:00', '18:00:00'),
(2104, 163, 4, '10:00:00', '18:00:00'),
(2105, 163, 5, '10:00:00', '18:00:00'),
(2106, 163, 6, '10:00:00', '18:00:00'),
(2107, 163, 7, '10:00:00', '18:00:00'),
(2108, 164, 1, '10:00:00', '18:00:00'),
(2109, 164, 2, '10:00:00', '18:00:00'),
(2110, 164, 3, '10:00:00', '18:00:00'),
(2111, 164, 4, '10:00:00', '18:00:00'),
(2112, 164, 5, '10:00:00', '18:00:00'),
(2113, 164, 6, '10:00:00', '18:00:00'),
(2114, 164, 7, '10:00:00', '18:00:00'),
(2115, 68, 1, '09:00:00', '17:00:00'),
(2116, 68, 2, '09:00:00', '17:00:00'),
(2117, 68, 3, '09:00:00', '17:00:00'),
(2118, 68, 4, '09:00:00', '17:00:00'),
(2119, 68, 5, '09:00:00', '17:00:00'),
(2120, 68, 6, '09:00:00', '17:00:00'),
(2121, 68, 7, '09:00:00', '17:00:00'),
(2122, 69, 1, '08:00:00', '17:00:00'),
(2123, 69, 2, '08:00:00', '17:00:00'),
(2124, 69, 3, '08:00:00', '17:00:00'),
(2125, 69, 4, '08:00:00', '17:00:00'),
(2126, 69, 5, '08:00:00', '17:00:00'),
(2127, 69, 6, '08:00:00', '18:00:00'),
(2128, 69, 7, '08:00:00', '18:00:00'),
(2129, 70, 1, '07:00:00', '17:00:00'),
(2130, 70, 2, '07:00:00', '17:00:00'),
(2131, 70, 3, '07:00:00', '17:00:00'),
(2132, 70, 4, '07:00:00', '17:00:00'),
(2133, 70, 5, '07:00:00', '17:00:00'),
(2134, 70, 6, '07:00:00', '17:00:00'),
(2135, 70, 7, '07:00:00', '17:00:00'),
(2136, 71, 1, '00:00:00', '00:00:00'),
(2137, 71, 2, '00:00:00', '00:00:00'),
(2138, 71, 3, '00:00:00', '00:00:00'),
(2139, 71, 4, '00:00:00', '00:00:00'),
(2140, 71, 5, '00:00:00', '00:00:00'),
(2141, 71, 6, '00:00:00', '00:00:00'),
(2142, 71, 7, '00:00:00', '00:00:00'),
(2143, 73, 1, '18:00:00', '02:00:00'),
(2144, 73, 2, '18:00:00', '02:00:00'),
(2145, 73, 3, '18:00:00', '02:00:00'),
(2146, 73, 4, '18:00:00', '02:00:00'),
(2147, 73, 5, '18:00:00', '02:00:00'),
(2148, 73, 6, '18:00:00', '02:00:00'),
(2149, 73, 7, '18:00:00', '02:00:00'),
(2150, 74, 1, '23:00:00', '03:00:00'),
(2151, 74, 2, '23:00:00', '03:00:00'),
(2152, 74, 3, '23:00:00', '03:00:00'),
(2153, 74, 4, '23:00:00', '03:00:00'),
(2154, 74, 5, '23:00:00', '03:00:00'),
(2155, 74, 6, '23:00:00', '03:00:00'),
(2156, 74, 7, '23:00:00', '03:00:00'),
(2157, 75, 1, '09:00:00', '18:00:00'),
(2158, 75, 2, '09:00:00', '18:00:00'),
(2159, 75, 3, '09:00:00', '18:00:00'),
(2160, 75, 4, '09:00:00', '18:00:00'),
(2161, 75, 5, '09:00:00', '18:00:00'),
(2162, 75, 6, '09:00:00', '18:00:00'),
(2163, 75, 7, '09:00:00', '18:00:00'),
(2164, 76, 1, '09:00:00', '18:00:00'),
(2165, 76, 2, '09:00:00', '18:00:00'),
(2166, 76, 3, '09:00:00', '18:00:00'),
(2167, 76, 4, '09:00:00', '18:00:00'),
(2168, 76, 5, '09:00:00', '18:00:00'),
(2169, 76, 6, '09:00:00', '18:00:00'),
(2170, 76, 7, '09:00:00', '18:00:00'),
(2171, 77, 1, '09:00:00', '17:00:00'),
(2172, 77, 2, '09:00:00', '17:00:00'),
(2173, 77, 3, '09:00:00', '17:00:00'),
(2174, 77, 4, '09:00:00', '17:00:00'),
(2175, 77, 5, '09:00:00', '17:00:00'),
(2176, 77, 6, '09:00:00', '17:00:00'),
(2177, 77, 7, '09:00:00', '17:00:00'),
(2178, 78, 1, '09:00:00', '17:00:00'),
(2179, 78, 2, '09:00:00', '17:00:00'),
(2180, 78, 3, '09:00:00', '17:00:00'),
(2181, 78, 4, '09:00:00', '17:00:00'),
(2182, 78, 5, '09:00:00', '17:00:00'),
(2183, 78, 6, '09:00:00', '17:00:00'),
(2184, 78, 7, '09:00:00', '17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `office_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`, `image_id`, `department_id`, `office_no`, `work_no`, `company`) VALUES
(1, 'Admin', 'faiz@hpcs.my', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', 'Asia/Kuala_Lumpur', NULL, NULL, '', '', ''),
(2, 'HPCS Support ', 'support@hpcs.my', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', 'Asia/Kuala_Lumpur', NULL, NULL, '0358873341 ', '01033141171', 'HPCS Sdn Bhd'),
(7, 'User ITC', 'itc@test.my', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '0123456789', NULL),
(8, 'Noor Syuhadah ', 'syuhadah.rusdi@gmail.com', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '0125828587', NULL),
(9, 'Hadzim ', 'hadzimmahaad@gmail.com', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '0172042685', NULL),
(10, '', '', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '', NULL),
(11, 'Amalina Baharom ', 'amalina@hpcs.my', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '0132220106', NULL),
(12, 'atila', 'nurathilahmohammad@gmail.com', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '01112335161', NULL),
(13, 'apeng bos', 'apeng@gmail.com', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '0123456789', NULL),
(14, 'tell', 'tell@gmail.com', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '01112335161', NULL),
(15, 'till', 'till@gmail.com', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '0111233651', NULL),
(16, 'Amalina baharom ', 'nuramalina8@gmail.com', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '0132220106', NULL),
(17, 'Nadhirah Aripin', 'nadhirah1988@gmail.com', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '0193560530', NULL),
(18, 'Mohd Zaidi Bahirin Zainal Abidin', 'zaidictc@gmail.com', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL, 'Asia/Kuala_Lumpur', NULL, NULL, NULL, '0127019775', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `spending_type`
--

CREATE TABLE `spending_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spending_type`
--

INSERT INTO `spending_type` (`id`, `name`, `status`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'Fast-Pace', 1, NULL, NULL, 1, '2018-09-03 07:00:12'),
(2, 'Medium', 1, 1, '2018-09-03 15:00:24', 1, '2018-09-03 07:00:24'),
(3, 'Slow & Easy', 1, 1, '2018-09-03 15:00:36', 1, '2018-09-03 07:00:36');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `status`) VALUES
(1, 'Johor', 1),
(2, 'Kedah', 1),
(3, 'Kelantan', 1),
(4, 'Melaka', 1),
(5, 'Negeri Sembilan', 1),
(6, 'Pahang', 1),
(7, 'Pulau Pinang', 1),
(8, 'Perak', 1),
(9, 'Perlis', 1),
(10, 'Selangor', 1),
(11, 'Terengganu', 1),
(12, 'Sabah', 1),
(13, 'Sarawak', 1),
(14, 'Wilayah Persekutuan Kuala Lumpur', 1),
(15, 'Wilayah Persekutuan Labuan', 1),
(16, 'Wilayah Persekutuan Putrajaya', 1);

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(1, 'x270oG7B3epITVQiROBVgCg07xG1uf-x', 1547630185, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`, `status`, `password_reset_token`) VALUES
(1, 'admin', 'admin@admin.my', '$2y$10$S4Dr0DtwP6QihO6wSwSk6ustSURo8Nr/hWzUgBr9ZMA4ChBXH3o9.', 'afrAn7_2CIX2Uuk5GHWxAMvyepfz-Srg', 1505969126, 'admin@admin.my', NULL, NULL, 1505969114, 1547631858, 0, 1550537618, 10, NULL),
(2, 'support', 'support@hpcs.my', '$2y$13$EWt0koHGs3dM7IEljF4CQOK6/17gM6QZTL.uW71GL8.X0wAdQs462', 'Nenf8C7LQsruH7ChIu-fb6XLdKVgfiXq', 1505969126, NULL, NULL, NULL, 1505969114, 1545619796, 0, 1550454665, 10, NULL),
(7, 'itc@test.my', 'itc@test.my', '$2y$13$SXzOzpb6Y27lAOpsZcIMO.KJonWh0c5qvXWltqTWpcJSWl0DP.F1m', 'NEbFmJ0n-1HW21kHQTGinfYqu7cZykau', 1545644488, NULL, NULL, '192.168.1.101', 1545644488, 1545644488, 0, NULL, 10, NULL),
(8, 'syuhadah.rusdi@gmail.com', 'syuhadah.rusdi@gmail.com', '$2y$13$sr3GHmKH2Xlg8iyealPuV.QePzci7YwK12ErREI3kBPL9tYJrcXPC', 'IThRw-EQchtbBMa3hNm2QQ6J95SqzUIQ', 1545843640, NULL, NULL, '192.168.1.101', 1545843640, 1545843640, 0, NULL, 10, NULL),
(9, 'hadzimmahaad@gmail.com', 'hadzimmahaad@gmail.com', '$2y$13$znWxNtEWf00KSD/ScigPeeYcRMT973FrsOB/zt8UZrV195brQrY/m', 'QgrGqMyulzbSY0r5om35YQd7bmeCYooM', 1545871978, NULL, NULL, '192.168.1.101', 1545871978, 1545872434, 0, NULL, 10, NULL),
(10, '', '', '$2y$13$cQ0yENEBVbQX3BHmyet8m.0ZvJEYbPbcmDWHIYotDxXPCMv2LYcrO', 'ty3ZudSZdF4VuoPSLEa9NcuzCn9BEWCQ', 1545882528, NULL, NULL, '192.168.1.101', 1545882528, 1545882528, 0, NULL, 10, NULL),
(11, 'amalina@hpcs.my', 'amalina@hpcs.my', '$2y$13$FsYWHnpCXr3BABNtdCrEHue56I//YwxV1dU1R36/QzWaUqybcb0T2', 'jEw30MitvHYfjrNJ8VFc1YHzpPDRHhov', 1545910414, NULL, NULL, '192.168.1.101', 1545910414, 1545910414, 0, NULL, 10, NULL),
(12, 'nurathilahmohammad@gmail.com', 'nurathilahmohammad@gmail.com', '$2y$13$5qQjOGJq4vcURfrwOnqfb.Zpin72./AMDc//b60g1GHPsPycdLdue', '301gsJLSEH7bfq5YthE8A7BplHbcQw_-', 1546423364, NULL, NULL, '192.168.1.101', 1546423364, 1546423364, 0, NULL, 10, NULL),
(13, 'apeng@gmail.com', 'apeng@gmail.com', '$2y$13$Qm1o2TqLAKgSGG.aU3VNCumT2GcOMoNcdQpPVUccY8JBCsv3DvTMe', 'IwyeDVPRbDPnxGN3QP1-g9VjJbfppKdj', 1546480209, NULL, NULL, '192.168.1.101', 1546480209, 1546504038, 0, NULL, 10, NULL),
(14, 'tell@gmail.com', 'tell@gmail.com', '$2y$13$f/4/4gyD2EiyJ1K5sjp/3O4MDqSO/mIyBuv7qwQFPyDUCplHZl/Va', 'cXkw02EyVo5E0R_w4xkG6UsFhc4d3_sB', 1546480959, NULL, NULL, '192.168.1.101', 1546480959, 1546480959, 0, NULL, 10, NULL),
(15, 'till@gmail.com', 'till@gmail.com', '$2y$13$AJhfhuSgGwr.qtd0D3JbA.1SIo4xJy9y9V1MYeomvDyyFdmVY4O22', '-s7PBdpW9nm5h1L4k1vY2XtZKUFbF-0S', 1546481002, NULL, NULL, '192.168.1.101', 1546481002, 1546481002, 0, NULL, 10, NULL),
(16, 'nuramalina8@gmail.com', 'nuramalina8@gmail.com', '$2y$13$haXYw23A695UXHtRWXuI0.Q.qR9wGAk8.qcn7y.lPwgur1jNFi8sG', 'fg3spJYso2YTDazOjBDX3-d1f5zkP0jk', 1547460511, NULL, NULL, '192.168.1.101', 1547460511, 1547460511, 0, NULL, 10, NULL),
(17, 'nadhirah1988@gmail.com', 'nadhirah1988@gmail.com', '$2y$13$9CSRjxnZONsbQ2cWFvMeRe6ihMTz6aXjmMiSc3BiuYwF6oqBYOO/C', 'RH2c5VmQWlhEuaINXwdbv1IHbSQrG_kQ', 1547520105, NULL, NULL, '192.168.1.101', 1547520105, 1547520105, 0, NULL, 10, NULL),
(18, 'zaidictc@gmail.com', 'zaidictc@gmail.com', '$2y$13$EwnIftHA77fkdRhjfCukaO5GsmWtOdZlhtGrp6AwaLrqCW77ZuRgW', 'x8Z88WssNb_7HuUQSRU2kZgbYXgDOL-N', 1547520159, NULL, NULL, '192.168.1.101', 1547520159, 1547520159, 0, NULL, 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_mobile`
--

CREATE TABLE `user_mobile` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `islamic_elements`
--
ALTER TABLE `islamic_elements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `parking_type`
--
ALTER TABLE `parking_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_place_state_id` (`state_id`),
  ADD KEY `fk_place_parking_type_id` (`parking_type_id`),
  ADD KEY `fk_place_spending_type_id` (`spending_type_id`);

--
-- Indexes for table `place_category`
--
ALTER TABLE `place_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_place_category_place_id` (`place_id`);

--
-- Indexes for table `place_image`
--
ALTER TABLE `place_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_place_image_place_id` (`place_id`);

--
-- Indexes for table `place_islamic_element`
--
ALTER TABLE `place_islamic_element`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_place_islamic_element_place_id` (`place_id`);

--
-- Indexes for table `place_operation_hour`
--
ALTER TABLE `place_operation_hour`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_place_operation_hour_place_id` (`place_id`),
  ADD KEY `fk_place_operation_hour_day_id` (`day_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Indexes for table `spending_type`
--
ALTER TABLE `spending_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- Indexes for table `user_mobile`
--
ALTER TABLE `user_mobile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `islamic_elements`
--
ALTER TABLE `islamic_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `parking_type`
--
ALTER TABLE `parking_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `place`
--
ALTER TABLE `place`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `place_category`
--
ALTER TABLE `place_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `place_image`
--
ALTER TABLE `place_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `place_islamic_element`
--
ALTER TABLE `place_islamic_element`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `place_operation_hour`
--
ALTER TABLE `place_operation_hour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2185;
--
-- AUTO_INCREMENT for table `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spending_type`
--
ALTER TABLE `spending_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user_mobile`
--
ALTER TABLE `user_mobile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `place`
--
ALTER TABLE `place`
  ADD CONSTRAINT `fk_place_parking_type_id` FOREIGN KEY (`parking_type_id`) REFERENCES `parking_type` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `fk_place_spending_type_id` FOREIGN KEY (`spending_type_id`) REFERENCES `spending_type` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `fk_place_state_id` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE NO ACTION;

--
-- Constraints for table `place_category`
--
ALTER TABLE `place_category`
  ADD CONSTRAINT `fk_place_category_place_id` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `place_image`
--
ALTER TABLE `place_image`
  ADD CONSTRAINT `fk_place_image_place_id` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `place_islamic_element`
--
ALTER TABLE `place_islamic_element`
  ADD CONSTRAINT `fk_place_islamic_element_place_id` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `place_operation_hour`
--
ALTER TABLE `place_operation_hour`
  ADD CONSTRAINT `fk_place_operation_hour_day_id` FOREIGN KEY (`day_id`) REFERENCES `days` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_place_operation_hour_place_id` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
