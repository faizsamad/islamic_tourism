<?php

use yii\db\Migration;

/**
 * Handles the creation of table `place_islamic_element`.
 */
class m180901_022144_create_place_islamic_element_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /**
         * Create table place_islamic_element and relation with place
         */
        $this->createTable('place_islamic_element', [
            'id' => $this->primaryKey(),
            'place_id' => $this->integer(),
            'islamic_element_id' => $this->string(100),

        ]);

        // create foreign key relationship
        $this->addForeignKey('fk_place_islamic_element_place_id', 'place_islamic_element', 'place_id', 'place', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('place_islamic_element');
    }
}
