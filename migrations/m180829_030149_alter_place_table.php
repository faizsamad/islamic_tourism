<?php

use yii\db\Migration;

/**
 * Class m180829_030149_alter_place_table
 */
class m180829_030149_alter_place_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('place', 'status', $this->integer()->defaultValue(1)->after('url'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180829_030149_alter_place_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180829_030149_alter_place_table cannot be reverted.\n";

        return false;
    }
    */
}
