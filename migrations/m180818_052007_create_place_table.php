<?php

use yii\db\Migration;

/**
 * Handles the creation of table `place`.
 */
class m180818_052007_create_place_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {   
        /**
         * Create table category
         */
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'status' => $this->integer()->defaultValue(1),

        ]);

        /**
         * insert data into category
         */
        $this->insert('category', ['id'=>1, 'name'=>'Culture', 'status'=>1]);
        $this->insert('category', ['id'=>2, 'name'=>'Outdoors', 'status'=>1]);
        $this->insert('category', ['id'=>3, 'name'=>'Relaxing', 'status'=>1]);
        $this->insert('category', ['id'=>4, 'name'=>'Romantic', 'status'=>1]);
        $this->insert('category', ['id'=>5, 'name'=>'Beaches', 'status'=>1]);
        $this->insert('category', ['id'=>6, 'name'=>'Historic Sites', 'status'=>1]);
        $this->insert('category', ['id'=>7, 'name'=>'Museums', 'status'=>1]);
        $this->insert('category', ['id'=>8, 'name'=>'Shopping', 'status'=>1]);
        $this->insert('category', ['id'=>9, 'name'=>'Wildlife', 'status'=>1]);

        /**
         * Create table parking type
         */
        $this->createTable('parking_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'status' => $this->integer()->defaultValue(1),

        ]);

        /**
         * insert data into parking_type
         */
        $this->insert('parking_type', ['id'=>1, 'name'=>'Lot parking available', 'status'=>1]);

        /**
         * Create table states
         */
        $this->createTable('states', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'status' => $this->integer()->defaultValue(1),

        ]);

        /**
         * insert data into states
         */
        $this->insert('states', ['id'=>1, 'name'=>'Johor', 'status'=>1]);
        $this->insert('states', ['id'=>2, 'name'=>'Kedah', 'status'=>1]);
        $this->insert('states', ['id'=>3, 'name'=>'Kelantan', 'status'=>1]);
        $this->insert('states', ['id'=>4, 'name'=>'Melaka', 'status'=>1]);
        $this->insert('states', ['id'=>5, 'name'=>'Negeri Sembilan', 'status'=>1]);
        $this->insert('states', ['id'=>6, 'name'=>'Pahang', 'status'=>1]);
        $this->insert('states', ['id'=>7, 'name'=>'Pulau Pinang', 'status'=>1]);
        $this->insert('states', ['id'=>8, 'name'=>'Perak', 'status'=>1]);
        $this->insert('states', ['id'=>9, 'name'=>'Perlis', 'status'=>1]);
        $this->insert('states', ['id'=>10, 'name'=>'Selangor', 'status'=>1]);
        $this->insert('states', ['id'=>11, 'name'=>'Terengganu', 'status'=>1]);
        $this->insert('states', ['id'=>12, 'name'=>'Sabah', 'status'=>1]);
        $this->insert('states', ['id'=>13, 'name'=>'Sarawak', 'status'=>1]);
        $this->insert('states', ['id'=>14, 'name'=>'Wilayah Persekutuan Kuala Lumpur', 'status'=>1]);
        $this->insert('states', ['id'=>15, 'name'=>'Wilayah Persekutuan Labuan', 'status'=>1]);
        $this->insert('states', ['id'=>16, 'name'=>'Wilayah Persekutuan Putrajaya', 'status'=>1]);

        /**
         * Create table spending_type
         */
        $this->createTable('spending_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'status' => $this->integer()->defaultValue(1),

        ]);

        /**
         * insert data into spending_type
         */
        $this->insert('spending_type', ['id'=>1, 'name'=>'Normal', 'status'=>1]);

        /**
         * Create table place
         */
        $this->createTable('place', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'contact_no' => $this->string(15)->notNull(),
            'duration' => $this->time(),
            'address1' => $this->string()->notNull(),
            'address2' => $this->string(),
            'address3' => $this->string(),
            'poscode' => $this->string(15)->notNull(),
            'city' => $this->string(100)->notNull(),
            'state_id' => $this->integer()->notNull(),
            'price_range' => $this->text(),
            'parking_type_id' => $this->integer()->notNull(),
            'detail' => $this->text(),
            'spending_type_id' => $this->integer()->notNull(),
            'latitude' => $this->float(),
            'longitude' => $this->float(),
            'url' => $this->string(),
            'created_by' => $this->integer(),
            'created_date' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_date' => $this->timestamp(),
            
        ]);

        // create foreign key relationship
        $this->addForeignKey('fk_place_state_id', 'place', 'state_id', 'states', 'id', 'NO ACTION');
        $this->addForeignKey('fk_place_parking_type_id', 'place', 'parking_type_id', 'parking_type', 'id', 'NO ACTION');
        $this->addForeignKey('fk_place_spending_type_id', 'place', 'spending_type_id', 'spending_type', 'id', 'NO ACTION');

        /**
         * Create table place_category and relation with place
         */
        $this->createTable('place_category', [
            'id' => $this->primaryKey(),
            'place_id' => $this->integer(),
            'category_id' => $this->string(100),

        ]);

        // create foreign key relationship
        $this->addForeignKey('fk_place_category_place_id', 'place_category', 'place_id', 'place', 'id', 'CASCADE');
        // $this->addForeignKey('fk_place_category_category_id', 'place_category', 'category_id', 'category', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('place');
    }
}


