<?php

use yii\db\Migration;

/**
 * Handles the creation of table `islamic_elements`.
 */
class m180831_154605_create_islamic_elements_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /**
         * Create table islamic_elements
         */
        $this->createTable('islamic_elements', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'status' => $this->integer()->defaultValue(1),
            'created_by' => $this->integer(),
            'created_date' => $this->datetime(),
            'updated_by' => $this->integer(),
            'updated_date' => $this->timestamp(),

        ]);

        /**
         * insert data into islamic_elements
         */
        $this->insert('islamic_elements', ['id'=>1, 'name'=>'Islamic Facilities', 'status'=>1]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('islamic_elements');
    }
}
