<?php

use yii\db\Migration;

/**
 * Class m190116_084420_alter_place_table
 */
class m190116_084420_alter_place_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('place', 'operating_remark', $this->text()->after('url'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190116_084420_alter_place_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190116_084420_alter_place_table cannot be reverted.\n";

        return false;
    }
    */
}
