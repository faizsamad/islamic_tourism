<?php

use yii\db\Migration;

/**
 * Handles the creation of table `place_operation_hour`.
 */
class m180818_054818_create_place_operation_hour_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        /**
         * Create table days
         */
        $this->createTable('days', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'status' => $this->integer()->defaultValue(1),

        ]);

        /**
         * insert data into days
         */
        $this->insert('days', ['id'=>1, 'name'=>'Monday', 'status'=>1]);
        $this->insert('days', ['id'=>2, 'name'=>'Tuesday', 'status'=>1]);
        $this->insert('days', ['id'=>3, 'name'=>'Wednesday', 'status'=>1]);
        $this->insert('days', ['id'=>4, 'name'=>'Thursday', 'status'=>1]);
        $this->insert('days', ['id'=>5, 'name'=>'Friday', 'status'=>1]);
        $this->insert('days', ['id'=>6, 'name'=>'Saturday', 'status'=>1]);
        $this->insert('days', ['id'=>7, 'name'=>'Sunday', 'status'=>1]);

        /**
         * Create table place_operation_hour
         */
        $this->createTable('place_operation_hour', [
            'id' => $this->primaryKey(),
            'place_id' => $this->integer(),
            'day_id' => $this->integer(),
            'start_time' => $this->time(),
            'end_time' => $this->time(),

        ]);

        // create foreign key relationship
        $this->addForeignKey('fk_place_operation_hour_place_id', 'place_operation_hour', 'place_id', 'place', 'id', 'CASCADE');
        $this->addForeignKey('fk_place_operation_hour_day_id', 'place_operation_hour', 'day_id', 'days', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('place_operation_hour');
    }
}
