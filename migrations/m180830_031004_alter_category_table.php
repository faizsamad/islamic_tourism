<?php

use yii\db\Migration;

/**
 * Class m180830_031004_alter_category_table
 */
class m180830_031004_alter_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('category', 'created_by', $this->integer()->after('status'));
        $this->addColumn('category', 'created_date', $this->datetime()->after('created_by'));
        $this->addColumn('category', 'updated_by', $this->integer()->after('created_date'));
        $this->addColumn('category', 'updated_date', $this->timestamp()->after('updated_by'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180830_031004_alter_category_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180830_031004_alter_category_table cannot be reverted.\n";

        return false;
    }
    */
}
