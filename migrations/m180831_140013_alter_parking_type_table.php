<?php

use yii\db\Migration;

/**
 * Class m180831_140013_alter_parking_type_table
 */
class m180831_140013_alter_parking_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('parking_type', 'created_by', $this->integer()->after('status'));
        $this->addColumn('parking_type', 'created_date', $this->datetime()->after('created_by'));
        $this->addColumn('parking_type', 'updated_by', $this->integer()->after('created_date'));
        $this->addColumn('parking_type', 'updated_date', $this->timestamp()->after('updated_by'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180831_140013_alter_parking_type_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180831_140013_alter_parking_type_table cannot be reverted.\n";

        return false;
    }
    */
}
