<?php

use yii\db\Migration;

/**
 * Handles the creation of table `place_image`.
 */
class m180818_054759_create_place_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('place_image', [
            'id' => $this->primaryKey(),
            'place_id' => $this->integer(),
            'file_name' => $this->string(100),
            'file_size' => $this->string(100),
            'file_type' => $this->string(100),
            'file_path' => $this->string(255),
            'detail' => $this->text(),

        ]);

        $this->addForeignKey('fk_place_image_place_id', 'place_image', 'place_id', 'place', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('place_image');
    }
}
