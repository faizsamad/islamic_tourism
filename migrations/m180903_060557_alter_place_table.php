<?php

use yii\db\Migration;

/**
 * Class m180903_060557_alter_place_table
 */
class m180903_060557_alter_place_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('place', 'contact_no', $this->string(15));
        $this->alterColumn('place', 'latitude', $this->float()->notNull());
        $this->alterColumn('place', 'longitude', $this->float()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180903_060557_alter_place_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180903_060557_alter_place_table cannot be reverted.\n";

        return false;
    }
    */
}
